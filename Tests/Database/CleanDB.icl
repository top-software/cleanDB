module CleanDB

import qualified Data.Foldable as DF
from Data.Foldable import class Foldable, instance Foldable []
import Data.Func, Data.Functor
from Data.List import find
import qualified Data.Map as Map
import Data.Maybe
import qualified Data.Set as Set
from Data.Set import instance Foldable Set
import Data.Tuple
import Gast, Gast.CommandLine, Gast.Gen
import Math.Random
import StdEnv => qualified all
import StdOverloadedList
import Text.GenPrint

import Database.CleanDB
import Database.CleanDB.Gast
import qualified Database.CleanDB.KeyEncoding as CDB.KeyEncoding

Start world = exposeProperties [OutputTestEvents] [Bent] properties world

// TODO: add properties concerning values as well, not only keys
// TODO: add properties concerning multi maps
// TODO: add properties mixing different kinds of conditions, e.g. ((0,3), isIn [1,2,3], LessThan 3)
properties :: [Property]
properties =
	[ performLargeWriteTransactions            as "perform large write transactions"
	, computeLargeSum                          as "compute large sum"
	, getAllIntKeys                            as "get all Int keys"
	, getAllIntKeysFromMulti                   as "get all Int keys from multi map"
	, getAllStringKeys                         as "get all String keys"
	, getAllIntIntKeys                         as "get all (Int, Int) keys"
	, getAllStringIntKeys                      as "get all (String, Int) keys"
	, getAllIntStringKeys                      as "get all (Int, String) keys"
	, getAllStringStringKeys                   as "get all (String, String) keys"
	, getNoIntKey                              as "get no Int keys"
	, getNoIntIntKey                           as "get no (Int, Int) keys"
	, getSingleIntKey                          as "get single Int key"
	, getSingleIntKeyFromPartitionedMap        as "get single Int key from partitioned map"
	, getSingleStringKey                       as "get single String key"
	, getSingleIntIntKey                       as "get single (Int, Int) key"
	, getSingleStringIntKey                    as "get single (String, Int) key"
	, getSingleIntStringKey                    as "get single (Int, String) key"
	, getSingleStringStringKey                 as "get single (String, String) key"
	, getSmallestIntKey                        as "get smallest Int key"
	, getSmallestIntIntKey                     as "get smallest (Int, Int) key"
	, getSmallestIntAndSingleIntKey            as "get smallest Int and single Int key"
	, getIntKeysObeyingIneq                    as "get all Int keys obeying inequality"
	, getStringKeysObeyingIneq                 as "get all String keys obeying inequality"
	, getIntIntKeysObeyingIneq                 as "get all (Int, Int) keys obeying inequality"
	, getStringStringKeysObeyingIneq           as "get all (String, String) keys obeying inequality"
	, getIntStringKeysObeyingIneq              as "get all (Int, String) keys obeying inequality"
	, getStringIntKeysObeyingIneq              as "get all (String, Int) keys obeying inequality"
	, getIntKeyRange                           as "get Int key range"
	, getStringKeyRange                        as "get String key range"
	, getIntIntKeyRange                        as "get (Int, Int) key range"
	, getStringIntKeyRange                     as "get (String, Int) key range"
	, getIntStringKeyRange                     as "get (Int, String) key range"
	, getStringStringKeyRange                  as "get (String, String) key range"
	, getIntIntIntKeyRange                     as "get (Int, Int, Int) key range"
	, getStringStringStringKeyRange            as "get (String, String, String) key range"
	, getStringIntStringKeyRange               as "get (String, Int, String) key range"
	, getIntStringIntKeyRange                  as "get (Int, String, Int) key range"
	, getIntIntIntIntKeyRange                  as "get (Int, Int, Int, Int) key range"
	, getStringIntStringIntKeyRange            as "get (String, Int, String, Int) key range"
	, getIntKeySet                             as "get Int key set"
	, getStringKeySet                          as "get String key set"
	, getIntIntKeySet                          as "get (Int, Int) key set"
	, getStringStringKeySet                    as "get (String, String) key set"
	, getIntStringKeySet                       as "get (Int, String) key set"
	, getStringIntKeySet                       as "get (String, Int) key set"
	, getIntKeyNearValue                       as "get Int key near value"
	, getStringKeyNearValue                    as "get String key near value"
	, getIntIntKeyNearValues                   as "get (Int, Int) key near values"
	, getStringStringKeyNearValues             as "get (String, String) key near values"
	, getIntStringKeyNearValues                as "get (Int, String) key near values"
	, getStringIntKeyNearValues                as "get (String, Int) key near values"
	, getIntIntIntKeyNearValues                as "get (Int, Int, Int) key near values"
	, getSingleIntAndAllIntKeys                as "get single Int and all Int keys of (Int, Int)"
	, getAllIntIntKeysNear                     as "get (all Int keys, Int keys near)"
	, getSingleIntIntKeysNear                  as "get (single Int keys, Int keys near)"
	, getIntKeysNearAllIntKeys                 as "get (Int keys near, all Int keys)"
	, getSingleIntKeyIntKeysNearSingleIntKey   as "get (single Int key, Int keys near, single Int Key)"
	, getAllIntOrAllIntKeys                    as "get all Int OR all Int keys"
	, getNoIntOrNoIntKey                       as "get no Int OR no Int keys"
	, getSingleIntOrSingleIntKey               as "get single Int key OR single Int key"
	, getIntKeysObeyingIneqOrObeyingIneq       as "get Int keys obeying inequality OR keys obeying inequality"
	, getIntKeyRangeOrKeyRange                 as "get Int key range OR key range"
	, getIntKeySetOrKeySet                     as "get Int key set OR key set"
	, getSmallestIntOrSmallestIntKey           as "get smallest Int OR smallest Int key"
	, getIntKeyNearValueOrNearValue            as "get Int key near value OR near value"
	, getNoOrIntKeyNearValue                   as "get no OR Int key near value"
	, getIntKeyNearValueOrNoIntKey             as "get Int keys near value OR no Int key"
	, getIntKeysNearValueOrObeyingIneq         as "get Int keys near value OR obeying inequality"
	, getIntKeysObeyingIneqOrNearValue         as "get Int keys obeying inequality OR near value"
	, getIntKeysNearValueOrInRange             as "get Int keys near value OR in range"
	, getIntKeysInRangeOrNearValue             as "get Int keys in range OR near value"
	, getIntIntKeysEqToOrNearValueForFirstKey  as "get (equals Int value OR near Int value, all Int values)"
	, getIntIntKeysEqToOrNearValueForSecondKey as "get (all Int keys, equals Int value OR near Int value)"
	, getSingleIntKeyAndValuesOfMultiMap       as "get single Int key and values of multi map"
	, getIntKeysNearValueAndValuesOfMultiMap   as "get Int keys near value and values of multi map"
	, getAllIntKeysOrNearValueAndValuesOfMultiMap  as "get all Int keys OR near value and values of multi map"
	, getIntKeyRangeInParallel                 as "get Int key range in parallel"
	, getIntKeyRangeFromMultiInParallel        as "get Int key range from multi map in parallel"
	, getSingleLargeValueFromMap               as "get single large value from map"
	, getSingleReplacedLargeValueFromMap       as "get single replaced large value from map"
	, getLargeValuesFromMap                    as "get large values from map"
	, getDataFromMapsUsingContinuationChain    as "get data from multiple maps using continuation chain."
	, accumulateInParallelDifferentTypes       as "accumulate states of different types in parallel"
	, deleteCharKeysFromMap                    as "delete Char keys from map"
	, deleteCharKeysFromMapGetAccumDelete      as "delete Char keys from map using getAccumDelete"
	, deleteCharKeysFromMultiMap               as "delete Char keys from multi map"
	, deleteCharKeysFromMultiMapGetMultiAccumDeleteIsIn as
		"delete Char keys from multi map using getMultiAccumDelete (isIn condition)"
	// This test is necessary because `executeCond` for `isIn` does not jump to the next key after a key is processed.
	, deleteCharKeysFromMultiMapGetMultiAccumDeleteIsIn as
		"delete Char keys from multi map using getMultiAccumDelete (all condition)"
	, deleteCharKeyValuesFromMultiMap          as "delete Char keys and values from multi map"
	, deleteAndThenGetCharKeysNearValue        as "delete and then get char keys near value"
	, deleteLargeValuesFromMapResultsInLargeValueMapValuesBeingDeleted as
		"delete large values from map results in internal large value map values being deleted"
	, deleteAccumLargeValuesFromMapResultsInLargeValueMapValuesBeingDeleted as
		"delete large values from map using getAccumDelete results in internal large value map values being deleted"
	, delPartition as "delete partition"
	, addAndCheckStringValues                  as "add string values and check equality after reading them again"
	, addAndCheckdbMultiMapFixedSize           as "add string values to multi map with fixed value size"
	, addAndCheckValsToIntValMultiMap          as "add integer values to int values multi map"
	]

// Large write transactions caused crashes due to bugs in the LMDB freelist management.
performLargeWriteTransactions :: Property
performLargeWriteTransactions =: withTestCleanDB ParallelOperationsDisabled getKeys`
where
	getKeys` cdb
		# (_,   cdb) = doTransaction (\txn -> ((), foldl (\txn k -> put k 1 map txn) txn [1..n])) cdb
		// Overwrite all keys with different values to cause a large number of free pages to be stored.
		# (_,   cdb) = doTransaction (\txn -> ((), foldl (\txn k -> put k 2 map txn) txn [1..n])) cdb
		= (prop True, cdb)

	map = 'CDB.KeyEncoding'.dbMap "intMap"

	n = 10000000

computeLargeSum :: Property
computeLargeSum =: withTestCleanDB ParallelOperationsDisabled getKeys`
where
	getKeys` cdb
		# (_,   cdb) = doTransaction (\txn -> ((), foldl (\txn k -> put k 1 map txn) txn [1..n])) cdb
		# (sum, cdb) = doReadOnlyTransaction (getAccum all (withoutStop $ \_ -> (+)) 0 map) cdb
		= (sum =.= n, cdb)

	map = 'CDB.KeyEncoding'.dbMap "intMap"

	n = 10000000

getAllIntKeys :: ![Int] -> Property
getAllIntKeys keys = getAllKeys False keys

getAllIntKeysFromMulti :: ![Int] -> Property
getAllIntKeysFromMulti keys = getAllKeys True keys

getAllStringKeys :: [KeyString] -> Property
getAllStringKeys keys = getAllKeys False keys

getAllIntIntKeys :: [(Int, Int)] -> Property
getAllIntIntKeys keys = getAllTupleKeys keys

getAllStringIntKeys :: [(KeyString, Int)] -> Property
getAllStringIntKeys keys = getAllTupleKeys keys

getAllIntStringKeys :: [(Int, KeyString)] -> Property
getAllIntStringKeys keys = getAllTupleKeys keys

getAllStringStringKeys :: [(KeyString, KeyString)] -> Property
getAllStringStringKeys keys = getAllTupleKeys keys

getNoIntKey :: ![Int] -> Property
getNoIntKey keys = getKeys False False keys none $ const False

getNoIntIntKey :: ![(Int, Int)] -> Property
getNoIntIntKey keys = getKeys False False keys (>& none none) $ const False

getSingleIntKey :: [Int] Int -> Property
getSingleIntKey keys const = getSingleKey False keys const

getSingleIntKeyFromPartitionedMap :: ![Int] !Int -> Property
getSingleIntKeyFromPartitionedMap keys const = getSingleKey True keys const

getSingleStringKey :: [KeyString] KeyString -> Property
getSingleStringKey keys const = getSingleKey False keys const

getSingleIntIntKey :: [(Int, Int)] (Int, Int) -> Property
getSingleIntIntKey keys const = getSingleTupleKey keys const

getSingleStringIntKey :: [(KeyString, Int)] (KeyString, Int) -> Property
getSingleStringIntKey keys const = getSingleTupleKey keys const

getSingleIntStringKey :: [(Int, KeyString)] (Int, KeyString) -> Property
getSingleIntStringKey keys const = getSingleTupleKey keys const

getSingleStringStringKey :: [(KeyString, KeyString)] (KeyString, KeyString) -> Property
getSingleStringStringKey keys const = getSingleTupleKey keys const

getSmallestIntKey :: ![Int] -> Property
getSmallestIntKey keys = getKeys False False keys smallest $ (==) minKey
where
	minKey = 'DF'.minimum keys

getSmallestIntIntKey :: ![(Int, Int)] -> Property
getSmallestIntIntKey keys = getKeys False False keys (>& smallest smallest) $ (==) (minKey1, minKey2)
where
	minKey1 = 'DF'.minimum $ fst <$> keys
	minKey2 = 'DF'.minimum [k2 \\ (k1, k2) <- keys | k1 == minKey1]

getSmallestIntAndSingleIntKey :: ![(Int, Int)] !Int -> Property
getSmallestIntAndSingleIntKey keys key2 = getKeys False  False keys (>& smallest (equals key2)) $ (==) (minKey1, key2)
where
	minKey1 = 'DF'.minimum $ fst <$> keys

getIntKeysObeyingIneq :: [Int] (InequalityCondition Int) -> Property
getIntKeysObeyingIneq keys ineq = getKeysObeyingIneq keys ineq

getStringKeysObeyingIneq :: [KeyString] (InequalityCondition KeyString) -> Property
getStringKeysObeyingIneq keys ineq = getKeysObeyingIneq keys ineq

getIntIntKeysObeyingIneq :: [(Int, Int)] (InequalityCondition Int, InequalityCondition Int) -> Property
getIntIntKeysObeyingIneq keys ineqs = getTupleKeysObeyingIneq keys ineqs

getStringStringKeysObeyingIneq ::
	![(KeyString, KeyString)] !(!InequalityCondition KeyString, !InequalityCondition KeyString) -> Property
getStringStringKeysObeyingIneq keys ineqs = getTupleKeysObeyingIneq keys ineqs

getIntStringKeysObeyingIneq ::
	[(Int, KeyString)] (InequalityCondition Int, InequalityCondition KeyString) -> Property
getIntStringKeysObeyingIneq keys ineqs = getTupleKeysObeyingIneq keys ineqs

getStringIntKeysObeyingIneq ::
	[(KeyString, Int)] (InequalityCondition KeyString, InequalityCondition Int) -> Property
getStringIntKeysObeyingIneq keys ineqs = getTupleKeysObeyingIneq keys ineqs

getIntKeyRange :: [Int] (Int, Int) -> Property
getIntKeyRange keys range = getKeyRange keys range

getStringKeyRange :: [KeyString] (KeyString, KeyString) -> Property
getStringKeyRange keys range = getKeyRange keys range

getIntIntKeyRange :: [(Int, Int)] ((Int, Int), (Int, Int)) -> Property
getIntIntKeyRange keys range = getTupleKeyRange keys range

getStringIntKeyRange :: [(KeyString, Int)] ((KeyString, KeyString), (Int, Int)) -> Property
getStringIntKeyRange keys range = getTupleKeyRange keys range

getIntStringKeyRange :: [(Int, KeyString)] ((Int, Int), (KeyString, KeyString)) -> Property
getIntStringKeyRange keys range = getTupleKeyRange keys range

getStringStringKeyRange :: [(KeyString, KeyString)] ((KeyString, KeyString), (KeyString, KeyString)) -> Property
getStringStringKeyRange keys range = getTupleKeyRange keys range

getIntIntIntKeyRange :: [(Int, Int, Int)] ((Int, Int), (Int, Int), (Int, Int)) -> Property
getIntIntIntKeyRange keys range = getTuple3KeyRange keys range

getStringStringStringKeyRange :: [(KeyString, KeyString, KeyString)] ((KeyString, KeyString), (KeyString, KeyString), (KeyString, KeyString))
                              -> Property
getStringStringStringKeyRange keys range = getTuple3KeyRange keys range

getStringIntStringKeyRange :: [(Int, KeyString, Int)] ((Int, Int), (KeyString, KeyString), (Int, Int)) -> Property
getStringIntStringKeyRange keys range = getTuple3KeyRange keys range

getIntStringIntKeyRange :: [(KeyString, Int, KeyString)] ((KeyString, KeyString), (Int, Int), (KeyString, KeyString)) -> Property
getIntStringIntKeyRange keys range = getTuple3KeyRange keys range

getIntIntIntIntKeyRange :: [(Int, Int, Int, Int)] ((Int, Int), (Int, Int), (Int, Int), (Int, Int)) -> Property
getIntIntIntIntKeyRange keys range = getTuple4KeyRange keys range

getStringIntStringIntKeyRange :: [(KeyString, Int, KeyString, Int)]
                                 ((KeyString, KeyString), (Int, Int), (KeyString, KeyString), (Int, Int))
                              -> Property
getStringIntStringIntKeyRange keys range = getTuple4KeyRange keys range

getIntKeySet :: [Int] [Int] -> Property
getIntKeySet keys setCond = getKeySet keys setCond

getStringKeySet :: [KeyString] [KeyString] -> Property
getStringKeySet keys setCond = getKeySet keys setCond

getIntIntKeySet :: [(Int, Int)] ([Int], [Int]) -> Property
getIntIntKeySet keys cond = getTupleKeySet keys cond

getStringStringKeySet :: [(KeyString, KeyString)] ([KeyString], [KeyString]) -> Property
getStringStringKeySet keys cond = getTupleKeySet keys cond

getIntStringKeySet :: [(Int, KeyString)] ([Int], [KeyString]) -> Property
getIntStringKeySet keys cond = getTupleKeySet keys cond

getStringIntKeySet :: [(KeyString, Int)] ([KeyString], [Int]) -> Property
getStringIntKeySet keys cond = getTupleKeySet keys cond

getIntKeyNearValue :: ![Int] !(Near Int) -> Property
getIntKeyNearValue keys cond = getKeyNearValue keys cond

getStringKeyNearValue :: ![KeyString] !(Near KeyString) -> Property
getStringKeyNearValue keys cond = getKeyNearValue keys cond

getIntIntKeyNearValues :: ![(Int, Int)] !(!Near Int, !Near Int) -> Property
getIntIntKeyNearValues keys conds = getTupleKeyNearValue keys conds

getStringStringKeyNearValues :: ![(KeyString, KeyString)] !(!Near KeyString, !Near KeyString) -> Property
getStringStringKeyNearValues keys conds = getTupleKeyNearValue keys conds

getIntStringKeyNearValues :: ![(Int, KeyString)] !(!Near Int, !Near KeyString) -> Property
getIntStringKeyNearValues keys conds = getTupleKeyNearValue keys conds

getStringIntKeyNearValues :: ![(KeyString, Int)] !(!Near KeyString, !Near Int) -> Property
getStringIntKeyNearValues keys conds = getTupleKeyNearValue keys conds

getIntIntIntKeyNearValues :: ![(Int, Int, Int)] !(!Near Int, !Near Int, !Near Int) -> Property
getIntIntIntKeyNearValues keys conds = getTuple3KeyNearValues keys conds

getSingleIntAndAllIntKeys :: [(Int, Int)] Int -> Property
getSingleIntAndAllIntKeys keys const = getKeys False False keys (>& (equals const) all) $ (==) const o fst

getAllIntIntKeysNear :: ![(Int, Int)] !(Near Int) -> Property
getAllIntIntKeysNear keys cond2 =
	getKeys False False keys (>& all (nearConditionFor cond2)) (\(key1, key2) -> ?Just key2 == expectedKey2For key1)
where
	expectedKey2For key1 = keyNearValue (fmap snd $ filter (\(key1`, _) -> key1` == key1) keys) cond2

getSingleIntIntKeysNear :: ![(Int, Int)] !Int !(Near Int) -> Property
getSingleIntIntKeysNear keys cond1 cond2 = getKeys
	False  False keys (>& (equals cond1) (nearConditionFor cond2))
	(\(key1, key2) -> key1 == cond1 && ?Just key2 == expectedKey2)
where
	expectedKey2 = keyNearValue (fmap snd $ filter (\(key1, _) -> key1 == cond1) keys) cond2

getIntKeysNearAllIntKeys :: ![(Int, Int)] !(Near Int) -> Property
getIntKeysNearAllIntKeys keys cond1 =
	getKeys False False keys (>& (nearConditionFor cond1) all) (\(key1, _) -> ?Just key1 == expectedKey1)
where
	expectedKey1 = keyNearValue (fst <$> keys) cond1

getSingleIntKeyIntKeysNearSingleIntKey :: ![(Int, Int, Int)] !Int !(Near Int)  !Int -> Property
getSingleIntKeyIntKeysNearSingleIntKey keys cond1 cond2 cond3 = getKeys
	False  False keys (>>& (equals cond1) (nearConditionFor cond2) (equals cond3))
	(\(key1, key2, key3) -> key1 == cond1 && ?Just key2 == expectedKey2For key1 && key3 == cond3)
where
	expectedKey2For key1 = keyNearValue (fmap snd3 $ filter (\(key1`, _, _) -> key1` == key1) keys) cond2

getAllIntOrAllIntKeys :: ![Int] -> Property
getAllIntOrAllIntKeys keys = getKeysForDisjunction keys all (const True) all (const True)

getNoIntOrNoIntKey :: ![Int] -> Property
getNoIntOrNoIntKey keys = getKeysForDisjunction keys none (const False) none (const False)

getSingleIntOrSingleIntKey :: ![Int] !Int !Int -> Property
getSingleIntOrSingleIntKey keys const1 const2 =
	getKeysForDisjunction keys (equals const1) ((==) const1) (equals const2) ((==) const2)

getIntKeysObeyingIneqOrObeyingIneq :: ![Int] !(InequalityCondition Int) !(InequalityCondition Int) -> Property
getIntKeysObeyingIneqOrObeyingIneq keys cond1 cond2 =
	getKeysForDisjunction keys (ineqConditionFor cond1) (ineqHolds cond1) (ineqConditionFor cond2) (ineqHolds cond2)

getIntKeyRangeOrKeyRange :: ![Int] !(!Int, !Int) !(!Int, !Int) -> Property
getIntKeyRangeOrKeyRange keys (lower1, upper1) (lower2, upper2) =
	getKeysForDisjunction
		keys
		(isBetween lower1 upper1) (\k -> k >= lower1 && k <= upper1)
		(isBetween lower2 upper2) (\k -> k >= lower2 && k <= upper2)

getIntKeySetOrKeySet :: ![Int] ![Int] ![Int] -> Property
getIntKeySetOrKeySet keys list1 list2 =
	getKeysForDisjunction keys (isIn set1) (\k -> 'Set'.member k set1) (isIn set2) (\k -> 'Set'.member k set2)
where
	set1 = 'Set'.fromList list1
	set2 = 'Set'.fromList list2

getSmallestIntOrSmallestIntKey :: ![Int] -> Property
getSmallestIntOrSmallestIntKey keys = getKeysForDisjunction keys smallest ((==) minInt) smallest ((==) minInt)
where
	minInt = 'DF'.minimum keys

getIntKeyNearValueOrNearValue :: ![Int] !(Near Int) !(Near Int) -> Property
getIntKeyNearValueOrNearValue keys cond1 cond2 =
	getKeysForDisjunction
		keys
		(nearConditionFor cond1) ((==) (keyNearValue keys cond1) o ?Just)
		(nearConditionFor cond2) ((==) (keyNearValue keys cond2) o ?Just)

getIntKeysNearValueOrObeyingIneq :: ![Int] !(Near Int) !(InequalityCondition Int) -> Property
getIntKeysNearValueOrObeyingIneq keys cond1 cond2 =
	getKeysForDisjunction
		keys
		(nearConditionFor cond1) ((==) (keyNearValue keys cond1) o ?Just) (ineqConditionFor cond2) (ineqHolds cond2)

getIntKeysObeyingIneqOrNearValue :: ![Int] !(InequalityCondition Int) !(Near Int) -> Property
getIntKeysObeyingIneqOrNearValue keys cond1 cond2 = getKeysForDisjunction
	keys (ineqConditionFor cond1) (ineqHolds cond1) (nearConditionFor cond2) ((==) (keyNearValue keys cond2) o ?Just)

getIntKeysNearValueOrInRange :: ![Int] !(Near Int) !(!Int, !Int) -> Property
getIntKeysNearValueOrInRange keys cond1 (first, last) =
	getKeysForDisjunction
		keys
		(nearConditionFor cond1) ((==) (keyNearValue keys cond1) o ?Just)
		(isBetween first last)   (\k -> k >= first && k <= last)

getIntKeysInRangeOrNearValue :: ![Int] !(!Int, !Int) !(Near Int) -> Property
getIntKeysInRangeOrNearValue keys (first, last) cond2 =
	getKeysForDisjunction
		keys
		(isBetween first last)   (\k -> k >= first && k <= last)
		(nearConditionFor cond2) ((==) (keyNearValue keys cond2) o ?Just)

getIntKeyNearValueOrNoIntKey :: ![Int] !(Near Int) -> Property
getIntKeyNearValueOrNoIntKey keys cond1 =
	getKeysForDisjunction keys (nearConditionFor cond1) ((==) (keyNearValue keys cond1) o ?Just) none (const False)

getNoOrIntKeyNearValue :: ![Int] !(Near Int) -> Property
getNoOrIntKeyNearValue keys cond2 =
	getKeysForDisjunction keys none (const False) (nearConditionFor cond2) ((==) (keyNearValue keys cond2) o ?Just)

getKeysForDisjunction ::
	![k] !(Condition k) !(k -> Bool) !(Condition k) !(k -> Bool) -> Property
	| TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, key k
getKeysForDisjunction keys cond1 pred1 cond2 pred2 =
	getKeys False  False keys (cond1 OR cond2) (\key -> pred1 key || pred2 key)

getIntIntKeysEqToOrNearValueForFirstKey :: ![(Int, Int)] !Int !(Near Int) -> Property
getIntIntKeysEqToOrNearValueForFirstKey keys cond1 cond2 = getKeys
	False False keys (>& (equals cond1 OR nearConditionFor cond2) all)
	(\(key1, _) -> key1 == cond1 || ?Just key1 == expectedKey1)
where
	expectedKey1 = keyNearValue (fst <$> keys) cond2

getIntIntKeysEqToOrNearValueForSecondKey :: ![(Int, Int)] !Int !(Near Int) -> Property
getIntIntKeysEqToOrNearValueForSecondKey keys cond1 cond2 = getKeys
	False False keys (>& all (equals cond1 OR nearConditionFor cond2))
	(\(key1, key2) -> key2 == cond1 || ?Just key2 == expectedKeyFor2 key1)
where
	expectedKeyFor2 key1` = keyNearValue [key2 \\ (key1, key2) <- keys | key1 == key1`] cond2

getSingleIntKeyAndValuesOfMultiMap :: ![(Int, Int)] !Int -> Property
getSingleIntKeyAndValuesOfMultiMap keysAndValues cond = getSingleKeyAndValuesOfMultiMap keysAndValues cond

getIntKeysNearValueAndValuesOfMultiMap :: ![(Int, Int)] !(Near Int) -> Property
getIntKeysNearValueAndValuesOfMultiMap keysAndValues cond = getKeysAndValuesOfMultiMap
	keysAndValues (nearConditionFor cond) (\key -> ?Just key == keyNearValue [key \\ (key, _) <- keysAndValues] cond)

getAllIntKeysOrNearValueAndValuesOfMultiMap :: ![(Int, Int)] !(Near Int) -> Property
getAllIntKeysOrNearValueAndValuesOfMultiMap keysAndValues cond = getKeysAndValuesOfMultiMap
	keysAndValues (disjunctionOf [!all, nearConditionFor cond!]) (\_ = True)

getIntKeyRangeInParallel :: ![!(Int, Char)] !(!Int, !Int) !(?Int) -> Property
getIntKeyRangeInParallel keys range stopOn = getKeyRangeInParallel keys range stopOn

getIntKeyRangeFromMultiInParallel :: ![!(Int, Char)] !(!Int, !Int) !(?Int) -> Property
getIntKeyRangeFromMultiInParallel keys range stopOn = getKeyRangeInParallelMulti keys range stopOn

getSingleLargeValueFromMap :: Property
getSingleLargeValueFromMap =: getSingleKeyAndValue "0" ?None largeRandomString

// Tests whether replacing a large value with another one works correctly, if the new value is a multiple of the chunk
// size. We cover the cases 1x (the value is stored entirely in the original map) and 2x (the value is partially stored)
// in the large-values map).
getSingleReplacedLargeValueFromMap :: !Bool -> Property
getSingleReplacedLargeValueFromMap twiceTheSize = getSingleKeyAndValue
	"0" (?Just largeRandomString) (largeRandomStringOfLength $ MAX_CHUNK_SIZE * if twiceTheSize 2 1)

getLargeValuesFromMap :: Property
getLargeValuesFromMap =: getKeysAndValues [("0", largeRandomString), ("1", largeRandomString)] all (const True)

getDataFromMapsUsingContinuationChain :: ![!(Char, Char)] ![!(Char, Char)] -> Property
getDataFromMapsUsingContinuationChain testDataMapOne testDataMapTwo = getKeysAndValuesUsingContinuationChain
	all (const True) testDataMapOne testDataMapTwo

largeRandomString :: String
largeRandomString =: largeRandomStringOfLength 16000

largeRandomStringOfLength :: !Int -> String
// Some characters are changed to make sure the order of the value returned is correct.
largeRandomStringOfLength n = {toChar i \\ i <- take n $ genRandInt 1}

/**
 * This checks whether the process pool is able to deal with accumulator states of different types.
 * Depending on the `Bool` flag two operations with stats of different types are performed within the same transaction
 * or in two separate transactions.
 */
accumulateInParallelDifferentTypes :: !Bool -> Property
accumulateInParallelDifferentTypes twoTransactions = withTestCleanDB (ParallelOperationsEnabled $ ?Just 2) property
where
	property :: !*CDB -> (!Property, !*CDB)
	property cdb
		# (_, cdb) = doTransaction (\txn -> ((), Foldl (\txn n = put n () testMap txn) txn [!1..nrKeys])) cdb
		# ((lastStr, accSum), cdb) = if twoTransactions (doTwoTransactions cdb) (doReadOnlyTransaction twoGets cdb)
		= (lastStr =.= toString stopAt /\ accSum =.= Sum [!1..stopAt], cdb)

	doTwoTransactions :: !*CDB -> (!(!String, !Int), !*CDB)
	doTwoTransactions db
		# (lastStr, db) = doReadOnlyTransaction getFirst db
		# (sum, db) = doReadOnlyTransaction getSecond db
		= ((lastStr, sum), db)

	twoGets :: !*ReadOnlyTxn -> (!(!String, !Int), !*ReadOnlyTxn)
	twoGets txn
		# (lastStr, txn) = getFirst txn
		# (sum, txn) = getSecond txn
		= ((lastStr, sum), txn)

	getFirst :: !*ReadOnlyTxn -> (!String, !*ReadOnlyTxn)
	getFirst txn
		# (lastStrs, txn) = getAccumNestedParallel
			all (\n _ st = (False, ?Just n, st)) (\n txn = (n, txn)) (\n _ txn = (n == stopAt, [toString n], txn)) ()
			[] testMap (?Just 2) txn
		= (hd lastStrs, txn)

	getSecond :: !*ReadOnlyTxn -> (!Int, !*ReadOnlyTxn)
	getSecond txn = getAccumNestedParallel
		all (\n _ st = (False, ?Just n, st)) (\n txn = (n, txn)) (\n sum txn = (n == stopAt, sum + n, txn)) () 0
		testMap (?Just 2) txn

	testMap = 'CDB.KeyEncoding'.dbMap "intMap"

	nrKeys = 1000000
	stopAt = nrKeys / 2

deleteCharKeysFromMapGetAccumDelete :: ![(Char, Char)] ![Char] -> Property
deleteCharKeysFromMapGetAccumDelete keyVals deletedKeys = deleteKeysFromMapGetAccumDelete keyVals deletedKeys

deleteCharKeysFromMap :: ![(Char, Char)] ![Char] -> Property
deleteCharKeysFromMap keyVals deletedKeys = deleteKeysFromMap keyVals deletedKeys

deleteKeysFromMap ::
	![(k, v)] ![k] -> Property | key, gPrint{|*|}, genShow {|*|}, Eq, < k & key, Eq, genShow{|*|}, gPrint{|*|} v
deleteKeysFromMap keyValues deletedKeys = deleteKeysFromMapUsing
	(\testMap cdb = foldl (\cdb key = snd $ doTransaction (\txn = ((), delete key testMap txn)) cdb) cdb deletedKeys)
	keyValues deletedKeys

deleteKeysFromMapGetAccumDelete ::
	![(k, v)] ![k] -> Property | key, gPrint{|*|}, genShow {|*|}, Eq, < k & key, Eq, genShow{|*|}, gPrint{|*|} v
deleteKeysFromMapGetAccumDelete keyValues deletedKeys = deleteKeysFromMapUsing
	(\testMap =
		snd o doTransaction
			(getAccumDelete (isIn $ 'Set'.fromList deletedKeys) (\_ _ acc = (False, True, acc)) () testMap))
	keyValues deletedKeys

deleteKeysFromMapUsing ::
	!((DBMap k v) *CDB -> *CDB) ![(k, v)] ![k] -> Property
	| key, gPrint{|*|}, genShow {|*|}, Eq, < k & key, Eq, genShow{|*|}, gPrint{|*|} v
deleteKeysFromMapUsing delOperation keyValues deletedKeys = withTestCleanDB ParallelOperationsDisabled cdbFunc
where
	cdbFunc :: !*CDB -> (!Property, !*CDB)
	cdbFunc cdb
		// Do the operations on a DB map.
		# cdb =
			foldl (\cdb (key, value) -> snd $ doTransaction (\txn -> ((), put key value testMap txn)) cdb) cdb keyValues
		# cdb = delOperation testMap cdb
		// Do the same operations on an ordinary map.
		# map = foldl (\map (key, value) -> 'Map'.put key    value map) 'Map'.newMap keyValues
		# map = foldl (\map key          -> 'Map'.del key          map) map          deletedKeys
		= doReadOnlyTransaction (checkEqualityOfMap map testMap) cdb

	testMap = 'CDB.KeyEncoding'.dbMap "testMap"

deleteLargeValuesFromMapResultsInLargeValueMapValuesBeingDeleted :: Property
deleteLargeValuesFromMapResultsInLargeValueMapValuesBeingDeleted =: deleteLargeValuesWith [!15000..16000] delete

deleteAccumLargeValuesFromMapResultsInLargeValueMapValuesBeingDeleted :: Property
deleteAccumLargeValuesFromMapResultsInLargeValueMapValuesBeingDeleted =: deleteLargeValuesWith
	[!15000..16000] (\_ = deleteFunc)
where
	deleteFunc :: !(DBMap Int String) !*Txn -> *Txn
	// The OR is to generate a condition with generates suspended runs (see `executeCond`), this is a special
	// case for which large values may also need to be deleted and therefore doing so makes the test stronger.
	deleteFunc map txn = snd $ getAccumDelete
		(all OR smallestGreaterThanOrEq 0) (\_ _ st = (False, True, st)) () map txn

deleteLargeValuesWith :: ![!Int] !(Int (DBMap Int String) *Txn -> *Txn) -> Property
deleteLargeValuesWith strLengths f =
	withTestCleanDB ParallelOperationsDisabled (doTransaction testTxn)
where
	testTxn :: !*Txn -> (!Property, !*Txn)
	testTxn txn
		# txn = Foldl (\txn strLength
			# txn = put strLength (largeRandomStringOfLength strLength) testMap txn
			= f strLength testMap txn
			) txn strLengths
		# (Txn txnInfo) = txn
		# ((largeValueMap, txnPtr), txnInfo) = appFst
			((\{largeValueMap, txnPtr} = (fromJust largeValueMap, txnPtr)) o fromJust) $ mapInfoOf testMap txnInfo
		# (valuesInLargeValueMap, txnInfo) =
			Foldl
				(\(acc, txnInfo) strLength
					# (largeValues, txnInfo)
						= readValueFromLargeValueMap
							largeValueMap (toString $ strRepresentation strLength) txnPtr txnInfo
					= (acc ++ largeValues, txnInfo))
				([], txnInfo) strLengths
		= (valuesInLargeValueMap =.= [], Txn txnInfo)

	testMap = 'CDB.KeyEncoding'.dbMap "largeValueTestMap"

deleteCharKeysFromMultiMap :: ![(Char, Char)] ![Char] -> Property
deleteCharKeysFromMultiMap keyValues deletedKeys = deleteCharKeysFromMultiMapUsing
	(\testMap cdb = foldl (\cdb key -> snd $ doTransaction (\txn -> ((), delete key testMap txn)) cdb) cdb deletedKeys)
	keyValues deletedKeys

deleteCharKeysFromMultiMapGetMultiAccumDeleteIsIn :: ![(Char, Char)] ![Char] -> Property
deleteCharKeysFromMultiMapGetMultiAccumDeleteIsIn keyValues deletedKeys = deleteCharKeysFromMultiMapUsing
	(\testMap =
		snd o doTransaction
			(getMultiAccumDelete (isIn $ 'Set'.fromList deletedKeys) (\_ _ acc = (False, True, acc)) () testMap))
	keyValues deletedKeys

deleteCharKeysFromMultiMapGetMultiAccumDeleteAll :: ![(Char, Char, Bool)] -> Property
deleteCharKeysFromMultiMapGetMultiAccumDeleteAll keyValues = deleteCharKeysFromMultiMapUsingSingleList
	(\testMap =
		snd o doTransaction
			(getMultiAccumDelete all
				(\k _ acc = (False, thd3 $ fromJust $ find (\(k`, _, _) = k == k`) keyValues, acc)) () testMap))
	keyValues

deleteCharKeysFromMultiMapUsing :: !((DBMultiMap Char Char) *CDB -> *CDB) ![(Char, Char)] ![Char] -> Property
deleteCharKeysFromMultiMapUsing delOperation keyValues deletedKeys = withTestCleanDB ParallelOperationsDisabled cdbFunc
where
	cdbFunc :: !*CDB -> (!Property, !*CDB)
	cdbFunc cdb
		// Do the operations on a DB map.
		# cdb =
			foldl (\cdb (key, value) -> snd $ doTransaction (\txn -> ((), put key value testMap txn)) cdb) cdb keyValues
		# cdb = delOperation testMap cdb
		// Do the same operations on an ordinary map.
		# map = foldl (\map (key, value) ->
			'Map'.alter (?Just o maybe ('Set'.singleton value) ('Set'.insert value)) key map) 'Map'.newMap keyValues
		# map = foldl (\map key -> 'Map'.del key map) map deletedKeys
		= doReadOnlyTransaction (checkEqualityOfMultiMap map testMap) cdb

	testMap = 'CDB.KeyEncoding'.dbMultiMap "charMultiMap"

deleteCharKeysFromMultiMapUsingSingleList :: !((DBMultiMap Char Char) *CDB -> *CDB) ![(Char, Char, Bool)] -> Property
deleteCharKeysFromMultiMapUsingSingleList delOperation keyValues = withTestCleanDB ParallelOperationsDisabled cdbFunc
where
	cdbFunc :: !*CDB -> (!Property, !*CDB)
	cdbFunc cdb
		// Do the operations on a DB map.
		# cdb =
			foldl (\cdb (key, value, _) -> snd $ doTransaction (\txn -> ((), put key value testMap txn)) cdb) cdb keyValues
		# cdb = delOperation testMap cdb
		// Do the same operations on an ordinary map.
		# map = foldl (\map (key, value, del) ->
			'Map'.alter
				(\v = if (not del) (?Just $ maybe ('Set'.singleton value) ('Set'.insert value) v) v) key map)
				'Map'.newMap keyValues
		= doReadOnlyTransaction (checkEqualityOfMultiMap map testMap) cdb

	testMap = 'CDB.KeyEncoding'.dbMultiMap "charMultiMap"

deleteCharKeyValuesFromMultiMap :: ![(Char, Char)] ![(Char, Char)] -> Property
deleteCharKeyValuesFromMultiMap keyValues deletedKeys = withTestCleanDB ParallelOperationsDisabled cdbFunc
where
	cdbFunc :: !*CDB -> (!Property, !*CDB)
	cdbFunc cdb
		// Do the operations on a DB map.
		# cdb =
			foldl (\cdb (key, value) -> snd $ doTransaction (\txn -> ((), put key value testMap txn)) cdb) cdb keyValues
		# cdb =
			foldl
				(\cdb (key, value) -> snd $ doTransaction (\txn -> ((), deleteFromMulti key value testMap txn)) cdb)
				cdb
				deletedKeys
		// Do the same operations on an ordinary map.
		# map = foldl (\map (key, value) ->
			'Map'.alter (?Just o maybe ('Set'.singleton value) ('Set'.insert value)) key map) 'Map'.newMap keyValues
		# map = foldl (\map (key, value) -> 'Map'.alter (fmap $ 'Set'.delete value) key map) map deletedKeys
		= doReadOnlyTransaction (checkEqualityOfMultiMap map testMap) cdb

	testMap = 'CDB.KeyEncoding'.dbMultiMap "charMultiMap"

deleteAndThenGetCharKeysNearValue :: ![Char] ![Char] !(Near Char) -> Property
deleteAndThenGetCharKeysNearValue keyValues deletedKeys cond = withTestCleanDB ParallelOperationsDisabled cdbFunc
where
	cdbFunc :: !*CDB -> (!Property, !*CDB)
	cdbFunc cdb
		# cdb = foldl (\cdb key -> snd $ doTransaction (\txn -> ((), put key () testMap txn)) cdb) cdb keyValues
		// we delete and retrieve in the same transaction
		// to test whether the transaction administration is updated properly (see !96)
		# (readKeys, cdb) =
			doTransaction
				(\txn
					# txn = foldl (\txn key -> delete key testMap txn) txn deletedKeys
					= get (nearConditionFor cond) testMap txn
				)
				cdb
		= (filter (\val -> ?Just val == keyNearValue keysLeft cond) keysLeft =.= (fst <$> readKeys), cdb)

	testMap = 'CDB.KeyEncoding'.dbMap "charMap"
	keysLeft = 'Set'.toList $ foldl (flip 'Set'.delete) ('Set'.fromList keyValues) deletedKeys

delPartition :: Property
delPartition = withTestCleanDB ParallelOperationsDisabled $ doTransaction property
where
	property :: !*Txn -> (!Property, !*Txn)
	property txn
		# txn = put testKey 1 map txn
		# txn = deletePartition testPart txn
		# (vals, txn) = getSingleMulti testKey map txn
		= (prop $ isEmpty vals, txn)

	map :: DBMultiMap String Int
	map = withSelectedPartition testPart $ partitionedMap intMultiMap

	testPart = "some part"
	testKey = "key"

//* Adds a large number of string values and checks if reading them again provides the values written.
addAndCheckStringValues :: Property
addAndCheckStringValues = withTestCleanDB ParallelOperationsDisabled $ doTransaction (addAndGet 0)
where
	addAndGet :: !Int !*Txn -> (!Property, !*Txn)
	addAndGet i txn
		| i > NR_ELEMENTS = (prop True, txn)
		#! istr = toString i
		#! txn = put istr istr testMap txn
		#! (read, txn) = getSingle istr testMap txn
		| isNone read = (prop False, txn)
		#! read = fromJust read
		= if (read == istr) (addAndGet (i + 1) txn) (prop False, txn)

	testMap = dbMap "stringMap" id id

	NR_ELEMENTS  = 10000000

// Adds values to a multi map where the values have a fixed size and reads them.
addAndCheckdbMultiMapFixedSize :: ![Char] -> Property
addAndCheckdbMultiMapFixedSize chars = testMultiMapValueSize fixedMultiMap chars

// Add integer values to a multi map where the values are encoded as integers.
addAndCheckValsToIntValMultiMap :: ![Int] -> Property
addAndCheckValsToIntValMultiMap keys = testMultiMapValueSize intMultiMap keys

intMultiMap :: DBMultiMap String Int
intMultiMap = dbMultiMapIntValues "stringIntMultiMap" id id

// Char has a fixed length keyEncoding so the values of the resulting multimap are determined to be fixed size.
fixedMultiMap :: DBMultiMap String Char
fixedMultiMap = 'CDB.KeyEncoding'.dbMultiMap "stringCharMultiMap"

testMultiMapValueSize :: !(DBMultiMap String a) ![a] -> Property | Ord, Eq, gPrint{|*|}, genShow{|*|} a
testMultiMapValueSize testMultiMap expectedValues =
	withTestCleanDB ParallelOperationsDisabled $ doTransaction addAndCheck
where
	addAndCheck txn
		| isEmpty expectedValues = (prop True, txn)
		# txn = foldl (\txn n -> put key n testMultiMap txn) txn expectedValues
		# (actualValues, txn) = getSingleMulti key testMultiMap txn
		= ((removeDup $ sort expectedValues) =.= actualValues, txn)
	key = "key"
