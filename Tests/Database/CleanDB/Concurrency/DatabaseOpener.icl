module DatabaseOpener

/**
 * Opens and immeditelly closes the database rapidly. A transaction is started as this can cause crashes when the
 * implementation does not make sure that the LMDB database is not opened by multiple processes at the same time.
 * This is intended to be run multiple times concurrently.
 */

import StdEnv => qualified all
import StdOverloadedList
import System.CommandLine

import Database.CleanDB

Start world
	# (commandLine, world) = getCommandLine world
	= Foldl (openAndStartTxn $ commandLine !! 1) world [!1..nrIterations]

openAndStartTxn :: !FilePath !*World x -> *World
openAndStartTxn path world _ =
	snd $ withDatabase
		path defaultOptions
		(\cdb world
			# (_, cdb) = doTransaction (\txn -> ((), txn)) cdb
			= ((), cdb, world)
		)
		world

nrIterations = 30000
