#! /bin/bash

DB_PATH="/tmp/cleantest/cleanDb/concurrency"

rm -rf $DB_PATH

test/opener DB_PATH & opener1Proc=$!;
test/opener DB_PATH & opener2Proc=$!;
wait $opener1Proc;
rc1=$?;
wait $opener2Proc;
rc2=$?;

if [ $rc1 -ne 0 ] || [ $rc2 -ne 0 ];then
	echo "concurrently opening database test failed";
	exit 1;
fi
