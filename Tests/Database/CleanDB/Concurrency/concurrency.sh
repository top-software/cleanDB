#! /bin/bash
# Usage: concurrency RETRIEVE_IN_PARALLEL
# Example: `concurrency true` retrieves the data in parallel.
# Example: `concurrency false` does not retrieve the data in parallel.

NR_TRANSACTIONS=10000
DB_PATH="/tmp/cleantest/cleanDb/concurrency"

rm -rf $DB_PATH
test/consumer $DB_PATH $NR_TRANSACTIONS $1 & consumerProc=$!;
test/producer $DB_PATH $NR_TRANSACTIONS & producerProc=$!;
wait $consumerProc;
rcConsumer=$?;
if [ $rcConsumer -ne 0 ];then
	echo "concurrency test failed (retrieve in parallel = $1)";
	echo "consumer exited with $rcConsumer";
	exit $rcConsumer;
fi;
wait $producerProc;
rcProducer=$?;
if [ $rcProducer -ne 0 ];then
	echo "concurrency test failed (retrieve in parallel = $1)";
	echo "producer exited with $rcProducer";
	exit $rcProducer;
fi;
if [ $rcConsumer -eq 0 ] && [ $rcConsumer -eq 0 ];then
	echo "concurrency test passed (retrieve in parallel = $1)";
fi
