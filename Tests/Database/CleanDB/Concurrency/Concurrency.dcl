definition module Concurrency

/**
 * Provides common definitions for the concurrency test.
 */

from Database.CleanDB import :: DBMultiMap

testMap :: DBMultiMap Int Int
