implementation module Concurrency

import Database.CleanDB
import qualified Database.CleanDB.KeyEncoding as CDB.KeyEncoding
import StdEnv => qualified all

testMap :: DBMultiMap Int Int
testMap =: dbMultiMapIntValues "test" id id
