module Consumer

/**
 * This repeats a read transaction until the expected result is achieved.
 */

import StdEnv => qualified all
import System.CommandLine
import Database.CleanDB
import Concurrency

Start world
	# (commandLine, world) = getCommandLine world
	# retrieveInPar = commandLine !! 3 == "true"
	# parOpt =
		if retrieveInPar (ParallelOperationsEnabled $ ?Just nrConcurrentTransactions) ParallelOperationsDisabled
	= withDatabase
		(commandLine !! 1) {defaultOptions & parallelOperationsSetting = parOpt}
		(dbFunc retrieveInPar 0 $ toInt $ commandLine !! 2) world

dbFunc :: !Bool !Int !Int !*CDB !*World -> ((), !*CDB, !*World)
dbFunc retrieveInPar counter n cdb world
	| counter > n * 10000 = abort "transaction limit reached without seeing the expected result\n"
	# (sum, cdb) =
		doReadOnlyTransaction
			(if retrieveInPar
				(getMultiAccumNestedParallel
					(largestLessThanOrEq $ n + 1) (\k v st = (False, ?Just (k, v), st))
					(\(k, v) txn
						// Retrieve and discard `vals` to do some operation in the nested transaction.
						# (_, txn) = getSingleMulti k testMap txn
						= (v, txn))
					(\v acc txn = (False, acc + v, txn)) () 0 testMap (?Just nrConcurrentTransactions) )
				(getMultiAccum (largestLessThanOrEq $ n + 1) (withoutStop $ \_ val -> (+) val) 0 testMap))
			cdb
	| sum == n + 1 = ((), cdb, world)
	= dbFunc retrieveInPar (inc counter) n cdb world

// Use a high number of concurrent processes to test this situation.
nrConcurrentTransactions = 128
