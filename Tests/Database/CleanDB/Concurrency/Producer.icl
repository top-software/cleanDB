module Producer

/**
 * Performs consecutive write transactions. Each `put` in done in a single transaction for this test.
 */

import StdEnv => qualified all
import System.CommandLine
import Database.CleanDB
import Concurrency

Start world
	# (commandLine, world) = getCommandLine world
	= withDatabase (commandLine !! 1) defaultOptions (dbFunc $ toInt $ commandLine !! 2) world

dbFunc :: !Int !*CDB !*World -> ((), !*CDB, !*World)
dbFunc n cdb world
	# cdb = foldl (\cdb k -> snd $ doTransaction (\txn -> ((), put k 1 testMap $ put k k testMap txn)) cdb) cdb [1..n]
	= ((), cdb, world)
