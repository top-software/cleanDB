module Geo

import StdEnv => qualified all
import Data.Functor
from Data.Foldable import class Foldable
import Data.Geo, Data.Geo.Gast, Data.Tuple, Data.Int
import Data.Maybe
from Data.Set import :: Set, instance Foldable Set
import qualified Data.Set      as Set
import qualified Data.Foldable as DF
import Data.Geo._S2
import Control.GenBimap
import System.Time, System._Unsafe
import Text.GenPrint
import Gast, Gast.CommandLine, Gast.Gen
import _SystemStrictLists

import Database.CleanDB.Gast, Database.CleanDB, Database.CleanDB.Geo

Start world = exposeProperties [OutputTestEvents] [Bent] properties world

properties :: [Property]
properties =
	[ getAllPositionKeys                            as "CleanDB.Geo: get all Position keys"
	, getAllTimestampPositionKeys                   as "CleanDB.Geo: get all (Timestamp, Position) keys"
	, (\pos -> getPositionKeysInCircularArea [pos]) as "CleanDB.Geo: get Position key in circular area"
	, getPositionKeysInCircularArea                 as "CleanDB.Geo: get Position keys in circular area"
	, (\pos -> getPositionKeysInTriangleArea [pos]) as "CleanDB.Geo: get Position key in triangle area"
	, getPositionKeysInTriangleArea                 as "CleanDB.Geo: get Position keys in triangle area"
	, (\pos -> getPositionKeysInCircularAndTriangleArea [pos])
	  as "CleanDB.Geo: get Position key in circular or triangle area"
	, getPositionKeysInCircularAndTriangleArea      as "CleanDB.Geo: get Position keys in circuar or triangle area"
	, getTimestampPositionKeyInTriangleArea         as "CleanDB.Geo: get (Timestamp, Position) key in triangle area"
	, getTimestampPositionKeyInTimePeriod           as "CleanDB.Geo: get (Timestamp, Position) key in time period"
	, getTimestampPositionKeyInTriangleAreaAndTimePeriod
	  as "CleanDB.Geo: get (Timestamp, Position) key in time period & triangle area"
	, getTimestampPositionKeysInCellIdRangesOfCircularArea
	  as "CleanDB.Geo: get (Timestamp, Position) keys in cell ID ranges of circular area"
	]

getAllPositionKeys :: [Position] -> Property
getAllPositionKeys keys = getAllKeys False keys

getAllTimestampPositionKeys :: [(Timestamp, Position)] -> Property
getAllTimestampPositionKeys keys = getAllTupleKeys keys

getPositionKeysInCircularArea :: [Position] Position Real -> Property
getPositionKeysInCircularArea positions center radius =
	isJust circ ==> getPositionKeys positions ('Set'.singleton $ fromJust circ)
where
	circ = circle center radius

getPositionKeysInTriangleArea :: [Position] Position Position Position -> Property
getPositionKeysInTriangleArea positions area1 area2 area3 =
	isJust poly ==> getPositionKeys positions ('Set'.singleton $ fromJust poly)
where
	poly = polygon [area1, area2, area3]

getPositionKeysInCircularAndTriangleArea :: [Position] Position Real Position Position Position -> Property
getPositionKeysInCircularAndTriangleArea position center radius point1 point2 point3 =
	(isJust circ && isJust poly) ==> getPositionKeys position ('Set'.fromList [fromJust circ, fromJust poly])
where
	circ = circle center radius
	poly = polygon [point1, point2, point3]

getPositionKeys :: ![Position] !(Set Region) -> Property
getPositionKeys positions regions = getKeys
	False False ((\pos -> (idOfCellIncluding pos, pos)) <$> positions)
	(>& (cellIsInOneOf regions) (positionIsInOneOf regions))
	(\(_, pos) -> 'DF'.any (\region -> pos isInsideOf region) regions)

getTimestampPositionKeyInTriangleArea :: Timestamp Position Position Position Position -> Property
getTimestampPositionKeyInTriangleArea timePoint pos area1 area2 area3 =
		isJust mbRegion
	==>
		let region = fromJust mbRegion in
		getKeys
			False False [(timePoint, idOfCellIncluding pos, pos)] (>>& all (cellIsIn region) (positionIsIn region))
			(\(_, _, pos) -> pos isInsideOf region)
where
	mbRegion = polygon [area1, area2, area3]

getTimestampPositionKeyInTimePeriod :: Timestamp Position Timestamp Timestamp -> Property
getTimestampPositionKeyInTimePeriod timePoint pos first last = getKeys
	False False [(timePoint, idOfCellIncluding pos, pos)] (>>& (isBetween first last) all all)
	(\(timePoint, _, _) -> timePoint >= first && timePoint <= last)

getTimestampPositionKeyInTriangleAreaAndTimePeriod ::
    Timestamp Position Timestamp Timestamp Position Position Position -> Property
getTimestampPositionKeyInTriangleAreaAndTimePeriod timePoint pos first last area1 area2 area3 =
		isJust mbRegion
	==>
		let region = fromJust mbRegion in
		getKeys
			False False [(timePoint, idOfCellIncluding pos, pos)]
			(>>& (isBetween first last) (cellIsIn region) (positionIsIn region))
			(\(timePoint, _, pos) -> timePoint >= first && timePoint <= last && pos isInsideOf region)
where
	mbRegion = polygon [area1, area2, area3]

getTimestampPositionKeysInCellIdRangesOfCircularArea :: ![(Timestamp, Position)] !Position !Real -> Property
getTimestampPositionKeysInCellIdRangesOfCircularArea positions center radius =
		isJust region
	==>
		getKeys
			False False positionCellIds (>& all (cellIsIn $ fromJust region))
			(\(_, CellId cellId) -> any (\(l, u) -> cellId >= l && cellId <= u) regionCellIdRanges)
where
	positionCellIds    = appSnd idOfCellIncluding <$> positions
	region             = circle center radius
	regionCellIdRanges = coveringCellIdRanges (fromJust region).s2Region

instance key Timestamp where
	strRepresentation (Timestamp t) = KeyStr intKeyStr
	where
		(KeyStr intKeyStr) = strRepresentation t

	fromStrRepresentation keyStr = Timestamp (fromStrRepresentation $ KeyStr intKeyStr)
	where
		(KeyStr intKeyStr) = keyStr

	hasFixedSize _ = False

instance primitiveKey Timestamp where
	keyLength = unsafeCoerce intLength
	where
		intLength :: KeyLength Int
		intLength = keyLength

	nextLargerKey (Timestamp t) = (\t -> Timestamp t) <$> nextLargerKey t

	smallestKey = Timestamp SmallestInt

derive class Gast Timestamp
