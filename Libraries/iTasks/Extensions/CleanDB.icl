implementation module iTasks.Extensions.CleanDB

import iTasks

from Database.CleanDB import :: DBEnvironmentInfo, :: MapStats

derive gText DBEnvironmentInfo, MapStats
derive gEditor DBEnvironmentInfo, MapStats
