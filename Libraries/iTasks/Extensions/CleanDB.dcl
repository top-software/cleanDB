definition module iTasks.Extensions.CleanDB

from iTasks import
	generic gText, generic gEditor, :: EditorPurpose, :: EditorReport, :: Editor, generic gEq, :: TextFormat

from Database.CleanDB import :: DBEnvironmentInfo, :: MapStats

derive gText DBEnvironmentInfo, MapStats
derive gEditor DBEnvironmentInfo, MapStats
