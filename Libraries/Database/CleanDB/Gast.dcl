definition module Database.CleanDB.Gast

from StdEnv import
	class ==
from Data.Map import
	:: Map
from Data.Set import
	:: Set
from Database.CleanDB import
	:: CDB, :: SyncMode, class key, class primitiveKey, :: DBMap, :: DBMultiMap, :: Condition, :: KeyStr,
	class readTxn, :: ParallelOperationsSetting
from Gast import :: Property, generic genShow, generic ggen, :: GenState, class Gast, class TestArg
from StdClass import class Eq, class Ord, class <
from Text.GenPrint import generic gPrint, class PrintOutput, :: PrintState

/**
 * Runs a test on an empty test database. The database and possible associated sub-processes remain open after the test
 * and are reused for performance reasons. There is no way to free the resources as this function is only intended for
 * testing. DO NOT USE THIS IN A NESTED WAY! For multiple test databases use `withLabeledTestCleanDB`.
 */
withTestCleanDB :: !ParallelOperationsSetting !.(*CDB -> (.a, *CDB)) -> .a

//* Similar to `withTestCleanDB`, but allows nested, as long as different labels are provided for the test databases.
withLabeledTestCleanDB :: !String !ParallelOperationsSetting !.(*CDB -> (.a, *CDB)) -> .a

//* Tests whether given a list of keys, the correct result for the provided range condition is given.
getKeyRange :: ![k] !(!k, !k) -> Property | TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, key k

//* Tests whether given a list of key-tuples, the correct result for the provided range conditions is given.
getTupleKeyRange ::
	![(k1, k2)] !(!(!k1, !k1), !(!k2, !k2)) -> Property
	| TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k1 & TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k2

//* Tests whether given a list of key-3-tuples, the correct result for the provided range conditions is given.
getTuple3KeyRange ::
	![(k1, k2, k3)] !(!(!k1, !k1), !(!k2, !k2), !(!k3, !k3)) -> Property
	| TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k1 & TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k2
	& TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k3

//* Tests whether given a list of key-4-tuples, the correct result for the provided range conditions is given.
getTuple4KeyRange ::
	![(k1, k2, k3, k4)] !(!(!k1, !k1), !(!k2, !k2), !(!k3, !k3), !(!k4, !k4)) -> Property
	| TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k1 & TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k2
	& TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k3 & TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k4

/**
 * Tests whether the `all` conditions works properly for the provided list of keys. The flag indicates whether a
 * multi-map is used.
 */
getAllKeys :: !Bool ![k] -> Property | TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, key k

//* Tests whether the `all` conditions works properly for the provided list of tuple-keys.
getAllTupleKeys ::
	![(k1, k2)] -> Property
	| TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k1 & TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k2

/**
 * Tests whether retrieving the provided key given a list of keys works properly. The flag indicates whether a
 * partitioned map is used.
 */
getSingleKey :: !Bool ![k] !k -> Property | TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, key k

//* Tests whether retrieving the provided tuple-key given a list of keys works properly.
getSingleTupleKey ::
	![(k1, k2)] !(!k1, !k2) -> Property
	| TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k1 & TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k2

:: InequalityCondition k

derive class Gast InequalityCondition

ineqHolds :: !(InequalityCondition k) !k -> Bool | < k

ineqConditionFor :: !(InequalityCondition k) -> Condition k | primitiveKey k

//* Tests whether retrieving keys obeying the provided inequality condition works correctly given a list of keys.
getKeysObeyingIneq ::
	![k] !(InequalityCondition k) -> Property | TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k

/** Tests whether retrieving tuple-keys obeying the provided inequality conditions works correctly given a list of
 * tuple-keys.
 */
getTupleKeysObeyingIneq :: ![(k1, k2)] !(!InequalityCondition k1, !InequalityCondition k2) -> Property
                         | TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k1
                         & TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k2

//* Tests whether the key-set is correctly retrieved from the list of keys.
getKeySet :: ![k] ![k] -> Property | TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, key k

//* Tests whether the tuple-key-set is correctly retrieved from the lists of keys.
getTupleKeySet ::
	![(k1, k2)] !(![k1], ![k2]) -> Property
	| TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k1 & TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k2

:: Near k = LargestLessThanOrEq !k | SmallestGreaterThanOrEq !k

derive class Gast Near

nearConditionFor :: !(Near k) -> Condition k | key k

keyNearValue :: ![k] !(Near k) -> ?k | ==, Ord k

//* Tests whether retrieving keys obeying the provided near-constraint works correctly given a list of keys.
getKeyNearValue :: ![k] !(Near k) -> Property | TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, key k

//* Tests whether retrieving tuple-keys obeying the provided near-constraints works correctly given a list of keys.
getTupleKeyNearValue ::
	![(k1, k2)] !(!Near k1, !Near k2) -> Property
	| TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k1 & TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k2

//* Tests whether retrieving 3-tuple-keys obeying the provided near-constraints works correctly given a list of keys.
getTuple3KeyNearValues
	:: ![(k1, k2, k3)] !(!Near k1, !Near k2, !Near k3) -> Property
	| TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k1 & TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k2
	& TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k3

/**
 * General function testing that given a list of keys and a conditions, the result matches the provided predicate. The
 * flags indicates whether a multi map/a partitioned map is used.
 */
getKeys :: !Bool !Bool ![k] !(Condition k) !(k -> Bool) -> Property | TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, key k

getKeysUsingMap :: [(k, v)] (Condition k) (k -> Bool) !(DBMap k v) -> Property
                 | Eq, Ord, genShow{|*|}, gPrint{|*|}, key k & Eq, Ord, genShow{|*|}, gPrint{|*|} v

getKeysUsingMultiMap :: [(k, v)] (Condition k) (k -> Bool) !(DBMultiMap k v) -> Property
                      | Eq, Ord, genShow{|*|}, gPrint{|*|}, key k & Eq, Ord, genShow{|*|}, gPrint{|*|} v

//* Tests whether the correct key and associated values are retrieved from a multi-map.
getSingleKeyAndValuesOfMultiMap ::
	![(k, v)] !k -> Property
	| TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, key k & ==, Ord, genShow{|*|}, gPrint{|*|}, key v

/**
 * Tests whether the provided key and value can be found after putting it in a regular map and then retrieving it using
 * `getSingle`. Optionally, another value is put into the map for the same key before the value to test is put into the
 * map.
 */
getSingleKeyAndValue :: !k !(?v) !v -> Property | key, TC k & key, genShow{|*|}, gPrint{|*|}, == v

/**
 * General function testing that the keys and associated values are retrieved from a multi-map given a condition,
 * match the predicate provided.
 */
getKeysAndValuesOfMultiMap ::
	![(k, v)] !(Condition k) !(k -> Bool) -> Property
	| TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, key k & ==, Ord, genShow{|*|}, gPrint{|*|}, key v

/**
 * General function testing that the keys and the associated value are retrieving from the map given that
 * the condition for the k matches and the provided predicate returns `True` for the key.
 *
 * @param The list of keys and corresponding values to add.
 * @param The condition which should match for the key.
 * @param Predicate which returns whether the `k` should be returned.
 * @result Property.
 */
getKeysAndValues ::
	![(k, v)] !(Condition k) !(k -> Bool) -> Property
	| TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, key k & Eq, genShow{|*|}, gPrint{|*|}, key v

/**
 * Tests whether retrieving the key-value-pairs matching the provided conditions are correctly retrieved by
 * `getAccumNestedParallel`.
 * @param The key-value pairs added to the database.
 * @param The range of key to retrieve.
 * @param If provided, accumulation of the result stops after this key.
 * @result The property.
 */
getKeyRangeInParallel ::
	![!(k, v)] !(!k, !k) !(?k) -> Property
	| TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, key k & Eq, Ord, genShow{|*|}, gPrint{|*|}, key v

/**
 * Tests whether retrieving the key-value-pairs matching the provided conditions are correctly retrieved by
 * `getMultiAccumNestedParallel`
 * @param The key-value pairs added to the database.
 * @param The range of key to retrieve.
 * @param If provided, accumulation of the result stops at this key.
 * @result The property.
 */
getKeyRangeInParallelMulti ::
	![!(k, v)] !(!k, !k) !(?k) -> Property
	| TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, key k & Eq, Ord, genShow{|*|}, gPrint{|*|}, key v

/**
 * Tests whether key-value pairs added to two maps are correctly retrieved by
 * `getMultiAccumNestedParallelWithLocalStateAndContinuationChain`.
 */
getKeysAndValuesUsingContinuationChain ::
	!(Condition k) !(k -> Bool) ![!(k, v)] ![!(k, v)] -> Property
	| TC, Eq, genShow{|*|}, gPrint{|*|}, Ord, key k & Eq, genShow{|*|}, gPrint{|*|}, Ord, key v

/**
 * Special key string class to generate only strings of supported length.
 */
:: KeyString =: KeyString String

instance <            KeyString
instance ==           KeyString
instance key          KeyString
instance primitiveKey KeyString

derive gPrint  KeyString
derive genShow KeyString
derive ggen    KeyString

:: ValueString =: ValueString String

derive gPrint  ValueString
derive genShow ValueString
derive ggen    ValueString

toFromEqualsOriginal :: !a -> Property | Eq, key, genShow{|*|}, gPrint{|*|} a

keyOrderIsPreservedForPair :: !a !a -> Property | <, key a

keyLengthIsCorrect :: !a -> Property | primitiveKey a

keyHasFixedSizeImpliesAllStrRepresentationsFixedLength :: !a !a -> Property | key a

nextLargerKeyIsLarger :: !a -> Property | Ord, primitiveKey, genShow{|*|}, gPrint{|*|} a

noKeyInBetweenNextLargerKey :: !a -> Property | Ord, primitiveKey, TestArg a

:: Type a :== ()

noKeySmallerThanSmallest :: (Type k) -> Property | Ord, primitiveKey, TestArg k

//* Checks whether an ordinary map and a DB map have the same content.
checkEqualityOfMap ::
	!(Map k v) !(DBMap k v) !*txn -> (!Property, !*txn)
	| Eq, key, genShow{|*|}, gPrint{|*|} k & Eq, genShow{|*|}, gPrint{|*|} v & readTxn txn

//* Checks whether an ordinary map and a DB multi map have the same content.
checkEqualityOfMultiMap ::
	!(Map k (Set v)) !(DBMultiMap k v) !*txn -> (!Property, !*txn)
	| Eq, key, genShow{|*|}, gPrint{|*|} k & Eq, genShow{|*|}, gPrint{|*|} v & readTxn txn
