definition module Database.CleanDB._LMDB

/**
 * This provides an interface to the LMDB library.
 * The API documentation can be found at http://www.lmdb.tech/doc/.
 */

from Data.Error import :: MaybeError
from Data.Map import :: Map
from System.SysCall import class SysCallEnv
from System._Pointer import :: Pointer

EACCES :== 13

mdb_env_create :: !{#Int} !*w -> (!Int, !*w)

mdb_env_open :: !Pointer !String !Int !Int !*w -> (!Int, !*w)
MDB_NOSYNC     :== 0x10000
MDB_NOMETASYNC :== 0x40000
MDB_WRITEMAP   :== 0x80000
MDB_NORDAHEAD  :== 0x800000
MDB_NOMEMINIT  :== 0x1000000
MDB_NOTLS      :== 0x200000

MDB_RESERVE   :== 0x10000
MDB_APPEND    :== 0x20000
MDB_APPENDDUP :== 0x40000
MDB_NODUPDATA :== 0x20

mdb_env_close :: !Pointer !*w -> *w
mdb_env_set_maxdbs :: !Pointer !Int !*w -> (!Int, !*w)
mdb_env_set_mapsize :: !Pointer !Int !*w -> (!Int, !*w)
mdb_env_set_maxreaders :: !Pointer !Int !*w -> (!Int, !*w)
mdb_env_get_path :: !Pointer !{#Pointer} !*w -> (!Int, !*w)
mdb_env_get_maxkeysize :: !Pointer !*w -> (!Int, !*w)
mdb_reader_check :: !Pointer !Pointer !*w -> (!Int, !*w)

mdb_txn_begin :: !Pointer !Pointer !Int !{#Int} !*w -> (!Int, !*w)
mdb_txn_commit :: !Pointer !*w -> (!Int, !*w)
mdb_txn_env :: !Pointer !*w -> (!Pointer, !*w)
MDB_RDONLY :== 0x20000

mdb_dbi_open :: !Pointer !String !Int !{#Int} !*w -> (!Int, !*w)
MDB_CREATE     :== 0x40000
MDB_DUPSORT    :== 0x04
MDB_DUPFIXED   :== 0x10
MDB_INTEGERDUP :== 0x20

MDB_NOTFOUND :== -30798
MDB_KEYEXIST :== -30799
MDB_READERS_FULL :== -30790

mdb_cursor_open :: !Pointer !Int !{#Int} !*w -> (!Int, !*w)
mdb_cursor_close :: !Pointer !*w -> *w
mdb_cursor_put :: !Pointer !{#Int} !{#Int} !Int !*w -> (!Int, !*w)
mdb_cursor_del :: !Pointer !Int !*w -> (!Int, !*w)

MDB_FIRST       :==  0
MDB_FIRST_DUP   :==  1
MDB_GET_BOTH    :==  2
MDB_GET_CURRENT :== 4
MDB_LAST        :==  6
MDB_NEXT        :==  8
MDB_NEXT_DUP    :==  9
MDB_NEXT_NODUP  :== 11
MDB_PREV        :== 12
MDB_PREV_DUP    :== 13
MDB_PREV_NODUP  :== 14
MDB_SET         :== 15
MDB_SET_RANGE   :== 17

mdb_drop :: !Pointer !Int !Int !*w -> (!Int, !*w)
mdb_strerror :: !Int -> Pointer

//* `mdbCursorPut cur key val flags env`: Interface for `mdb_cursor_put` providing Clean `String`s as `MDB_val` values.
mdbCursorPut :: !Pointer !String !String !Int !*env -> (!Int, !*env)

/**
 * `mdbPutOffsetSize txn dbi key val offset size flags env`:
 *     Interface for `mdb_put` providing Clean `String`s as `MDB_val` values. The `val` string is provided only
 *     partially as defined by `offset` and `size`.
 */
mdbPutOffsetSize :: !Pointer !Int !String !String !Int !Int !Int !*env -> (!Int, !*env)

/**
 * `mdbGet txn dbi key env`:
 *     Interface for `mdb_get` providing the key Clean `String` as `MDB_val` value. The read value is returned as Clean
 *     `String`.
 */
mdbGet :: !Pointer !Int !String !*env -> (!Int, !String, !*env)

/**
 * `mdbGetWithoutValueRead txn dbi key env`:
 *     Interface for `mdb_get` providing the key Clean `String` as `MDB_val` value.
 * Unlike `mdbGet`, this function does not read the value for the key, this can save time
 * if reading the value is not needed.
 */
mdbGetWithoutValueRead :: !Pointer !Int !String !*env -> (!Int, !*env)

/**
 * `mdbCursorGetReadVal cursor key op env`:
 *     Interface for `mdb_cursor_get` providing the key Clean `String` as `MDB_val` value. The read value is returned
 *     as Clean `String`.
 */
mdbCursorGetReadVal :: !Pointer !String !Int !*env -> (!Int, !String, !*env)

/**
 * `mdbCursorGetReadKey cursor key valVal op env`:
 *     Interface for `mdb_cursor_get` providing the key Clean `String` as `MDB_val` value. The read key is returned
 *     as Clean `String`. `valVal` should be an array of length 2 representing a `MDB_val` value. The value is not
 *     returned as Clean `String` to make it possible to read it only when required.
 */
mdbCursorGetReadKey :: !Pointer !String !{#Int} !Int !*env -> (!Int, !String, !*env)

/**
 * `mdbCursorGetNo cursor key valVal op env`:
 *     Interface for `mdb_cursor_get` providing the key Clean `String` as `MDB_val` value.
 *     `valVal` should be an array of length 2 representing a `MDB_val` value.
 *     The value is not read by Clean. Therefore if it is not necessary to read the value using this function is
 *     more efficient than using `mdbCursorGetReadKey`.
 */
mdbCursorGetNoRead :: !Pointer !String !String !Int !*env -> (!Int, !*env)

//* `moveFromHighToLow n curLow curHigh isMultiMap env`: Moves `n` items from `curHigh` to `curLow`.
moveFromHighToLow :: !Int !Pointer !Pointer !Bool !*env -> (!Int, !*env)

//* `mdbDelNoData txn dbi key env`: Interface for `mdb_del` providing the key Clean `String` as `MDB_val` value.
mdbDelNoData :: !Pointer !Int !String !*env -> (!Int, !*env)

/**
 * `mdbDel txn dbi key data env`:
 *      Interface for `mdb_del` providing the key and value Clean `String` as `MDB_val` values.
 */
mdbDel :: !Pointer !Int !String !String !*env -> (!Int, !*env)

/**
 * Deletes all sub-databases. If the first argument is `True`, they are dropped completely and handles are closed.
 * This function is only safe if the database has not been modified in the same transaction, as all dbis are
 * dropped and closed. See the documentation of `mdb_dbi_close` for the caveats of doing this.
 * @param Indicates whether to drop the dbi.
 * @param Transaction pointer.
 * @param Arbitrary environment.
 * @param Return code indicating error if they occur.
 */
deleteAllSubDbs :: !Bool !Pointer !*env -> (!Int, !*env)

/**
 * Deletes all sub-databases related to maps without any data. They are dropped completely and handles are closed.
 * This function is only safe if the database has not been modified in the same transaction, as all dbis are
 * dropped and closed. See the documentation of `mdb_dbi_close` for the caveats of doing this.
 * @param Transaction pointer.
 * @param Arbitrary environment.
 * @param Return code indicating error if they occur.
 */
deleteEmptyMapsC :: !Pointer !*env -> (!Int, !*env)

/**
 * Retrieves the environment information for the provided MDB_ENV pointer.
 *
 * @param The MDB_ENV pointer to retrieve the environment information for.
 * @param Arbitrary syscall environment.
 * @result Size of the underlying memory map.
 * @result Size of a database page (does not have to be equal to the size of an OS-page).
 * @result The number of available pages within the memory map (mapsize / pagesize).
 * @result The number of pages that are currently in use.
 * @result The id of the last transaction that was performed (0 if none were performed yet).
 * @result The maximum number of allowed readers on the memory map.
 * @result The number of readers that are currently active.
 */
getMdbEnvInfo :: !Pointer !*env -> (!(!Int, !Int, !Int, !Int, !Int, !Int, !Int), !*env) | SysCallEnv env

/**
 * Retrieves the environment information for the provided path.
 *
 * @param The path to the lmdb database (C-string).
 * @param Arbitrary syscall environment.
 * @result Size of the underlying memory map.
 * @result Size of a database page (does not have to be equal to the size of an OS-page).
 * @result The number of available pages within the memory map (mapsize / pagesize).
 * @result The number of pages that are currently in use.
 * @result The id of the last transaction that was performed (0 if none were performed yet).
 * @result The maximum number of allowed readers on the memory map.
 * @result The number of readers that are currently active.
 */
getMdbEnvInfoForPath :: !String !*env -> (!(!Int, !Int, !Int, !Int, !Int, !Int, !Int), !*env) | SysCallEnv env

/**
 * Gets the dbi stats of all maps (see documentation of `MDB_stat`).
 * @param The txn pointer.
 * @param Arbitrary env.
 * @result Per map name, the stats included in `MDB_stat`.
 */
getDbiStats :: !Pointer !*env -> (!MaybeError Int (Map String (!Int, !Int, !Int, !Int, !Int, !Int)), !*env)
