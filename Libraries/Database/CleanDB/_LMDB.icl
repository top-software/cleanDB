implementation module Database.CleanDB._LMDB

import code from library "liblmdb.a"
import code from library "-lpthread"
import code from "lmdbInterface.o"
import Data.Error
import Data.Func
from Data.Map import :: Map
import qualified Data.Map as Map
import Data.Maybe, Data._Array, System._Memory, System._Pointer, System._Posix
import StdEnv

mdb_env_create :: !{#Int} !*w -> (!Int, !*w)
mdb_env_create env world = code {
    ccall mdb_env_create "A:I:A"
}

mdb_env_open :: !Pointer !String !Int !Int !*w -> (!Int, !*w)
mdb_env_open env path flag mode world = code {
    ccall mdb_env_open "psII:I:A"
}

mdb_env_close :: !Pointer !*w -> *w
mdb_env_close env world = code {
    ccall mdb_env_close "p:V:A"
}

mdb_env_set_maxdbs :: !Pointer !Int !*w -> (!Int, !*w)
mdb_env_set_maxdbs env dbs world = code {
    ccall mdb_env_set_maxdbs "pI:I:A"
}

mdb_env_set_mapsize :: !Pointer !Int !*w -> (!Int, !*w)
mdb_env_set_mapsize env size world = code {
    ccall mdb_env_set_mapsize "pI:I:A"
}

mdb_env_get_path :: !Pointer !{#Pointer} !*w -> (!Int, !*w)
mdb_env_get_path env path world = code {
	ccall mdb_env_get_path "pA:I:A"
}

mdb_env_get_maxkeysize :: !Pointer !*w -> (!Int, !*w)
mdb_env_get_maxkeysize env world = code {
	ccall mdb_env_get_maxkeysize "p:I:A"
}

mdb_env_set_maxreaders :: !Pointer !Int !*w -> (!Int, !*w)
mdb_env_set_maxreaders env nr world = code {
	ccall mdb_env_set_maxreaders "pI:I:A"
}

mdb_reader_check :: !Pointer !Pointer !*w -> (!Int, !*w)
mdb_reader_check env dead world = code {
	ccall mdb_reader_check "pp:I:A"
}

mdb_txn_begin :: !Pointer !Pointer !Int !{#Int} !*w -> (!Int, !*w)
mdb_txn_begin env parent flags txn world = code {
    ccall mdb_txn_begin "ppIA:I:A"
}

mdb_txn_commit :: !Pointer !*w -> (!Int, !*w)
mdb_txn_commit txn world = code {
    ccall mdb_txn_commit "p:I:A"
}

mdb_txn_env :: !Pointer !*w -> (!Pointer, !*w)
mdb_txn_env txn world = code {
	ccall mdb_txn_env "p:p:A"
}

mdb_dbi_open :: !Pointer !String !Int !{#Int} !*w -> (!Int, !*w)
mdb_dbi_open txn name flags dbi world = code {
    ccall mdb_dbi_open "psIA:I:A"
}

mdb_cursor_open :: !Pointer !Int !{#Int} !*w -> (!Int, !*w)
mdb_cursor_open txn dbi cursor world = code {
    ccall mdb_cursor_open "pIA:I:A"
}

mdb_cursor_close :: !Pointer !*w -> *w
mdb_cursor_close cursor world = code {
    ccall mdb_cursor_close "p:V:A"
}

mdb_cursor_put :: !Pointer !{#Int} !{#Int} !Int !*w -> (!Int, !*w)
mdb_cursor_put cursor key data op world = code {
    ccall mdb_cursor_put "pAAI:I:A"
}

mdb_cursor_del :: !Pointer !Int !*w -> (!Int, !*w)
mdb_cursor_del cursor flags  world = code {
	ccall mdb_cursor_del "pI:I:A"
}

mdb_drop :: !Pointer !Int !Int !*w -> (!Int, !*w)
mdb_drop txn dbi del world = code {
    ccall mdb_drop "pII:I:A"
}

mdb_strerror :: !Int -> Pointer
mdb_strerror err = code {
    ccall mdb_strerror "I:p"
}

mdbCursorPut :: !Pointer !String !String !Int !*env -> (!Int, !*env)
mdbCursorPut cur key val flags env = code {
	ccall mdbCursorPut "pssI:I:A"
}

mdbPutOffsetSize :: !Pointer !Int !String !String !Int !Int !Int !*env -> (!Int, !*env)
mdbPutOffsetSize txn dbi key val offset size flags env = code {
	ccall mdbPutOffsetSize "pIssIII:I:A"
}

mdbGet :: !Pointer !Int !String !*env -> (!Int, !String, !*env)
mdbGet txn dbi key env
	# (ret, env) = mdbGet` txn dbi key valVal env
	| ret <> 0 = (ret, "", env)
	# (str, env) = readStringFromVal valVal env
	= (ret, str, env)

mdbGetWithoutValueRead :: !Pointer !Int !String !*env -> (!Int, !*env)
mdbGetWithoutValueRead txn dbi key env = mdbGet` txn dbi key valVal env

mdbGet` :: !Pointer !Int !String !{#Int} !*env -> (!Int, !*env)
mdbGet` txn dbi key val env = code {
	ccall mdbGet "pIsA:I:A"
}

mdbCursorGetReadVal :: !Pointer !String !Int !*env -> (!Int, !String, !*env)
mdbCursorGetReadVal cursor key op env
	# (ret, env) = mdbCursorGetReadVal` cursor key valVal op env
	| ret <> 0 = (ret, "", env)
	# (str, env) = readStringFromVal valVal env
	= (ret, str, env)
where
	mdbCursorGetReadVal` :: !Pointer !String !{#Int} !Int !*env -> (!Int, !*env)
	mdbCursorGetReadVal` cur key val op env = code {
		ccall mdbCursorGetReadVal "psAI:I:A"
	}

mdbCursorGetNoRead :: !Pointer !String !String !Int !*env -> (!Int, !*env)
mdbCursorGetNoRead cursor key valVal op env
	# (ret, env) = mdbCursorGetNoRead` cursor key valVal op env
	= (ret, env)
where
	mdbCursorGetNoRead` :: !Pointer !String !String !Int !*env -> (!Int, !*env)
	mdbCursorGetNoRead` cur keyIn valIn op env = code {
		ccall mdbCursorGetNoRead "pssI:I:A"
	}

mdbCursorGetReadKey :: !Pointer !String !{#Int} !Int !*env -> (!Int, !String, !*env)
mdbCursorGetReadKey cursor key valVal op env
	# (ret, env) = mdbCursorGetReadKey` cursor key keyVal valVal op env
	| ret <> 0 = (ret, "", env)
	# (str, env) = readStringFromVal keyVal env
	= (ret, str, env)
where
	mdbCursorGetReadKey` :: !Pointer !String !{#Int} !{#Int} !Int !*env -> (!Int, !*env)
	mdbCursorGetReadKey` cur keyIn keyOut val op env = code {
		ccall mdbCursorGetReadKey "psAAI:I:A"
	}

moveFromHighToLow :: !Int !Pointer !Pointer !Bool !*env -> (!Int, !*env)
moveFromHighToLow n curLow curHigh isMultiMap env = code {
	ccall moveFromHighToLow "IppI:I:A"
}

mdbDelNoData :: !Pointer !Int !String !*env -> (!Int, !*env)
mdbDelNoData txn dbi key env = code {
	ccall mdbDelNoData "pIs:I:A"
}

mdbDel :: !Pointer !Int !String !String !*env -> (!Int, !*env)
mdbDel txn dbi key data env = code {
	ccall mdbDel "pIss:I:A"
}

deleteAllSubDbs :: !Bool !Pointer !*env -> (!Int, !*env)
deleteAllSubDbs dropDbis txn env = code {
	ccall deleteAllSubDbs "Ip:I:A"
}

deleteEmptyMapsC :: !Pointer !*env -> (!Int, !*env)
deleteEmptyMapsC txn env = code {
	ccall deleteEmptyMaps "p:I:A"
}

// Use global arrays to avoid memory allocations.
keyVal :: {#Int}
keyVal =: unsafeCreateArray 2

valVal :: {#Int}
valVal =: unsafeCreateArray 2

readStringFromVal :: !{#Int} !*env -> (!String, !*env)
readStringFromVal val env = (derefCharArray val.[1] val.[0], env)

getMdbEnvInfo :: !Pointer !*env -> (!(!Int, !Int, !Int, !Int, !Int, !Int, !Int), !*env) | SysCallEnv env
getMdbEnvInfo mdbEnvPtr env = getMdbEnvInfo` mdbEnvPtr env
where
	// Using p as a return type here allows clean to convert 32 bit unsigned ints to 64 bit signed ints.
	// using `I` instead causes overflow.
	getMdbEnvInfo` :: !Pointer !*env -> (!(!Int, !Int, !Int, !Int, !Int, !Int, !Int), !*env)
	getMdbEnvInfo` mdbEnvPtr env = code {
		ccall getMdbEnvInfo "p:Vppppppp:A"
	}

getMdbEnvInfoForPath :: !String !*env -> (!(!Int, !Int, !Int, !Int, !Int, !Int, !Int), !*env) | SysCallEnv env
getMdbEnvInfoForPath path env = getMdbEnvInfoForPath` path env
where
	// Using p as a return type here allows clean to convert 32 bit unsigned ints to 64 bit signed ints.
	// using `I` instead causes overflow.
	getMdbEnvInfoForPath` :: !String !*env -> (!(!Int, !Int, !Int, !Int, !Int, !Int, !Int), !*env)
	getMdbEnvInfoForPath` mdbEnvPtr env = code {
		ccall getMdbEnvInfoForPath "s:Vppppppp:A"
	}

getDbiStats :: !Pointer !*env -> (!MaybeError Int (Map String (!Int, !Int, !Int, !Int, !Int, !Int)), !*env)
getDbiStats txn env
	# (ret, env) = initStatRetrieval txn env
	| ret <> 0 = (Error ret, env)
	= getNext 'Map'.newMap env
where
	getNext ::
		!(Map String (!Int, !Int, !Int, !Int, !Int, !Int)) !*env
		-> (!MaybeError Int (Map String (!Int, !Int, !Int, !Int, !Int, !Int)), !*env)
	getNext acc env
		# ((res, name, ms_psize, ms_depth, ms_branch_pages, ms_leaf_pages, ms_overflow_pages, ms_entries), env) =
			getNextStat txn env
		| res == MDB_NOTFOUND = (Ok acc, env)
		| res <> 0 = (Error res, env)
		= getNext
			('Map'.put name (ms_psize, ms_depth, ms_branch_pages, ms_leaf_pages, ms_overflow_pages, ms_entries) acc) env

initStatRetrieval :: !Pointer !*env -> (!Int, !*env)
initStatRetrieval txn env = code {
	ccall initStatRetrieval "p:I:A"
}

getNextStat :: !Pointer !*env -> (!(!Int, !String, !Int, !Int, !Int, !Int, !Int, !Int), !*env)
getNextStat txn env = code {
	ccall getNextStat "p:VISIIpppp:A"
}
