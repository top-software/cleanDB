definition module Database.CleanDB.Geo

/**
 * This module provides an extension for using geo positions as provided by `Data.Geo` with `Database.CleanDB`.
 * Specifically, positions can be used as keys and areas as conditions for position keys.
 *
 * @property-bootstrap
 *     import Data.Geo, Data.Geo.Gast
 *     import Gast.Gen
 *     import StdEnv, _SystemStrictLists
 *     import Text.GenPrint
 *     import Database.CleanDB.Geo, Database.CleanDB.Gast
 */
from Data.Set         import :: Set
from Data.Geo         import :: Position, :: Region, :: CellId
from Database.CleanDB import class primitiveKey, class key, :: Condition

/**
 * @property is isomorphism: A.x :: Position:
 *     toFromEqualsOriginal x
 * @property preserves order: A.x :: Position; y :: Position:
 *     keyOrderIsPreservedForPair x y
 * @property hasFixedSize implies fixed size strRepresentation of values: A.v1 :: Position; v2 :: Position:
 *     keyHasFixedSizeImpliesAllStrRepresentationsFixedLength v1 v2
 */
instance key Position

/**
 * @property key length is correct: A.x :: Position:
 *    keyLengthIsCorrect x
 * @property next larger key is larger: A.k :: Position:
 *     nextLargerKeyIsLarger k
 * @property there is no key between this one and the next larger: A.k :: Position:
 *     noKeyInBetweenNextLargerKey k
 * @property there is no key smaller than the smallest one: A.type :: Type Position:
 *     noKeySmallerThanSmallest type
 */
instance primitiveKey Position

/**
 * These conditions are not efficient and have to go through all positions stored.
 * For efficient retrieval use a `CellId` key before the `Position` key.
 */
positionIsIn :: !Region -> Condition Position

//* A condition which checks that the position is in one of the provided regions.
positionIsInOneOf :: !(Set Region) -> Condition Position

//* A condition which checks that the position is not(!) in one of the provided regions.
positionIsNotInOneOf :: !(Set Region) -> Condition Position

/**
 * @property is isomorphism: A.x :: CellId:
 *     toFromEqualsOriginal x
 * @property preserves order: A.x :: CellId; y :: CellId:
 *     keyOrderIsPreservedForPair x y
 * @property hasFixedSize implies fixed size strRepresentation of values: A.v1 :: CellId; v2 :: CellId:
 *     keyHasFixedSizeImpliesAllStrRepresentationsFixedLength v1 v2
 */
instance key CellId

/**
 * @property key length is correct: A.x :: CellId:
 *    keyLengthIsCorrect x
 * @property next larger key is larger: A.k :: CellId:
 *     nextLargerKeyIsLarger k
 * @property there is no key between this one and the next larger: A.k :: CellId:
 *     noKeyInBetweenNextLargerKey k
 * @property there is no key smaller than the smallest one: A.type :: Type CellId:
 *     noKeySmallerThanSmallest type
 */
instance primitiveKey CellId

cellIsIn :: !Region -> Condition CellId

cellIsInOneOf :: !(Set Region) -> Condition CellId
