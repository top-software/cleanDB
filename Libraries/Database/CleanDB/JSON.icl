implementation module Database.CleanDB.JSON

import StdEnv => qualified all
import Data.Func
import Data.Map.GenJSON
import Data.Maybe
import Text, Text.GenJSON
import Database.CleanDB
import qualified Database.CleanDB as CDB

dbMap :: !String -> DBMap k v | JSONEncode{|*|}, JSONDecode{|*|} v
dbMap name = 'CDB'.dbMap name encode decode

dbMultiMap :: !String -> DBMultiMap k v  | JSONEncode{|*|}, JSONDecode{|*|} v
dbMultiMap name = 'CDB'.dbMultiMap name encode decode

encode :: !a -> String | JSONEncode{|*|} a
encode x = toString $ toJSON x

decode :: !String -> a | JSONDecode{|*|} a
decode str = fromMaybe (abort $ concat ["CleanDB: Invalid JSON encoding:", str, "\n"]) $ fromJSON $ fromString str

derive JSONEncode TxnDurationTable, DBEnvironmentInfo, MapStats
derive JSONDecode TxnDurationTable, DBEnvironmentInfo, MapStats
