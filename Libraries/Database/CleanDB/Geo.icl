implementation module Database.CleanDB.Geo

import Data.Error, Data.Functor, Data.Geo, Data.Geo._S2
from Data.Foldable import class Foldable
from Data.Functor import instance Functor []
import Data.Maybe
from Data.Set  import :: Set, instance Foldable Set
import qualified Data.Set as Set
import Data.Tuple
import StdEnv => qualified all

import Database.CleanDB

instance primitiveKey Position where
	keyLength = FixedLength 8
	nextLargerKey pos
		| lngInt == largestLngInt
			| latInt == largestLatInt = ?None
			| otherwise = ?Just $ positionCorrespondingToInts (inc latInt) 0
		| otherwise = ?Just $ positionCorrespondingToInts latInt (inc lngInt)
	where
		(latInt, lngInt) = latLngIntsOf pos
	smallestKey = smallestPosition

smallestPosition :: Position
smallestPosition =: fromOkWithError $ position -90.0 -180.0

smallestPositionKeyStr :: KeyStr Position
smallestPositionKeyStr =: strRepresentation smallestPosition

largestLatInt :: Int
largestLatInt =: fst largestPositionInts

largestLngInt :: Int
largestLngInt =: snd largestPositionInts

largestPositionInts :: (Int, Int)
largestPositionInts = latLngIntsOf $ fromOkWithError $ position 90.0 180.0

instance key Position where
	strRepresentation pos =
		KeyStr
			{ fromInt $ latInt >> 24, fromInt $ latInt >> 16, fromInt $ latInt >> 8, fromInt latInt
			, fromInt $ lngInt >> 24, fromInt $ lngInt >> 16, fromInt $ lngInt >> 8, fromInt lngInt
			}
	where
		(latInt, lngInt) = latLngIntsOf pos

	fromStrRepresentation (KeyStr str) = case pos of
		Ok pos = pos
		// Erroneous data can be present in database due to a past bug, instead of crashing the best thing we can do is
		// to provide some value.
		_ = fromOkWithError $ position 90.0 180.0
	where
		pos = position (lat / POSITION_PRECISION - 90.0) (lng / POSITION_PRECISION - 180.0)
		lat = fromInt (toInt str.[0] << 24 + toInt str.[1] << 16 + toInt str.[2] << 8 + toInt str.[3])
		lng = fromInt (toInt str.[4] << 24 + toInt str.[5] << 16 + toInt str.[6] << 8 + toInt str.[7])

	hasFixedSize _ = True

latLngIntsOf :: !Position -> (!Int, !Int)
latLngIntsOf pos =
	(toInt $ (latitudeOf pos +  90.0) * POSITION_PRECISION, toInt $ (longitudeOf pos + 180.0) * POSITION_PRECISION)

positionCorrespondingToInts :: !Int !Int -> Position
positionCorrespondingToInts lat lng = fromOkWithError $
	position (((toReal lat) / POSITION_PRECISION) - 90.0) (((toReal lng) / POSITION_PRECISION) - 180.0)

keyLengthOf :: !String !Int -> Int
keyLengthOf str i = (if (l < 0) (~l - 1) l) + 1
where
	l = fromChar str.[i] - 127

positionIsIn :: !Region -> Condition Position
positionIsIn region = positionIsInOneOf $ 'Set'.singleton region

positionIsInOneOf :: !(Set Region) -> Condition Position
positionIsInOneOf regions = positionIsInOneOfCondition False regions

positionIsNotInOneOf :: !(Set Region) -> Condition Position
positionIsNotInOneOf regions = positionIsInOneOfCondition True regions

positionIsInOneOfCondition :: !Bool !(Set Region) -> Condition Position
positionIsInOneOfCondition isNot regions = conditionWithDefaultOrder
	[!statelessPred
		( \pred = GivenKey
			{targetKey = smallestPositionKeyStr, keyMayBeSkipped = True, usePreviousKey = ?None, targetKeyPred = pred})
		pred!]
where
	pred :: !(KeyStr Position) -> (!Bool, !(Pred Position) -> PredContinuation Position)
	pred keyStr = (if isNot not id $ or ((isInsideOf) position <$> regionList), NextKey)
	where
		position = fromStrRepresentation keyStr
		regionList = 'Set'.toList regions

instance primitiveKey CellId where
	keyLength = FixedLength 8
	nextLargerKey (CellId str)         = (\str -> CellId str) <$> incrementedStrWithFixedLength str
	smallestKey                        = CellId "\0\0\0\0\0\0\0\0"

instance key CellId where
	strRepresentation (CellId str)     = KeyStr str
	fromStrRepresentation (KeyStr str) = CellId str
	hasFixedSize _ = True

cellIsIn :: !Region -> Condition CellId
cellIsIn region = cellIsInOneOf $ 'Set'.singleton region

cellIsInOneOf :: !(Set Region) -> Condition CellId
cellIsInOneOf regions =
	case ranges of
		[]              = none
		[(first, _): _] =
			conditionWithDefaultOrder
				[! statefulPred
					(\pred ->
						GivenKey
							{ targetKey       = KeyStr first
							, keyMayBeSkipped = True
							, usePreviousKey  = ?None
							, targetKeyPred   = pred
							}
					)
					pred`
					ranges
				!]
where
	ranges = sort $ flatten $ (\region -> coveringCellIdRanges region.s2Region) <$> 'Set'.toList regions

	pred` ::
		!(KeyStr CellId) ![(String, String)]
		-> (!Bool, !(Pred CellId) -> PredContinuation CellId, ![(String, String)])
	pred` keyStr ranges = (includeRes, cont, ranges`)
	where
		(KeyStr str)                = keyStr
		(includeRes, cont, ranges`) = pred`` ranges

		pred`` :: ![(String, String)]
			   -> (!Bool , !(Pred CellId) -> PredContinuation CellId, ![(String, String)])
		pred`` ranges = case ranges of
			[] = (False, const Stop, [])
			[(lower, upper): rest]
				| str < upper = (True, NextKey, ranges)
				| otherwise   = (str == upper || isIncluded, cont, ranges)
				with
					(isIncluded, cont, ranges) = nextRange rest
			where
				nextRange :: ![(String, String)] -> (!Bool, !(Pred k) -> PredContinuation k, ![(String, String)])
				nextRange [] = (False, const Stop, [])
				nextRange ranges=:[(nextLower, nextUpper): rest]
					// we are already past the end of the range -> skip it
					| str >= nextUpper =
						appFst3 (\isIncluded -> isIncluded || str == nextUpper) $ nextRange rest
					// the current key is smaller than the start of the next range -> goto start of range
					| nextLower >  str =
						( False
						, \pred ->
							GivenKey
								{ targetKey       = KeyStr nextLower
								, keyMayBeSkipped = True
								, usePreviousKey  = ?None
								, targetKeyPred   = pred
								}
						, ranges
						)
					// the current key is inside this range -> goto next key
					| otherwise =
						(True, NextKey, ranges)
