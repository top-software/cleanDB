implementation module Database.CleanDB.KeyEncoding

import StdEnv => qualified all
import Data.Func
import Database.CleanDB
import qualified Database.CleanDB as CDB

dbMap :: !String -> DBMap k v | key v
dbMap name = 'CDB'.dbMap name encode decode

dbMultiMap :: !String -> DBMultiMap k v  | key v
dbMultiMap name
	# multiMap = 'CDB'.dbMultiMap name encode decode
	/*
	 * Because of the type of hasFixedSize below, an undef value can be provided to
	 * 'CDB'.hasFixedSize while making the compiler know that undef is a value of type v.
	 * since the (DBMultiMap k v) is passed as an argument to hasFixedSize.
	 * This makes sure the proper instance of 'CDB'.hasFixedSize can be evaluated.
	 * Functions within Clean type classes,
	 * such as class key v have to use the v type argument within every function.
	 * That is why 'CDB'.hasFixedSize is :: v -> Bool instead of :: Bool.
	 * Because of this, it is necessary to pass on an undefined v since we do not have a v in dbMultiMap.
	 * dbMultiMap does not use a v argument since it should not be needed to provide a value when using dbMultiMap.
	 */
	# hasFixedSizeValues = hasFixedSize multiMap undef
	= if (hasFixedSizeValues) ('CDB'.dbMultiMapFixedValueSize name encode decode) multiMap
where
	hasFixedSize :: (DBMultiMap k v) v -> Bool | key v
	hasFixedSize _ v = 'CDB'.hasFixedSize v

encode :: !a -> String | key a
encode x = str
where
	(KeyStr str) = 'CDB'.strRepresentation x

decode :: !String -> a | key a
decode str = 'CDB'.fromStrRepresentation $ KeyStr str
