definition module Database.CleanDB.KeyEncoding

from Database.CleanDB import :: DBMap, :: DBMultiMap, class key

dbMap :: !String -> DBMap k v | key v
dbMultiMap :: !String -> DBMultiMap k v  | key v
