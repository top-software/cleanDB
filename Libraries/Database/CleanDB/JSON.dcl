definition module Database.CleanDB.JSON

from Text.GenJSON     import generic JSONEncode, generic JSONDecode, :: JSONNode
from Database.CleanDB import :: DBMap, :: DBMultiMap, :: TxnDurationTable, :: DBEnvironmentInfo, :: MapStats

dbMap :: !String -> DBMap k v | JSONEncode{|*|}, JSONDecode{|*|} v
dbMultiMap :: !String -> DBMultiMap k v  | JSONEncode{|*|}, JSONDecode{|*|} v

derive JSONEncode TxnDurationTable, DBEnvironmentInfo, MapStats
derive JSONDecode TxnDurationTable, DBEnvironmentInfo, MapStats
