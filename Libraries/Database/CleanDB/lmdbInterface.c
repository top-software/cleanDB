#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "lmdb.h"
#include "../../../nitrile-packages/linux-x64/base-rts/misc/src/Clean.h"

const int MAX_MAPNAME_SIZE = 512; // We assume a max size to avoid malloc/free calls.

int mdbPutOffsetSize(MDB_txn* txn, MDB_dbi dbi, void* key, void* data, size_t offset, size_t size, unsigned int flags) {
	MDB_val keyVal = {.mv_size = *((size_t*)key - 1), .mv_data = key};
	MDB_val dataVal = {.mv_size = size, .mv_data = data + offset};
	return mdb_put(txn, dbi, &keyVal, &dataVal, flags);
}

int mdbCursorPut(MDB_cursor* cur, void* key, void* data, unsigned int flags) {
	MDB_val keyVal = {.mv_size = *((size_t*)key - 1), .mv_data = key};
	MDB_val dataVal = {.mv_size = *((size_t*)data - 1), .mv_data = data};
	return mdb_cursor_put(cur, &keyVal, &dataVal, flags);
}

int mdbGet(MDB_txn* txn, MDB_dbi dbi, void* key, MDB_val* dataVal) {
	MDB_val keyVal = {.mv_size = *((size_t*)key - 1), .mv_data = key};
	return mdb_get(txn, dbi, &keyVal, dataVal);
}

int mdbCursorGetReadVal(MDB_cursor* cursor, void* key, MDB_val* dataVal, MDB_cursor_op op) {
	MDB_val keyVal = {.mv_size = *((size_t*)key - 1), .mv_data = key};
	return mdb_cursor_get(cursor, &keyVal, dataVal, op);
}

int mdbCursorGetNoRead(MDB_cursor* cursor, void* key, void* val, MDB_cursor_op op) {
	MDB_val keyVal = {.mv_size = *((size_t*)key - 1), .mv_data = key};
	MDB_val valVal = {.mv_size = *((size_t*)val - 1), .mv_data = val};
	return mdb_cursor_get(cursor, &keyVal, &valVal, op);
}

int mdbCursorGetReadKey(MDB_cursor* cursor, void* keyIn, MDB_val* keyOut, MDB_val* dataVal, MDB_cursor_op op) {
	MDB_val keyVal = {.mv_size = *((size_t*)keyIn - 1), .mv_data = keyIn};
	int ret = mdb_cursor_get(cursor, &keyVal, dataVal, op);
	*keyOut = keyVal;
	return ret;
}

int mdbDel(MDB_txn* txn, MDB_dbi dbi, void* key, void* data) {
	MDB_val keyVal = {.mv_size = *((size_t*)key - 1), .mv_data = key};
	MDB_val dataVal = {.mv_size = *((size_t*)data - 1), .mv_data = data};
	return mdb_del(txn, dbi, &keyVal, &dataVal);
}

int mdbDelNoData(MDB_txn* txn, MDB_dbi dbi, void* key) {
	MDB_val keyVal = {.mv_size = *((size_t*)key - 1), .mv_data = key};
	return mdb_del(txn, dbi, &keyVal, 0);
}

int moveFromHighToLow(const unsigned int n, MDB_cursor* curLow, MDB_cursor* curHigh, bool isMultiMap) {
	int rc;
	MDB_val key;
	MDB_val data;

	for(unsigned int i = 0; i < n; i++) {
		rc = mdb_cursor_get(curHigh, &key, &data, MDB_FIRST);
		if (rc == MDB_NOTFOUND) break;
		if (rc) return rc;

		rc = mdb_cursor_put(curLow, &key, &data, MDB_APPEND);
		if (rc) return rc;

		if (isMultiMap) {
			while (true) {
				rc = mdb_cursor_get(curHigh, &key, &data, MDB_NEXT_DUP);
				if (rc == MDB_NOTFOUND) break;
				if (rc) return rc;

				// Using `MDB_APPENDDUP` for this can cause segfaults for unknown reasons.
				rc = mdb_cursor_put(curLow, &key, &data, 0);
				if (rc) return rc;
				i++;
			}

			// Deleting the keys one-by-one is required, as deleting all values using `mdb_cursor_del` with
			// `MDB_NODUPDATA` causes segfaults in some cases.
			rc = mdb_cursor_get(curHigh, &key, &data, MDB_FIRST);
			if (rc) return rc;
			rc = mdb_cursor_del(curHigh, 0);
			if (rc) return rc;

			while (true) {
				rc = mdb_cursor_get(curHigh, &key, &data, MDB_NEXT_DUP);
				if (rc == MDB_NOTFOUND) break;
				if (rc) return rc;
				rc = mdb_cursor_del(curHigh, 0);
				if (rc) return rc;
				i++;
			}
		} else {
			rc = mdb_cursor_del(curHigh, 0);
			if (rc) return rc;
		}
	}

	return 0;
}

int deleteAllSubDbs(bool dropDbis, MDB_txn* txn) {
	int rc;

	// Open main database, containing all sub-databases.
	MDB_dbi dbi;
	rc = mdb_open(txn, NULL, 0, &dbi);
	if (rc) return rc;

	MDB_cursor *cursor;
	rc = mdb_cursor_open(txn, dbi, &cursor);
	if (rc) return rc;

	char* str = malloc(MAX_MAPNAME_SIZE  + 1); // Assume that this is enough to store sub-database names.
	MDB_val key;
	while ((rc = mdb_cursor_get(cursor, &key, NULL, MDB_NEXT_NODUP)) == 0) {
		if (key.mv_size > MAX_MAPNAME_SIZE) {printf("unexpectedly large keysize\n", stderr); exit(1);}
		memcpy(str, key.mv_data, key.mv_size);
		str[key.mv_size] = '\0';
		MDB_dbi dbi2;
		rc = mdb_open(txn, str, 0, &dbi2);
		if (rc) break;

		rc = mdb_drop(txn, dbi2, dropDbis);
		if (rc) break;
	}
	free(str);
	if (rc == MDB_NOTFOUND) rc = MDB_SUCCESS;

	mdb_cursor_close(cursor);

	return rc;
}

int deleteEmptyMaps(MDB_txn* txn) {
	int rc;

	// Open main database, containing all sub-databases.
	MDB_dbi dbi, dbi2, dbi_prev;
	rc = mdb_open(txn, NULL, 0, &dbi);
	if (rc) return rc;

	MDB_cursor *cursor;
	rc = mdb_cursor_open(txn, dbi, &cursor);
	if (rc) return rc;

	char* str = malloc(MAX_MAPNAME_SIZE  + 1); // Assume that this is enough to store sub-database names.
	MDB_val key;
	MDB_stat mst;
	bool prev_was_empty = false;
	while ((rc = mdb_cursor_get(cursor, &key, NULL, MDB_NEXT_NODUP)) == 0) {
		const size_t size = key.mv_size;
		if (size > MAX_MAPNAME_SIZE) {printf("unexpectedly large keysize\n", stderr); exit(1);}
		memcpy(str, key.mv_data, size);
		str[size] = '\0';

		rc = mdb_open(txn, str, 0, &dbi2);
		if (rc) break;

		// This is assumed to be either `A` or `B`, for both map parts implementing the low/high part or
		// `L` for the map storing large values.
		const char last_char = str[size - 1];

		// The A and B map must either both be kept or both deleted if both are empty.
		if (last_char == 'B' && !prev_was_empty) continue;

		rc = mdb_stat(txn, dbi2, &mst);
		if (rc) break;

		const bool isEmpty = mst.ms_entries == 0;
		if (last_char == 'B' && isEmpty && prev_was_empty) {
			rc = mdb_drop(txn, dbi_prev, 1);
			if (rc) break;
			rc = mdb_drop(txn, dbi2, 1);
			if (rc) break;
		} else if (last_char != 'A' && isEmpty) {
			rc = mdb_drop(txn, dbi2, 1);
			if (rc) break;
		}

		dbi_prev = dbi2;
		prev_was_empty = isEmpty;
	}
	free(str);
	if (rc == MDB_NOTFOUND) rc = MDB_SUCCESS;

	mdb_cursor_close(cursor);

	return rc;
}

void getMdbEnvInfo
	(MDB_env* env, size_t* mapSize, size_t* pageSize, size_t* maxPages, size_t* nrPagesUsed, size_t* lastTxnId
	, size_t* maxReaders , size_t* nrReadersUsed) {

	MDB_stat mdbStat;
	MDB_envinfo mdbEnvInfo;
	(void)mdb_env_stat(env, &mdbStat);
	(void)mdb_env_info(env, &mdbEnvInfo);
	*mapSize = mdbEnvInfo.me_mapsize;
	*pageSize = mdbStat.ms_psize;
	*maxPages = mdbEnvInfo.me_mapsize / mdbStat.ms_psize;
	*nrPagesUsed = mdbEnvInfo.me_last_pgno+1;
	*lastTxnId = mdbEnvInfo.me_last_txnid;
	*maxReaders = mdbEnvInfo.me_maxreaders;
	*nrReadersUsed = mdbEnvInfo.me_numreaders;
}

void getMdbEnvInfoForPath
	(char* path, size_t* mapSize, size_t* pageSize, size_t* maxPages, size_t* nrPagesUsed, size_t* lastTxnId
	, size_t* maxReaders , size_t* nrReadersUsed) {
	MDB_env* env;
	mdb_env_create(&env);
	mdb_env_open(env, path, MDB_RDONLY, 0664);
	getMdbEnvInfo(env, mapSize, pageSize, maxPages, nrPagesUsed, lastTxnId, maxReaders, nrReadersUsed);
	mdb_env_close(env);
}

CleanString nameBufferForStatRetrieval = NULL;
MDB_cursor* cursorForStatRetrieval;

int initStatRetrieval(MDB_txn* txn) {
	// Open main database, containing all sub-databases.
	MDB_dbi dbi;
	int rc;
	rc = mdb_open(txn, NULL, 0, &dbi);
	if (rc) return rc;

	rc = mdb_cursor_open(txn, dbi, &cursorForStatRetrieval);
	if (rc) return rc;
}

void getNextStat(MDB_txn* txn, int* rc, CleanString* name, unsigned int* ms_psize, unsigned int* ms_depth,
	size_t* ms_branch_pages, size_t* ms_leaf_pages, size_t* ms_overflow_pages, size_t* ms_entries) {
	// Init the `name` result to an empty string to yield a proper results in case of error.
	if (nameBufferForStatRetrieval == NULL) nameBufferForStatRetrieval = malloc(sizeof(long));
	CleanStringLength(nameBufferForStatRetrieval) = 0;
	*name = nameBufferForStatRetrieval;
	MDB_val key;
	MDB_dbi dbi;
	MDB_stat stat;

	*rc = mdb_cursor_get(cursorForStatRetrieval, &key, NULL, MDB_NEXT_NODUP);
	if (*rc) {
		mdb_cursor_close(cursorForStatRetrieval);
		return;
	}

	const size_t size = key.mv_size;
	nameBufferForStatRetrieval = (CleanString)realloc(nameBufferForStatRetrieval, size + sizeof(long) + 1);
	*name = nameBufferForStatRetrieval;
	CleanStringLength(nameBufferForStatRetrieval) = size;
	memcpy(CleanStringCharacters(nameBufferForStatRetrieval), key.mv_data, size);
	CleanStringCharacters(nameBufferForStatRetrieval)[size] = '\0'; // zero-termination for `mdb_open`.

	*rc = mdb_open(txn, CleanStringCharacters(nameBufferForStatRetrieval), 0, &dbi);
	if (*rc) return;
	*rc = mdb_stat(txn, dbi, &stat);
	*ms_psize = stat.ms_psize; *ms_depth = stat.ms_depth; *ms_branch_pages = stat.ms_branch_pages;
	*ms_leaf_pages = stat.ms_leaf_pages; *ms_overflow_pages = stat.ms_overflow_pages; *ms_entries = stat.ms_entries;
}
