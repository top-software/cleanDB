implementation module Database.CleanDB.Gast

import Control.Applicative
import Control.GenBimap
import Control.Monad
import Data.Bool, Data.Func, Data.Functor
from Data.Foldable import minimum, maximum, class Foldable, instance Foldable []
import Data.List
import qualified Data.Map as Map
from Data.Map import instance Functor (Map k), :: Map
import Data.Maybe
import Data.OrdList
import qualified Data.Set as Set
from Data.Set import instance == (Set a), instance Foldable Set, :: Set
import Data.Set.GenPrint
import Data.Tuple
import Gast, Gast.Gen, graph_copy
import StdEnv
from StdFunc import seq
import StdOverloadedList
import System.Directory, System.File
import System.FilePath => qualified <.>
import System.SysCall
import System.Time => qualified time, clock
import System._Pointer, System._Posix, System._Unsafe
import qualified Text
from Text import class Text, instance Text String
import Text.GenPrint

import qualified Database.CleanDB as CDB
from Database.CleanDB import
	class key (..), class primitiveKey, :: KeyStr (..), :: CDB, :: CleanDBOptions {..},
	:: PredContinuation, instance key String, instance primitiveKey String,
	class dbMap, instance dbMap DBMultiMap, class readTxn, instance readTxn ReadOnlyTxn, :: ReadOnlyTxn,
	instance key (k1, k2), instance dbMap DBMap, instance key (k1, k2, k3),
	instance key (k1, k2, k3, k4), :: DBMap, :: DBMultiMap, :: Pointer, >&, >>&, >>>&, :: Condition,
	instance < (KeyStr k), class primitiveKey (..), :: KeyLength (..), :: Txn, :: SyncMode (NoSync),
	instance toString (KeyStr k), :: ParallelOperationsSetting (..),
	instance readTxn Txn, class isPartitionedMap, instance isPartitionedMap PartDbMap DBMap, :: PartDbMap,
	instance isPartitionedMap PartDbMultiMap DBMultiMap, :: PartDbMultiMap, :: GetContinuation (..),
	:: GetContinuationResult {..}
import qualified Database.CleanDB.KeyEncoding as KeyEncoding

withTestCleanDB :: !ParallelOperationsSetting !.(*CDB -> (.a, *CDB)) -> .a
withTestCleanDB parOpt func = withTestCleanDBWithPathPostfix "" parOpt func

withLabeledTestCleanDB :: !String !ParallelOperationsSetting !.(*CDB -> (.a, *CDB)) -> .a
withLabeledTestCleanDB label parOpt func = withTestCleanDBWithPathPostfix ("-" +++ label) parOpt func

withTestCleanDBWithPathPostfix :: !String !ParallelOperationsSetting !.(*CDB -> (.a, *CDB)) -> .a
withTestCleanDBWithPathPostfix postfix parOpt func = accUnsafe withTestCleanDB`
where
	withTestCleanDB` :: E.^ .a: !*World -> (!.a, !*World)
    withTestCleanDB` world
		# sizeDbsString = readInt storedTestDatabasePointers 0
		# databasesStr = derefCharArray (storedTestDatabasePointers + 8) sizeDbsString
		# (databases, _) = copy_from_string {#c \\ c <-: databasesStr}
		= case 'Map'.get postfix databases of
			?None // Database not opened yet.
				# dir        = 'Text'.concat3 "/tmp/testCleanDB-" (toString getpid) postfix
				// Remove possible leftover files to be sure.
				# (_, world) = deleteFile (dir </> "data.mdb") world
				# (_, world) = deleteFile (dir </> "lock.mdb") world
				# (db, world) =
					// With a low, but non-zero compaction factor we split even few test data items into low and high
					// map.
					'CDB'.openDatabase
						dir
						{'CDB'.defaultOptions
							& syncMode = NoSync, compactionFactor = 1, parallelOperationsSetting = parOpt}
						world
				# (res, db) = func db
				# (ptr, world) = malloc MAX_DB_STRING_SIZE world
				# world = storeDb ptr db world
				# world = storeDbMap ('Map'.put postfix ptr databases) world
				= (res, world)
			?Just storedTestDatabasePointer // Re-use already opened database.
				# size = readInt storedTestDatabasePointer 0
				# dbString = derefCharArray (storedTestDatabasePointer + 8) size
				# ((parOptOfStored, db), _) = copy_from_string {#c \\ c <-: dbString}
				# needToReopenDatabase = case parOpt of
					ParallelOperationsDisabled = False
					ParallelOperationsEnabled nr = case parOptOfStored of
						ParallelOperationsDisabled = True
						ParallelOperationsEnabled nrOfStored = case (nr, nrOfStored) of
							(?Just nr, ?Just nrOfStored) = nr > nrOfStored
							(?None,    ?None)            = False
							_                            = True
				| needToReopenDatabase
					# world = 'CDB'.closeDatabase db world
					# world = free storedTestDatabasePointer world
					# world = storeDbMap ('Map'.del postfix databases) world
					= withTestCleanDB` world
				// Delete all data before the next test.
				# (_, db) = 'CDB'.doTransaction (\txn = ((), 'CDB'.deleteDataFromAllMaps txn)) db
				# (res, db) = func db
				# world = storeDb storedTestDatabasePointer db world
				= (res, world)

	storeDbMap :: !(Map String Pointer) !*World -> *World
	storeDbMap dbMap world
		# databasesStr = copy_to_string dbMap
		# sizeDbsString = size databasesStr
		| sizeDbsString >= MAX_DB_STRING_SIZE =
			abort "Database.CleanDB.Gast.withTestCleanDB: unexpectedly large size of test database map\n"
		# storedTestDatabasePointers = writeInt storedTestDatabasePointers 0 sizeDbsString
		= memcpyStringToPointer (storedTestDatabasePointers + 8) databasesStr sizeDbsString world

	storeDb :: !Pointer !*CDB !*World -> *World
	storeDb pointer db world
		# dbString = copy_to_string (parOpt, db)
		# sizeDbString = size dbString
		| sizeDbString >= MAX_DB_STRING_SIZE = abort $ 'Text'.concat5
			"Database.CleanDB.Gast.withTestCleanDB: unexpectedly large size of encoded test database (max size: "
			(toString MAX_DB_STRING_SIZE) ", actual size: " (toString sizeDbString) ")\n"
		# pointer = writeInt pointer 0 sizeDbString
		= memcpyStringToPointer (pointer + 8) dbString sizeDbString world

MAX_DB_STRING_SIZE :== 65536 // We assume a maximum size of the encoded database to avoid malloc/free calls.

/**
 * This is a global pointer to a location at which the test databases are stored used by
 * `withTestCleanDB`/`withLabeledTestCleanDB`. The first integer this pointer points to indicates the size of the
 * encoded graph_copy string of a value of type `Map String Pointer`, where the `String` keys are the test DB postfixes
 * and the pointers point to the location the databases are stored. The actual graph_copy representation is stored in
 * the consecutive memory. The pointers in the map are structured similarly; the first integer indicates the size of
 * the consecutively stored graph_copy representation of the database.
 */
storedTestDatabasePointers :: Pointer
storedTestDatabasePointers =:
	(\x = if (x <> x) (abort "x has to be evaluated") (ptr - 8)) $
	appUnsafe
		(\env = memcpyStringToPointer
			ptr emptyMapString emptyMapStringSize env)
		0

where
	emptyMapString = copy_to_string 'Map'.newMap
	emptyMapStringSize = size emptyMapString
	ptr = writeInt (accUnsafe \env = malloc MAX_DB_STRING_SIZE env) 0 emptyMapStringSize + 8

getTupleKeyRange ::
	![(k1, k2)] !(!(!k1, !k1), !(!k2, !k2)) -> Property
	| TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k1 & TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k2
getTupleKeyRange keys range=:((first1, last1), (first2, last2)) = getKeys
	False False keys (>& ('CDB'.isBetween first1 last1) ('CDB'.isBetween first2 last2))
	(\(k1, k2) -> k1 >= first1 && k1 <= last1 && k2 >= first2 && k2 <= last2)

getTuple3KeyRange ::
	![(k1, k2, k3)] !(!(!k1, !k1), !(!k2, !k2), !(!k3, !k3)) -> Property
	| TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k1 & TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k2
	& TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k3
getTuple3KeyRange keys range=:((first1, last1), (first2, last2), (first3, last3)) = getKeys
	False False keys (>>& ('CDB'.isBetween first1 last1) ('CDB'.isBetween first2 last2) ('CDB'.isBetween first3 last3))
	(\(k1, k2, k3) -> k1 >= first1 && k1 <= last1 && k2 >= first2 && k2 <= last2 && k3 >= first3 && k3 <= last3)

getTuple4KeyRange ::
	![(k1, k2, k3, k4)] !(!(!k1, !k1), !(!k2, !k2), !(!k3, !k3), !(!k4, !k4)) -> Property
	| TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k1 & TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k2
	& TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k3 & TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k4
getTuple4KeyRange keys range=:((first1, last1), (first2, last2), (first3, last3), (first4, last4)) = getKeys
	False False keys
	( >>>&
		('CDB'.isBetween first1 last1) ('CDB'.isBetween first2 last2)
		('CDB'.isBetween first3 last3) ('CDB'.isBetween first4 last4))
	( \(k1, k2, k3, k4) -> k1 >= first1 && k1 <= last1 && k2 >= first2 && k2 <= last2 &&
							k3 >= first3 && k3 <= last3 && k4 >= first4 && k4 <= last4)

getAllKeys :: !Bool ![k] -> Property | TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, key k
getAllKeys useMultiMap keys = getKeys useMultiMap False keys 'CDB'.all $ const True

getAllTupleKeys ::
	![(k1, k2)] -> Property
	| TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k1 & TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k2
getAllTupleKeys keys = getKeys False False keys (>& 'CDB'.all 'CDB'.all) $ const True

getSingleKey :: !Bool![k] !k -> Property | TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, key k
getSingleKey usePartitionedMap keys const = getKeys False usePartitionedMap keys ('CDB'.equals const) $ (==) const

getSingleTupleKey ::
	![(k1, k2)] !(!k1, !k2) -> Property
	| TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k1 & TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k2
getSingleTupleKey keys const = getKeys False False keys ('CDB'.equals const) $ (==) const

:: InequalityCondition a = LessThan !a | LessThanOrEqual !a | GreaterThan !a | GreaterThanOrEqual !a

getKeysObeyingIneq ::
	![k] !(InequalityCondition k) -> Property | TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k
getKeysObeyingIneq keys ineq = getKeys False False keys (ineqConditionFor ineq) $ ineqHolds ineq

getTupleKeysObeyingIneq ::
	![(k1, k2)] !(!InequalityCondition k1, !InequalityCondition k2) -> Property
	| TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k1 & TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k2
getTupleKeysObeyingIneq keys (ineq1, ineq2) = getKeys
	False False keys (>& (ineqConditionFor ineq1) (ineqConditionFor ineq2))
	(\(k1, k2) -> ineqHolds ineq1 k1 && ineqHolds ineq2 k2)

ineqHolds :: !(InequalityCondition k) !k -> Bool | < k
ineqHolds (LessThan           v) k = k <  v
ineqHolds (LessThanOrEqual    v) k = k <= v
ineqHolds (GreaterThan        v) k = k >  v
ineqHolds (GreaterThanOrEqual v) k = k >= v

ineqConditionFor :: !(InequalityCondition k) -> Condition k | primitiveKey k
ineqConditionFor (LessThan           v) = 'CDB'.lessThan v
ineqConditionFor (LessThanOrEqual    v) = 'CDB'.lessThanOrEqual v
ineqConditionFor (GreaterThan        v) = 'CDB'.greaterThan v
ineqConditionFor (GreaterThanOrEqual v) = 'CDB'.greaterThanOrEqual v

getKeyRange :: ![k] !(!k, !k) -> Property | TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, key k
getKeyRange keys (first, last) = getKeys False False keys ('CDB'.isBetween first last) (\k -> k >= first && k <= last)

getKeySet :: ![k] ![k] -> Property | TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, key k
getKeySet keys keySetList = getKeys False False keys ('CDB'.isIn keySet) (\k -> 'Set'.member k keySet)
where
	keySet = 'Set'.fromList keySetList

getTupleKeySet ::
	![(k1, k2)] !(![k1], ![k2]) -> Property
	| TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k1 & TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k2
getTupleKeySet keys (keys1, keys2) = getKeys
	False False keys (>& ('CDB'.isIn keySet1) ('CDB'.isIn keySet2))
	(\(k1, k2) -> 'Set'.member k1 keySet1 && 'Set'.member k2 keySet2)
where
	keySet1 = 'Set'.fromList keys1
	keySet2 = 'Set'.fromList keys2

getKeyNearValue :: ![k] !(Near k) -> Property | TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, key k
getKeyNearValue keys cond = getKeys False False keys (nearConditionFor cond) $ (==) expectedKey o ?Just
where
	expectedKey = keyNearValue keys cond

getTupleKeyNearValue ::
	![(k1, k2)] !(!Near k1, !Near k2) -> Property
	| TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k1 & TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k2
getTupleKeyNearValue keys (cond1, cond2) =
	getKeys False False keys (>& (nearConditionFor cond1) (nearConditionFor cond2)) $ (==) expectedKeyPair o ?Just
where
	expectedKeyPair =
		keyNearValue (fst <$> keys) cond1                          >>= \key1 ->
		keyNearValue (snd <$> filter ((==) key1 o fst) keys) cond2 >>= \key2 ->
		?Just (key1, key2)

getTuple3KeyNearValues ::
	![(k1, k2, k3)] !(!Near k1, !Near k2, !Near k3) -> Property
	| TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k1 & TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k2
	& TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, primitiveKey k3
getTuple3KeyNearValues keys (cond1, cond2, cond3) = getKeys
	False False keys (>>& (nearConditionFor cond1) (nearConditionFor cond2) (nearConditionFor cond3))
	((==) expectedKeyTriple o ?Just)
where
	expectedKeyTriple =
		keyNearValue (fst3 <$> keys)                                                               cond1 >>= \key1 ->
		keyNearValue (snd3 <$> filter ((==) key1 o fst3) keys)                                     cond2 >>= \key2 ->
		keyNearValue (thd3 <$> filter (\(key1`, key2`, _) -> key1` == key1 && key2` == key2) keys) cond3 >>= \key3 ->
		?Just (key1, key2, key3)

nearConditionFor :: !(Near k) -> Condition k | key k
nearConditionFor (LargestLessThanOrEq     k) = 'CDB'.largestLessThanOrEq k
nearConditionFor (SmallestGreaterThanOrEq k) = 'CDB'.smallestGreaterThanOrEq k

keyNearValue :: ![k] !(Near k) -> ?k | ==, Ord k
keyNearValue keys cond
	| isEmpty keysForWhichCondHolds = ?None
	| otherwise                     = ?Just $ if isLessThanOrEq maximum minimum keysForWhichCondHolds
where
	(condPoint, isLessThanOrEq)
		= case cond of
			LargestLessThanOrEq k     = (k, True)
			SmallestGreaterThanOrEq k = (k, False)

	keysForWhichCondHolds = filter (if isLessThanOrEq (>=) (<=) $ condPoint) keys

getKeys :: !Bool !Bool ![k] !(Condition k) !(k -> Bool) -> Property | TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, key k
getKeys useMultiMap usePartitionedMap keys cond shouldBeInRes = withTestCleanDB ParallelOperationsDisabled getKeys`
where
    getKeys` cdb
		// Do each put in a separate transaction to perform compaction more than once.
		// This is required to properly test the effect of compaction.
        # cdb = foldl (\cdb k -> snd $ 'CDB'.doTransaction (\txn -> ((), put k txn)) cdb) cdb keys
        # (readKeys, cdb) = 'CDB'.doTransaction get cdb
        = ('Set'.toList ('Set'.fromList $ filter shouldBeInRes keys) =.= (reverse readKeys), cdb)

	put :: E.^ k: !k !*Txn -> *Txn | key k
	put k txn
		// Add two values to test an actual multi-map with more than one value.
		| useMultiMap = 'CDB'.put k "a" multiMap $ 'CDB'.put k "b" multiMap txn
		| otherwise = 'CDB'.put k () map txn

	get :: E.^ k: !*Txn -> (![k], !*Txn) | Eq, key k
	get txn
		| useMultiMap =
			// We use the "keys only" variant to test it and as values are not relevant for the test.
			'CDB'.getMultiAccumKeysOnly cond (\k acc = (False, [k: acc])) [] multiMap txn
		| otherwise =
			// Delete all retrieved keys. This should not influence the results, so be deleting the keys we test that
			// this is the case indeed.
			appFst (fmap fst) $ 'CDB'.getAccumDelete cond (\k v acc = (False, True, [(k, v): acc])) [] map txn

	map = if usePartitionedMap ('CDB'.withSelectedPartition "some part" $ 'CDB'.partitionedMap map`) map`
    map` = 'CDB'.dbMap (keyType +++ "Test") (const "") (const ())

	multiMap = if usePartitionedMap ('CDB'.withSelectedPartition "some part" $ 'CDB'.partitionedMap multiMap`) multiMap`
    multiMap` = 'CDB'.dbMultiMap (keyType +++ "TestMulti") id id

	keyType = toString $ typeCodeOfDynamic (dynamic undef :: k^)

getKeysAndValues ::
	![(k, v)] !(Condition k) !(k -> Bool) -> Property
	| TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, key k & Eq, genShow{|*|}, gPrint{|*|}, key v
getKeysAndValues keyVals cond shouldBeInRes = withTestCleanDB ParallelOperationsDisabled getKeysAndValues`
where
	getKeysAndValues` :: !*CDB -> (!Property, !*CDB)
    getKeysAndValues` cdb
		// Do each put in a separate transaction to perform compaction more than once.
		// This is required to properly test the effect of compaction.
        # cdb = foldl (\cdb (k, v) -> snd $ 'CDB'.doTransaction (\txn -> ((), 'CDB'.put k v map txn)) cdb) cdb keyVals
		// Delete all retrieved keys. This should not influence the results, so be deleting the keys we test that this
		// is the case indeed.
        # (readKeys, cdb) =
			'CDB'.doTransaction ('CDB'.getAccumDelete cond (\k v acc = (False, True, [(k, v): acc])) [] map) cdb
        = ( [(k, v) \\ (k, v) <- keyVals | shouldBeInRes k]  =.= reverse readKeys, cdb)

    map = 'KeyEncoding'.dbMap (keyType +++ "Test")

	keyType = toString $ typeCodeOfDynamic (dynamic undef :: k^)

getKeysAndValuesUsingContinuationChain ::
	!(Condition k) !(k -> Bool) ![!(k, v)] ![!(k, v)] -> Property
	| TC, Eq, genShow{|*|}, gPrint{|*|}, Ord, key k & Eq, genShow{|*|}, gPrint{|*|}, Ord, key v
getKeysAndValuesUsingContinuationChain condition shouldBeInRes mapOneContents` mapTwoContents` =
	withTestCleanDB (ParallelOperationsEnabled nrOfParallelTransactionsToTest) getKeysAndValues`
where
	mapOneContents = RemoveDupBy (\(k0, _) (k1, _) = k0 == k1) mapOneContents`
	mapTwoContents = RemoveDupBy (\(k0, _) (k1, _) = k0 == k1) mapTwoContents`
	getKeysAndValues` :: !*CDB -> (!Property, !*CDB)
	getKeysAndValues` cdb
		// Do each put in a separate transaction to perform compaction more than once.
		// This is required to properly test the effect of compaction.
		# cdb = Foldl
			(\cdb (k, v) = snd $ 'CDB'.doTransaction (\txn -> ((), 'CDB'.put k v mapOne txn)) cdb) cdb mapOneContents
		# cdb = Foldl
			(\cdb (k, v) = snd $ 'CDB'.doTransaction (\txn -> ((), 'CDB'.put k v mapTwo txn)) cdb) cdb mapTwoContents
		// Delete all retrieved keys. This should not influence the results, so be deleting the keys we test that this
		// is the case indeed.
		# (readKeys, cdb) = 'CDB'.doReadOnlyTransaction
			('CDB'.getAccumNestedParallelWithLocalStateAndContinuationChain
				continuationChain tuple3 (\keyValPair acc txn = (False, [!keyValPair:acc], txn))
				() () (const ()) [!] nrOfParallelTransactionsToTest) cdb
		= ( Sort [!(k, v) \\ (k, v) <|- mapOneContents ++| mapTwoContents | shouldBeInRes k] =.= Sort readKeys, cdb)

	continuationChain = GetContinuation (\contSt txn =
		(	{condition = condition, map = mapOne, paramFor = \k v kvSt = (False, ?Just (k, v), kvSt)
			, nextContinuation = continuationForMapTwo}, contSt, txn))

	continuationForMapTwo = GetContinuation (\contSt txn =
		(	{condition = condition, map = mapTwo, paramFor = \k v kvSt = (False, ?Just (k, v), kvSt)
			, nextContinuation = NopContinuation}, contSt, txn))
	mapOne = 'KeyEncoding'.dbMap (keyType +++ "TestMapOne")
	mapTwo = 'KeyEncoding'.dbMap (keyType +++ "TestMapTwo")

	keyType = toString $ typeCodeOfDynamic (dynamic undef :: k^)

getSingleKeyAndValue :: !k !(?v) !v -> Property | key, TC k & key, genShow{|*|}, gPrint{|*|}, == v
getSingleKeyAndValue k prevValue v = withTestCleanDB ParallelOperationsDisabled getSingleKeyAndValue`
where
	getSingleKeyAndValue` :: !*CDB -> (!Property, !*CDB)
    getSingleKeyAndValue` cdb
		// Do each put in a separate transaction to perform compaction more than once.
		// This is required to properly test the effect of compaction.
        # cdb = maybeSt cdb (\v = snd o 'CDB'.doTransaction (\txn -> ((), 'CDB'.put k v map txn))) prevValue
        # cdb = snd $ 'CDB'.doTransaction (\txn -> ((), 'CDB'.put k v map txn)) cdb
		// Delete all retrieved keys. This should not influence the results, so be deleting the keys we test that this
		// is the case indeed.
        # (mbVal, cdb) =
			'CDB'.doTransaction ('CDB'.getSingle k map) cdb
		# mbVal = case mbVal of
			?None = mbVal
			?Just val = mbVal
        = (mbVal =.= ?Just v, cdb)

    map = 'KeyEncoding'.dbMap (keyType +++ "Test")

	keyType = toString $ typeCodeOfDynamic (dynamic undef :: k^)

getSingleKeyAndValuesOfMultiMap ::
	![(k, v)] !k -> Property
	| TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, key k & ==, Ord, genShow{|*|}, gPrint{|*|}, key v
getSingleKeyAndValuesOfMultiMap keysAndValues key = withTestCleanDB ParallelOperationsDisabled getKeyAndValues
where
	getKeyAndValues :: !*CDB -> (!Property, !*CDB)
	getKeyAndValues cdb
		# cdb  = addValuesToMultiMap keysAndValues map cdb
		# (readKeys, cdb) = 'CDB'.doReadOnlyTransaction ('CDB'.getSingleMulti key map) cdb
		=	(		('Set'.toList $
						'Map'.findWithDefault
							'Set'.newSet
							key
							('Map'.unionsWith
								'Set'.union
								[ 'Map'.singleton key $ 'Set'.singleton val
								\\ (key`, val) <- keysAndValues | key` == key
								]
							)
					)
				=.=
					readKeys
			, cdb
			)

	withKeyAndValues txn (key, values) = foldl (\txn val -> 'CDB'.put key val map txn) txn values

	map = 'KeyEncoding'.dbMultiMap $ keyType +++ "MultiTest"

	keyType = toString $ typeCodeOfDynamic (dynamic undef :: k^)

getKeysAndValuesOfMultiMap ::
	![(k, v)] !(Condition k) !(k -> Bool) -> Property
	| TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, key k & ==, Ord, genShow{|*|}, gPrint{|*|}, key v
getKeysAndValuesOfMultiMap keysAndValues cond shouldBeInRes =
	withTestCleanDB ParallelOperationsDisabled getKeysAndValues
where
	getKeysAndValues :: !*CDB -> (!Property, !*CDB)
	getKeysAndValues cdb
		# cdb = addValuesToMultiMap keysAndValues map cdb
		# (readKeys, cdb) = 'CDB'.doReadOnlyTransaction ('CDB'.getMulti cond map) cdb
		=	(		[ (key, 'Set'.toList vals)
					\\ (key, vals) <-
						'Map'.toList $
							'Map'.unionsWith
								'Set'.union
								[ 'Map'.singleton key $ 'Set'.singleton val
								\\ (key, val) <- keysAndValues | shouldBeInRes key]
					]
				=.=
					reverse readKeys
			, cdb
			)

	map = 'KeyEncoding'.dbMultiMap $ keyType +++ "MultiTest"

	keyType = toString $ typeCodeOfDynamic (dynamic undef :: k^)

getKeyRangeInParallel ::
	![!(k, v)] !(!k, !k) !(?k) -> Property
	| TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, key k & Eq, Ord, genShow{|*|}, gPrint{|*|}, key v
getKeyRangeInParallel keys range=:(first, last) stopOn =
	getKeyValuesInParallel
		keys ('CDB'.isBetween first last) (maybe (\_ -> False) (==) stopOn)
		(\k -> isInRange range k && (stopKeyIncludedInRange --> ?Just k <= stopOn))
where
	stopKeyIncludedInRange = maybe False (\stopOn -> Any (\(k, _) -> isInRange range k && k == stopOn) keys) stopOn

getKeyValuesInParallel ::
	![!(k, v)] !(Condition k) !(k -> Bool) !(k -> Bool) -> Property
	| TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, key k & Eq, Ord, genShow{|*|}, gPrint{|*|}, key v
getKeyValuesInParallel keys cond stopCond shouldBeInRes =
	getKeyValuesInParallelParam getInParallel pairsToCompare map keys cond
where
	getInParallel :: E.^k v: !*ReadOnlyTxn -> (![!(k, v)], !*ReadOnlyTxn) | key k
	getInParallel txn
		# (pairs, txn) = 'CDB'.getAccumNestedParallel
			cond (\k _ st = (False, ?Just k, st)) nestedTxn updFor () [!] map nrOfParallelTransactionsToTest txn
		= (Reverse pairs, txn)

	nestedTxn :: E.^ k v: !k !*ReadOnlyTxn -> (!(!k, !v), !*ReadOnlyTxn) | key k
	nestedTxn k txn
		# (?Just v, txn) = 'CDB'.getSingle k map txn
		= ((k, v), txn)

	updFor :: E.^ k: !(!k, !v) ![!(k, v)] !*ReadOnlyTxn -> (!Bool, ![!(k, v)], !*ReadOnlyTxn)
	updFor keyVal=:(k, _) acc txn = (stopCond k, [!keyVal: acc], txn)

	pairsToCompare :: E.^ k: ![!(k, v)] -> [!(k, v)] | ==, < k
	pairsToCompare pairs = 'Map'.ToList $ 'Map'.FromList $ Filter (shouldBeInRes o fst) pairs

	map = 'KeyEncoding'.dbMap $ keyType +++ "Test"

	keyType = toString $ typeCodeOfDynamic (dynamic undef :: k^)

getKeyRangeInParallelMulti ::
	![!(k, v)] !(!k, !k) !(?k) -> Property
	| TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, key k & Eq, Ord, genShow{|*|}, gPrint{|*|}, key v
getKeyRangeInParallelMulti keys range=:(first, last) stopOn =
	getKeyMultiValuesInParallel
		keys ('CDB'.isBetween first last) (maybe (\_ -> False) (==) stopOn)
		(\k -> isInRange range k && (stopKeyIncludedInRange --> ?Just k < stopOn))
where
	stopKeyIncludedInRange = maybe False (\stopOn -> Any (\(k, _) -> isInRange range k && k == stopOn) keys) stopOn

getKeyMultiValuesInParallel ::
	![!(k, v)] !(Condition k) !(k -> Bool) !(k -> Bool) -> Property
	| TC, Eq, Ord, genShow{|*|}, gPrint{|*|}, key k & Eq, Ord, genShow{|*|}, gPrint{|*|}, key v
getKeyMultiValuesInParallel keys cond stopCond shouldBeInRes =
	getKeyValuesInParallelParam getInParallel pairsToCompare map keys cond
where
	getInParallel :: E.^k v: !*ReadOnlyTxn -> (![!(k, [v])], !*ReadOnlyTxn) | ==, <, key k
	getInParallel txn
		# (pairs, txn) =
			('CDB'.getMultiAccumNestedParallel
				cond (\k _ st = (False, ?Just k, st)) nestedTxn updFor () [!] map nrOfParallelTransactionsToTest
				txn)
		= ('Map'.ToList $ 'Map'.FromList pairs, txn)

	nestedTxn :: E.^ k v: !k !*ReadOnlyTxn -> (!?(k, [v]), *ReadOnlyTxn) | key k
	nestedTxn k txn
		| stopCond k = (?None, txn)
		# (vs, txn) = 'CDB'.getSingleMulti k map txn
		= (?Just (k, vs), txn)

	updFor :: !(?(k, [v])) ![!(k, [v])] !*ReadOnlyTxn -> (!Bool, ![!(k, [v])], !*ReadOnlyTxn)
	updFor ?None acc txn = (True, acc, txn)
	updFor (?Just keyVals) acc txn = (False, [!keyVals: acc], txn)

	pairsToCompare :: E.^ k: ![!(k, v)] -> [!(k, [v])] | < k & < v
	pairsToCompare pairs =
		'Map'.ToList $
			fmap 'Set'.toList $
				'Map'.unionsWith
					'Set'.union ['Map'.singleton k ('Set'.singleton v) \\ (k, v) <|- pairs | shouldBeInRes k]

    map = 'KeyEncoding'.dbMultiMap $ keyType +++ "MultiTest"

	keyType = toString $ typeCodeOfDynamic (dynamic undef :: k^)

isInRange :: !(!k, !k) !k -> Bool | < k
isInRange (first, last) k = k >= first && k <= last

// We need at least 2 parallel transactions to have a proper test.
// Using more transactions would however only waste resources without adding much to the test.
nrOfParallelTransactionsToTest :: ?Int
nrOfParallelTransactionsToTest =: ?Just 2

getKeyValuesInParallelParam ::
	!(*ReadOnlyTxn -> ([!(k, p)], *ReadOnlyTxn)) !([!(k, v)] -> [!(k, p)]) !(map k v) ![!(k, v)] !(Condition k)
	-> Property
	| Eq, Ord, genShow{|*|}, gPrint{|*|}, key k & Eq, Ord, genShow{|*|}, gPrint{|*|}, key v
	& Eq, genShow{|*|}, gPrint{|*|} p & dbMap map
getKeyValuesInParallelParam getInParallel pairsToCompare map pairs cond =
	withTestCleanDB (ParallelOperationsEnabled nrOfParallelTransactionsToTest) getKeys`
where
	getKeys` :: !*CDB -> (!Property, !*CDB)
	getKeys` cdb
		// Do each put in a separate transaction to perform compaction more than once.
		// This is required to properly test the effect of compaction.
		# cdb = Foldl (\cdb (k, v) -> snd $ 'CDB'.doTransaction (\txn -> ((), 'CDB'.put k v map txn)) cdb) cdb pairs
		# (readKeys, cdb) = 'CDB'.doReadOnlyTransaction getInParallel cdb
		= (pairsToCompare pairs =.= readKeys, cdb)

addValuesToMultiMap :: ![(k, v)] !(DBMultiMap k v) !*CDB -> *CDB | key k
addValuesToMultiMap keysAndValues map cdb
	# (_, cdb) = 'CDB'.doTransaction (\txn -> ((), foldl withKeyAndValues txn keysAndValues)) cdb
	= cdb
where
	withKeyAndValues :: E.^ k v: !*Txn !(!k, !v) -> *Txn | key k
	withKeyAndValues txn (key, value) = 'CDB'.put key value map txn

// FIXME: this will overwrite existing values with equal keys
getKeysUsingMap ::
	[(k, v)] (Condition k) (k -> Bool) !('CDB'.DBMap k v) -> Property
	| Eq, Ord, genShow{|*|}, gPrint{|*|}, key k & Eq, Ord, genShow{|*|}, gPrint{|*|} v
getKeysUsingMap keyValuePairs cond shouldBeInRes map = withTestCleanDB ParallelOperationsDisabled getKeys`
where
    getKeys` cdb
        # (_,         cdb) = 'CDB'.doTransaction (\txn -> ((), seq ['CDB'.put k v map \\ (k, v) <- keyValuePairs] txn)) cdb
        # (readPairs, cdb) = 'CDB'.doReadOnlyTransaction ('CDB'.get cond map) cdb
        = ('Set'.toList ('Set'.fromList $ filter (shouldBeInRes o fst) keyValuePairs) =.= reverse readPairs, cdb)

// TODO: also check order (as for `getKeys` & `getKeysUsingMap`)
getKeysUsingMultiMap ::
	[(k, v)] (Condition k) (k -> Bool) !('CDB'.DBMultiMap k v) -> Property
	| Eq, Ord, genShow{|*|}, gPrint{|*|}, key k & Eq, Ord, genShow{|*|}, gPrint{|*|} v
getKeysUsingMultiMap keyValuePairs cond shouldBeInRes map = withTestCleanDB ParallelOperationsDisabled getKeys`
where
    getKeys` cdb
        # (_,         cdb) = 'CDB'.doTransaction (\txn -> ((), seq ['CDB'.put k v map \\ (k, v) <- keyValuePairs] txn)) cdb
        # (readPairs, cdb) = 'CDB'.doReadOnlyTransaction ('CDB'.getMulti cond map) cdb
        # readPairs        = flatten [[(key, value) \\ value <- values] \\ (key, values) <- readPairs]
        = ('Set'.fromList (filter (shouldBeInRes o fst) keyValuePairs) =.= 'Set'.fromList readPairs, cdb)

derive gPrint KeyString
derive genShow KeyString
instance < KeyString where
	(<) (KeyString x) (KeyString y) = x < y

instance == KeyString where
	(==) (KeyString x) (KeyString y) = x == y

instance key KeyString where
	strRepresentation     (KeyString    str)  = unsafeCoerce $ 'CDB'.strRepresentation str
	fromStrRepresentation ('CDB'.KeyStr kstr) = KeyString ('CDB'.fromStrRepresentation $ 'CDB'.KeyStr kstr)
	hasFixedSize _ = False

instance primitiveKey KeyString where
	keyLength = unsafeCoerce strKeyLength
	where
		strKeyLength :: 'CDB'.KeyLength String
		strKeyLength = 'CDB'.keyLength

	nextLargerKey (KeyString str) = ?Just $ KeyString (str +++ "\0")
	smallestKey = KeyString smallestKey

ggen{|KeyString|} _ =
	(\s -> KeyString s) <$>
		// As 0 and 1-bytes are escaped, we generate test strings with such bytes.
	(	[!"\0", "\1": [!{c1, c2} \\ c1 <|- [!'\0', '\1'], c2 <|- [!'\0', '\1']]] ++|
		// simple strings and a max length string
		// it is important that the are several keys sharing a common prefix to cover all relevant test cases
		Flatten [![!"key" +++ toString i, "key" +++ toString i +++ "a"] \\ i <- [1..3]] ++|
		[!{'*' \\ _ <|- [!1..maxLength]}])
where
	// the length of composed keys is restricted to 255 bytes
	// as we test with 3 keys max, we have to consider the combination of 2 keys,
	// so we divide by 2 (and subtract 3 for overhead and possible escaping of 0 and 1 bytes)
	maxLength = (255 / 2) - 3

derive gPrint ValueString
derive genShow ValueString
ggen{|ValueString|} _ = (\str -> ValueString str) <$> ggenString 16 4.0 0 255 aStream

derive class Gast InequalityCondition, Near

instance == (a, b, c, d) | == a & == b & == c & == d where
	(==) (a1, b1, c1, d1) (a2, b2, c2, d2) = a1 == a2 && b1 == b2 && c1 == c2 && d1 == d2

instance <  (a, b, c, d) | Ord a & Ord b & Ord c & Ord d where
    (<) ::!(a, b, c, d) !(a, b, c, d) -> Bool | Ord a & Ord b & Ord c & Ord d
    (<) (x1,y1,z1,a1) (x2,y2,z2,a2) | x1 < x2   = True
                                    | x1 > x2   = False
                                    | otherwise = (y1, z1, a1) < (y2, z2, a2)

toFromEqualsOriginal :: !a -> Property | Eq, key, genShow{|*|}, gPrint{|*|} a
toFromEqualsOriginal k = fromStrRepresentation (strRepresentation k) =.= k

keyOrderIsPreservedForPair :: !a !a -> Property | <, key a
keyOrderIsPreservedForPair x y = (x < y) =.= (strRepresentation x < strRepresentation y)

keyLengthIsCorrect :: !a -> Property | primitiveKey a
keyLengthIsCorrect k = maybe (prop True) (\length -> size str =.= length) $ keyLengthOf keyLength keyStr
where
	(keyStr =: (KeyStr str)) = strRepresentation k

	keyLengthOf :: !(KeyLength k) !(KeyStr k) -> ?Int
	keyLengthOf keyLength (KeyStr keyStr) =
		case keyLength of
			FixedLength l          = ?Just l
			DynamicLength lengthOf = (\lengthOf -> lengthOf keyStr) <$> lengthOf

keyHasFixedSizeImpliesAllStrRepresentationsFixedLength :: !a !a -> Property | key a
keyHasFixedSizeImpliesAllStrRepresentationsFixedLength v1 v2 =
	not (hasFixedSize $ undefOfType v1) \/
	size (toString $ strRepresentation v1) =.= size (toString $ strRepresentation v2)
where
	// `hasFixedSize` is expected to work with `undef`, as we usually only have a type and no value when using it.
	undefOfType :: v1 -> v1
	undefOfType _ = undef

nextLargerKeyIsLarger :: !a -> Property | Ord, primitiveKey, genShow{|*|}, gPrint{|*|} a
nextLargerKeyIsLarger k = maybe (prop True) (\k` -> k` >. k) $ nextLargerKey k

noKeyInBetweenNextLargerKey :: !a -> Property | Ord, primitiveKey, TestArg a
noKeyInBetweenNextLargerKey k = maybe (prop True) (\k` -> (ForAll \k`` -> k`` <=. k \/ k`` >=. k`)) $ nextLargerKey k

noKeySmallerThanSmallest :: (Type k) -> Property | Ord, primitiveKey, TestArg k
noKeySmallerThanSmallest type = ForAll $ isNotLessThan type
where
	isNotLessThan :: (Type k) !k -> Property | Ord, primitiveKey, TestArg k
	isNotLessThan _ k = k >=. smallestKey

checkEqualityOfMap ::
	!(Map k v) !(DBMap k v) !*txn -> (!Property, !*txn)
	| Eq, key, genShow{|*|}, gPrint{|*|} k & Eq, genShow{|*|}, gPrint{|*|} v & readTxn txn
checkEqualityOfMap map dbMap txn
	# (dbMapContent, txn) = 'CDB'.get 'CDB'.all dbMap txn
	= (reverse dbMapContent =.= 'Map'.toList map, txn)

checkEqualityOfMultiMap ::
	!(Map k (Set v)) !(DBMultiMap k v) !*txn -> (!Property, !*txn)
	| Eq, key, genShow{|*|}, gPrint{|*|} k & Eq, genShow{|*|}, gPrint{|*|} v & readTxn txn
checkEqualityOfMultiMap map dbMap txn
	# (dbMapContent, txn) = 'CDB'.getMulti 'CDB'.all dbMap txn
	= (reverse dbMapContent =.= (appSnd 'Set'.toList <$> filter (not o 'Set'.null o snd) ('Map'.toList map)), txn)
