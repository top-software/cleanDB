implementation module Database.CleanDB.Binary

import StdEnv => qualified all
import Data.Maybe
import Data.Encoding.GenBinary
import Database.CleanDB
import qualified Database.CleanDB as CDB

dbMap :: !String -> DBMap k v | GenBinary v
dbMap name = 'CDB'.dbMap name encode (fromMaybe (abort "invalid binary DB encoding\n") o decode)

dbMultiMap :: !String -> DBMultiMap k v | GenBinary v
dbMultiMap name = 'CDB'.dbMultiMap name encode (fromMaybe (abort "invalid binary DB encoding\n") o decode)
