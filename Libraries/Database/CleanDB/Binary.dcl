definition module Database.CleanDB.Binary

from Data.Encoding.GenBinary import class GenBinary, generic gBinaryDecode, generic gBinaryEncodingSize,
                                    generic gBinaryEncode, :: EncodingSt
from Database.CleanDB        import :: DBMap, :: DBMultiMap

dbMap :: !String -> DBMap k v | GenBinary v
dbMultiMap :: !String -> DBMultiMap k v  | GenBinary v
