implementation module Database.CleanDB

from Control.Applicative import class <*> (..), instance <*> ?
from Control.Monad import class Monad(bind), >>=
import Data.Array, Data.Error, Data.Func, Data.Functor, Data.GenEq, Data.Int
from Data.Map import :: Map, derive gEq Map, instance Functor (Map k)
import qualified Data.Map as Map
import Data.Maybe
from Data.Set import :: Set, instance Foldable Set, class Foldable
import qualified Data.Set as Set
import Data.Tuple
import Data._Array
import StdDebug
import StdEnv => qualified all
import StdOverloadedList
import System.Directory, System.File
from System.FilePath import :: FilePath, </>, dropDirectory
import System.LockFile
import System.OSError, System.Process, System.SubProcess, System.SysCall
import System.Time => qualified clock, time
import System._Memory, System._Linux, System._Pointer, System._Posix, System._Signal, System._Unsafe
import Text, Text.Unicode.Encodings.UTF8

import Database.CleanDB._LMDB

// This is used for mixing channels of different type in a list, for cases the type does not matter, e.g. for closing.
:: UntypedInChannel = E.a: UntypedInChannel !(InChannel a)
:: UntypedOutChannel = E.a: UntypedOutChannel !(OutChannel a)

openDatabase :: !FilePath !CleanDBOptions !*World -> (!*CDB, !*World)
openDatabase
	path
	options=:
		{ syncMode, readahead, compactionFactor, maxNumberPages, readaheadPartitionDatabases
		, maxNumberPagesPartitionDatabases, parallelOperationsSetting}
	world
	# (getCondAndPassResultsProccess, processPool, world) = case parallelOperationsSetting of
		ParallelOperationsDisabled
			= (ParallelGetDisabled, emptyProcessPool, world)
		ParallelOperationsEnabled nrProcs
			# (nrProcs, world) = case nrProcs of
				?Just nr = (nr, world)
				?None    = getNumberOfAvailableProcessors world
			# (pool, world) = processPoolOfSize nrProcs world
			# (getAndPassProc, world) = forkGetAndPassProc pool world
			= (getAndPassProc, pool, world)
	= openDatabaseWithoutCreatingSubProcs
		path syncMode readahead readaheadPartitionDatabases maxNumberPagesPartitionDatabases compactionFactor
		maxNumberPages getCondAndPassResultsProccess processPool world
where
	processPoolOfSize :: !Int !*World -> (!ProcessPool, !*World)
	processPoolOfSize nrProcesses world
		# (processes, _, _, world) =
			Foldl forkNestedSubProcess (unsafeCreateArray nrProcesses, [!], [!], world) [!0..nrProcesses-1]
		= (ProcessPool processes, world)
	where
		// Accumulate open channels of already opened child processes, which are inherited by child processes forked
		// afterwards and should be closed by such on startup.
		forkNestedSubProcess ::
			!(*{#NestedSubProcess parParam .locSt partRes}, ![!UntypedInChannel], ![!UntypedOutChannel], !*World) !Int
			-> ( !*{#NestedSubProcess parParam .locSt partRes}, ![!UntypedInChannel], ![!UntypedOutChannel], !*World)
		forkNestedSubProcess (processes, inChannelsToClose, outChannelsToClose, world) idx
			# (stopSigChannels, world) = createChannel world
			| isError stopSigChannels =
				abortError "forkNestedSubProcess" stopSigChannels "createChannel returned an error"
			# (stopSigInChannel, stopSigOutChannel) = fromOk stopSigChannels
			# (subProc, world) = forkSubProcess
				(doNestedSubProcess
					inChannelsToClose [!UntypedOutChannel stopSigOutChannel: outChannelsToClose] path options
					stopSigInChannel)
				world
			| isError subProc = abortError "forkNestedSubProcess" subProc "forkSubProcess returned an error"
			# world = closeInChannelAbortOnError "forkNestedSubProcess" stopSigInChannel world
			# subProc = fromOk subProc
			// Increase buffersize of channel for receiving updates from the subprocess to improve performance.
			# (size, world) = setBufferSize (64*PAGESIZE) subProc.inChannel world
			# world = traceSetBufferSizeWarning size world
			=	( {processes & [idx] = {subprocess = subProc, stopSignalChannel = stopSigOutChannel}}
				, [!UntypedInChannel subProc.inChannel: inChannelsToClose]
				, [!UntypedOutChannel subProc.outChannel, UntypedOutChannel stopSigOutChannel: outChannelsToClose]
				, world)

	forkGetAndPassProc :: !ProcessPool !*World -> (!MaybeGetCondAndPassResultsSubProcess, !*World)
	forkGetAndPassProc processPool world
		# (getAndPassProc, world) = forkSubProcess (getCondAndPassResults processPool path options) world
		| isError getAndPassProc = abortError "forkGetAndPassProc" getAndPassProc "forkSubProcess returned an error"
		// Close out-channels of process pool, which prevents processes in the pool to wait forever in case
		// `getCondAndPassResults` crashes. (The channels are not kept open by the main process in this case.)
		# world = forEachProcessIn
			processPool
			(\{subprocess = {outChannel}} world = closeOutChannelAbortOnError "forkGetAndPassProc" outChannel world)
			world
		= (GetCondAndPassResultsSubProcess $ fromOk getAndPassProc, world)

openDatabaseWithoutCreatingSubProcs ::
	!FilePath !SyncMode !Bool !Bool !Int !Int !Int !MaybeGetCondAndPassResultsSubProcess !ProcessPool !*World
	-> (!*CDB, !*World)
openDatabaseWithoutCreatingSubProcs
	path syncMode readahead readaheadPartitionDatabases maxNumberPagesPartitionDatabases compactionFactor maxNumberPages
	getCondAndPassResultsProcess processPool world
	# (env, world) = openDatabasePtr path syncMode readahead maxNumberPages world
	=	(	{ path = path, env = env, compactionFactor = compactionFactor
			, getCondAndPassResultsProcess = getCondAndPassResultsProcess, processPool = processPool
			, partDbs = 'Map'.newMap, syncMode = syncMode, readaheadPartitionDatabases = readaheadPartitionDatabases
			, maxNumberPagesPartitionDatabases = maxNumberPagesPartitionDatabases}
		, world)

openDatabasePtr :: !FilePath !SyncMode !Bool !Int !*World -> (!Pointer, !*World)
openDatabasePtr path syncMode readahead maxNumberPages world
	# (mbError, world) = ensureDirectoryExists path world
	| isError mbError =
		abortError
			"openDatabaseWithoutCreatingSubProcs" mbError ("CleanDB: could not create database " +++ path)
	# (ret, world) = mdb_env_create tmpArr world
	| ret <> 0 = abortLmdbError "openDatabaseWithoutCreatingSubProcs" ret "mdb_env_create returned an error"
	# env          = tmpArr.[0]
	# (ret, world) = mdb_env_set_maxdbs env 512 world
	| ret <> 0 = abortLmdbError "openDatabaseWithoutCreatingSubProcs" ret "mdb_env_set_maxdbs returned an error"
	# (ret, world) = mdb_env_set_mapsize env (maxNumberPages * PAGESIZE) world
	| ret <> 0 = abortLmdbError "openDatabaseWithoutCreatingSubProcs" ret "mdb_env_set_mapsize returned an error"
	// Use a high number of max readers to prevent problems if parallel operations are enabled. We cannot derive this
	// from the settings as the same DB can be opened with and without parallel operations enabled. The high number of
	// maxreaders does not seem to have a significant impact on performance.
	# (ret, world) = mdb_env_set_maxreaders env 262144 world
	| ret <> 0 = abortLmdbError "openDatabaseWithoutCreatingSubProcs" ret "mdb_env_set_maxreaders returned an error"
	# (ret, world) = withLockFile
		(path </> LOCKFILE_NAME) (criticalSectionRetry 0 $ mdb_env_open env (packString path) flags 0664) world
	| isError ret = abortError "openDatabaseWithoutCreatingSubProcs" ret "withLockFile returned an error"
	# ret = fromOk ret
	| isError ret = abortError "openDatabaseWithoutCreatingSubProcs" ret "criticalSection returned an error"
	# ret = fromOk ret
	| ret <> 0 = abortLmdbError "openDatabaseWithoutCreatingSubProcs" ret "mdb_env_open returned an error"
	= (env, world)
where
	flags = syncFlag bitor MDB_NOMEMINIT bitor readaheadFlag
	syncFlag = case syncMode of
		FullSync   = 0
		NoMetaSync = MDB_NOMETASYNC
		NoSync     = MDB_NOSYNC

	readaheadFlag = if readahead 0 MDB_NORDAHEAD

PAGESIZE :== 4096 // OS pagesize.

:: TxnDurationTable =: TxnDurationTable (Map Int Int)

abortTxnsForWhich ::
	!FilePath !(Int Int *World -> (Bool, *World)) !TxnDurationTable !Int !FilePath !*World
	-> (!TxnDurationTable, *World)
abortTxnsForWhich
	pathToMdbStat pred (TxnDurationTable pidToTxnDurationMap) pollIntervalSecs pathToCdb w
	# (mbPidToSecsMap, w) = updateTransactionRuntimes pidToTxnDurationMap w
	| isError mbPidToSecsMap =
		abortError "abortTxnsWhichExceedMaxTime" mbPidToSecsMap "updateTransactionRuntimes returned an error"
	# updatedPidtoSecsMap = fromOk mbPidToSecsMap
	# (updatedPidtoSecsMap, w) = 'Map'.foldlWithKey
		(\(acc, w) pid time
			# (actuallyKill, w) = pred pid time w
			| actuallyKill
				# (ret, w) = kill pid SIGKILL w
				# (mbError, w) = if (ret <> 0) (getLastOSError w) (Ok (), w)
				= (trace_n
					(concat
						[ "abortTxnsWhichExceedMaxTime: killed ", toString pid, " after ", toString time
						, " seconds with exit code", toString ret,
						if (isError mbError) (" with error " +++ toString (fromError mbError))  ""])
					('Map'.del pid acc, w))
			| otherwise = (acc, w)) (updatedPidtoSecsMap, w) updatedPidtoSecsMap
	= (TxnDurationTable updatedPidtoSecsMap, w)
where
	updateTransactionRuntimes :: !(Map Int Int) !*World -> (!MaybeError String (Map Int Int), !*World)
	updateTransactionRuntimes pidToSecsMap w
		// Provide `-rr` to prevent `mdb_stat` from providing PIDs to processes not existing anymore.
		# (mbErr, w) = callProcessWithOutput pathToMdbStat ["-rr", pathToCdb] ?None w
		| isError mbErr = (liftError $ mapError snd mbErr, w)
		# {stdout, stderr, exitCode} = fromOk mbErr
		// mdb_stat can return exit code 1 if there are no active readers.
		| stderr <> "" || (exitCode <> 1 && exitCode <> 0) =
			(Error $ concat4 "Error occured when using mdb_stat, exit code: " (toString exitCode) " stderr: " stderr
			, w)
		# lines = drop 2 $ dropWhile (not o endsWith "stale readers cleared.") $ split "\n" stdout
		= (parseLines lines 'Map'.newMap pidToSecsMap, w)

	parseLines :: ![String] !(Map Int Int) !(Map Int Int) -> MaybeError String (Map Int Int)
	parseLines ["": _] pidToSecsMap _ = Ok pidToSecsMap
	parseLines [line:lines] pidToSecsMap origPidToSecsMap = case split " " $ trim line of
		[pid, _, txnId]
			# pid = toInt pid
			// Reader is stale so it is skipped.
			| txnId == "-" = parseLines lines pidToSecsMap origPidToSecsMap
			= parseLines
				lines
				('Map'.put pid (maybe 0 ((+) pollIntervalSecs) $ 'Map'.get pid origPidToSecsMap) pidToSecsMap)
				origPidToSecsMap
		_ = Error ("could not parse line" +++ line)
	parseLines  _ pidToSecsMap _ = Ok pidToSecsMap

initialTxnDurationTable :: TxnDurationTable
initialTxnDurationTable =: TxnDurationTable 'Map'.newMap

closeDatabase :: !*CDB !*World -> *World
closeDatabase db world
	# db = sendToGetAndPassProcess CloseDatabaseForGetAndPass db
	# db = forGetAndPassProcess
		(\proc db
			# (err, db) = waitForSubProcessToTerminate proc db
			| isError err = abortError "closeDatabase" err "waitForSubProcessToTerminate returned an error"
			# status = fromOk err
			# (exitCode, exitedWithCoreDump, signal) = (exitCodeIn status, hadCoreDump status, signalIn status)
			| exitCode <> ?Just 0 ||  exitedWithCoreDump || isJust signal = abortError
				"closeDatabase" (Error "error waiting for subprocess") "subprocess did not exit normally"
			= db)
		db
	# world = closeDatabasePtr db.env world
	= 'Map'.foldlWithKey` (\world _ (PartDb env) = closeDatabasePtr env world) world db.CDB.partDbs

closeDatabasePtr :: !Int !*World -> *World
closeDatabasePtr env world
	# (ret, world) = mdb_env_get_path env tmpArr world
	| ret <> 0 = abortLmdbError "closeDatabasePtr" ret "mdb_env_get_path returned an error"
	# path = derefString tmpArr.[0]
	# (err, world) =
		withLockFile (path </> LOCKFILE_NAME) (criticalSectionRetry 0 \world = ((), mdb_env_close env world)) world
	| isError err = abortError "closeDatabasePtr" err "withLockFile returned an error"
	# err = fromOk err
	| isError err = abortError "closeDatabasePtr" err "criticalSection returned an error"
	= world

//* Lockfile used to ensure that LMDB databases are not opened/closed at the same time (see LMDB caveats).
LOCKFILE_NAME :== "clean-db.lock"

withDatabase :: !FilePath !CleanDBOptions !.(*CDB -> *(*World -> *(.a, *CDB, *World))) !*World -> (.a, !*World)
withDatabase path opts dbFunc world
	# (res, _, world) = withDatabaseSt path opts dbFunc` () world
	= (res, world)
where
	dbFunc` st cdb world
		# (res, cdb, world) = dbFunc cdb world
		= (res, st, cdb, world)

withDatabaseSt ::
	!FilePath !CleanDBOptions !.(.st -> *(*CDB -> *(*World -> *(.a, .st, *CDB, *World)))) .st !*World
	-> (.a, .st, !*World)
withDatabaseSt path opts dbFunc st world
	# (cdb, world)          = openDatabase path opts world
	# (res, st, cdb, world) = dbFunc st cdb world
	# world                 = closeDatabase cdb world
	= (res, st, world)

:: *CDB =
	{ path :: !FilePath //* The database's path.
	, env :: !Pointer //* Pointer to the LMDB environment.
	, compactionFactor :: !Int     //* Higher values reduce storage size, but decrease write performance.
	, getCondAndPassResultsProcess :: !MaybeGetCondAndPassResultsSubProcess
		//* The subprocess for getting results for the condition and passing them to the processes running the nested
		//* transactions. Present only when parallel operations are enabled.
	, processPool :: !ProcessPool //* A process pool for executing parallel nested transactions operations.
	, partDbs :: !Map String PartDb //* Databases for storing partitioned maps.
	, syncMode :: !SyncMode //* The database's sync mode.
	, readaheadPartitionDatabases :: !Bool //* Indicates whether readhead is enabled for partition databases.
	, maxNumberPagesPartitionDatabases :: !Int //* The max number of pages for partition databases.
	}

//* A process pool for processes running nested transactions on an arbitrary state.
:: ProcessPool = ProcessPool !(A. parParam .locSt partRes: {#NestedSubProcess parParam locSt partRes})

//* The process for getting and passing result for parallel operations if enabled.
:: MaybeGetCondAndPassResultsSubProcess
	= ParallelGetDisabled //* Parallel get operations are disabled.
	| AlreadyInsideNestedTransaction //* We are already inside a nested parallel transaction.
	| GetCondAndPassResultsSubProcess !GetCondAndPassResultsSubProcess
		//* The process for getting and passing results for parallel operations.

emptyProcessPool :: ProcessPool
emptyProcessPool =: ProcessPool {}

defaultOptions :: CleanDBOptions
defaultOptions =:
	{ syncMode = FullSync, readahead = False, readaheadPartitionDatabases = False, compactionFactor = 3
	, maxNumberPages = 4000000000 /*16TB*/, maxNumberPagesPartitionDatabases = 4000000 /* 16GB */
	, parallelOperationsSetting = ParallelOperationsDisabled}

doReadOnlyTransaction :: !.(*ReadOnlyTxn -> (a, *ReadOnlyTxn)) !*CDB -> (!a, !*CDB)
doReadOnlyTransaction txnFunc db
	# (a, _, db) = doReadOnlyTransactionSt (\st txn = let (res, txn`) = txnFunc txn in (res, st, txn`)) () db
	= (a, db)

doReadOnlyTransactionSt :: !.(.st -> *(*ReadOnlyTxn -> (a, .st, *ReadOnlyTxn))) !.st !*CDB -> (!a, !.st, !*CDB)
doReadOnlyTransactionSt txnFunc acc db
	# (res, st, db) = doTransaction` True Unconstrained (\t -> ReadOnlyTxn t) (\(ReadOnlyTxn t) -> t) txnFunc acc db
	# db = sendToGetAndPassProcess EndOfTransactionForGetAndPass db
	= (res, st, db)

doTransaction :: !(*Txn -> (a, *Txn)) !*CDB -> (!a, !*CDB)
doTransaction txnFunc db
	# (res, _, db) = doTransactionSt (\st txn = let (res, txn`) = txnFunc txn in (res, st, txn`)) () db
	= (res, db)

doTransactionSt :: !(.st -> *(*Txn -> (a, .st, *Txn))) !.st !*CDB -> (!a, !.st, !*CDB)
doTransactionSt txnFunc acc db = doTransactionStWithOptions defaultTransactionOptions txnFunc acc db

doTransactionStWithOptions :: !TransactionOptions !(.st -> *(*Txn -> (a, .st, *Txn))) !.st !*CDB -> (!a, !.st, !*CDB)
doTransactionStWithOptions {TransactionOptions| partitionConstraint} txnFunc acc db=:{CDB| compactionFactor} =
	doTransaction` False partitionConstraint (\t -> Txn t) (\(Txn t) -> t) txnFunc` acc db
where
	txnFunc` :: E.^ a .st: !.st !*Txn -> (!a, !.st, !*Txn)
	txnFunc` st txn
		# (a, st, Txn info) = txnFunc st txn
		# info = moveFromHighToLowForAllMaps compactionFactor info
		= (a, st, Txn info)

defaultTransactionOptions :: TransactionOptions
defaultTransactionOptions =: {TransactionOptions| partitionConstraint = Unconstrained}

/**
 * For all maps move a number of items from the beginning of the high map to the low map.
 * The number of moved items is `compactionFactor` times the number of writes in the last transaction.
 * The moved items can be stored in a more compact way than at the original location,
 * as they are not stored in order instead of randomly.
 */
moveFromHighToLowForAllMaps :: !Int !*TxnInfo -> *TxnInfo
moveFromHighToLowForAllMaps compactionFactor info=:{dbis} = 'Map'.foldlWithKey` moveFromHighToLowFor info dbis
where
	moveFromHighToLowFor :: !*TxnInfo x !MapInfo -> *TxnInfo
	moveFromHighToLowFor info _ mapInfo=:{nrWrites, lowCursor, highCursor, isMultiMap, txnPtr}
		# nrMoves = compactionFactor * nrWrites
		| nrMoves == 0 = info
		# (ret, info) = moveFromHighToLow nrMoves lowCursor highCursor isMultiMap info
		| ret <> 0 = abortLmdbError "moveFromHighToLowFor" ret "moveFromHighToLow returned an error"
		# info = mdb_cursor_close lowCursor info
		= mdb_cursor_close highCursor info

/**
 * This performs a transaction. No check is done whether the transaction is read-only or not.
 * The type system prevents the `txnFunc` of a read-only transaction from containing write operations.
 */
doTransaction` ::
	!Bool !PartitionConstraint !(TxnInfo -> *txn) !(*txn -> TxnInfo) !.(.st -> *(*txn -> (a, .st, *txn))) !.st !*CDB
	-> (!a, !.st, !*CDB) | readTxn txn
doTransaction` isReadOnly partConstr toTxn fromTxn txnFunc acc
	db=:{CDB| env, getCondAndPassResultsProcess, processPool, compactionFactor}
	# txn = toTxn $ startTransaction` isReadOnly partConstr db
	// do transaction
	# (res, acc, txn) = txnFunc acc $ txn
	// commit transaction
	# db = endTransaction txn
	= (res, acc, db)

/**
 * @cons `Txn info` represents a transaction with `info`.
 */
:: *Txn =: Txn TxnInfo

/**
 * @cons `ReadOnlyTxn info` represents a read-only transaciton with `info`.
 */
:: *ReadOnlyTxn =: ReadOnlyTxn TxnInfo

/**
 * This is used as info during an ongoing transaction.
 */
:: *TxnInfo =
	{ ptr :: !Pointer //* pointer to the LMDB transaction
	, dbEnv :: !Int //* The database environment.
	, dbis :: !Map (MapName, ?String) MapInfo //* A map to find the LMDB DB indexes for all used maps.
	, isReadOnly :: !Bool //* Indicates whether the transaciton is read-only.
	, getCondAndPassResultsProcess :: !MaybeGetCondAndPassResultsSubProcess
		//* The subprocess for getting results for the condition and passing them to the processes running the nested
		//* transactions. Present only when parallel operations are enabled.
	, processPool                  :: !ProcessPool
		//* A process pool for executing parallel nested transactions operations.
	, compactionFactor             :: !Int
		//* Higher values reduce storage size, but decrease write performance.
		//* For each write `compactionFactor`-many items are compacted.
	, partDbs :: !Map String PartDb //* Databases for storing partitioned maps.
	, partDbTxns :: !Map String PartDbTxn //* Transactions on databases storing partitioned maps.
	, dbPath :: !FilePath //* The path of the database this transaction works on.
	, syncMode :: !SyncMode //* The sync mode of the database this transaction works on.
	, readaheadPartitionDatabases :: !Bool
		//* Is readhead for partiton databases enabled for the database this transaction works on.
	, maxNumberPagesPartitionDatabases :: !Int //* The max number of pages for partition databases.
	, partitionConstraint :: !PartitionConstraint
		//* This restricts how usage of partition databases is constrained for this transaction.
	}

instance SysCallEnv TxnInfo where
	accWorld f txnInfo
		# res = accUnsafe $ f
		= (res, txnInfo)

:: PartDb = PartDb !Pointer // Contains a db pointer.
:: PartDbTxn = PartDbTxn !Pointer // Contains a txn pointer.

instance readTxn Txn
where
	infoOf :: !*Txn -> *TxnInfo
	infoOf (Txn info) = info

	txnWith :: !*TxnInfo -> *Txn
	txnWith info = Txn info

instance readTxn ReadOnlyTxn
where
	infoOf :: !*ReadOnlyTxn -> *TxnInfo
	infoOf (ReadOnlyTxn info) = info

	txnWith :: !*TxnInfo -> *ReadOnlyTxn
	txnWith info = ReadOnlyTxn info

dbMap :: !MapName !(v -> String) !(String -> v) -> DBMap k v
dbMap mapName encode decode = DBMap {name = mapName, encode = encode, decode = decode, partName = ?None}

:: DBMap k v =: DBMap (DBMapCommon k v)

instance dbMap DBMap where
	commonOf (DBMap common) = common
	valueSizeOfMultiMap _ = ?None

dbMultiMap :: !MapName !(v -> String) !(String -> v) -> DBMultiMap k v
dbMultiMap mapName encode decode
	= DBMultiMap {common = {name = mapName, encode = encode, decode = decode, partName = ?None}, valueSize = Dynamic}

dbMultiMapFixedValueSize :: !MapName !(v -> String) !(String -> v) -> DBMultiMap k v
dbMultiMapFixedValueSize mapName encode decode
	= DBMultiMap {common = {name = mapName, encode = encode, decode = decode, partName = ?None}, valueSize = Fixed}

dbMultiMapIntValues :: !MapName !(v -> Int) !(Int -> v) -> DBMultiMap k v
dbMultiMapIntValues mapName encode decode = DBMultiMap
	{ common = {name = mapName, encode = intToBinString o encode, decode = decode o binStringToInt, partName = ?None}
	, valueSize = Integer }
where
	/*
	 * The rightmost bit determines the order, which is why the Int is encoded in reverse byte order.
	 * The sign bit is swapped because for the ordering 0 < 1 and
	 * negative integers should be smaller than positive integers.
	 * (Negative integers have sign bit 1)
	 */
	intToBinString :: Int -> String
	intToBinString i =
		{ fromInt $ i, fromInt $ i >> 8 , fromInt $ i >> 16, fromInt $ i >> 24
		, fromInt $ i >> 32, fromInt $ i >> 40, fromInt $ i >> 48, fromInt $ 128 bitxor (i >> 56)
		}

	binStringToInt :: String -> Int
	binStringToInt str =
		( (toInt str.[0])  + toInt str.[1] << 8 + toInt str.[2] << 16 + toInt str.[3] << 24 +
		  toInt str.[4] << 32 + toInt str.[5] << 40 + toInt str.[6] << 48 + (128 bitxor toInt str.[7]) << 56
		)

:: DBMultiMap k v =: DBMultiMap (DBMultiMapContext k v)

:: DBMultiMapContext k v = {common :: !DBMapCommon k v, valueSize :: !MultiMapValueSize}

:: MultiMapValueSize = Dynamic | Fixed | Integer

instance == MultiMapValueSize where
	(==) Dynamic Dynamic = True
	(==) Fixed Fixed = True
	(==) Integer Integer = True
	(==) _ _ =  False

instance dbMap DBMultiMap where
	commonOf (DBMultiMap context) = context.common
	valueSizeOfMultiMap (DBMultiMap context) = ?Just context.valueSize

:: PartDbMap p k v =: PartDbMap (DBMap k v)

:: PartDbMultiMap p k v =: PartDbMultiMap (DBMultiMap k v)

instance isPartitionedMap PartDbMap DBMap where
	withSelectedPartition :: !p !(PartDbMap p k v) -> DBMap k v | toString p
	withSelectedPartition partName (PartDbMap (DBMap common)) = DBMap {common & partName = ?Just $ toString partName}

	partitionedMap :: !(DBMap k v) -> PartDbMap p k v
	partitionedMap map = PartDbMap map

instance isPartitionedMap PartDbMultiMap DBMultiMap where
	withSelectedPartition :: !p !(PartDbMultiMap p k v) -> DBMultiMap k v | toString p
	withSelectedPartition partName (PartDbMultiMap (DBMultiMap context)) =
		DBMultiMap {context & common.partName = ?Just $ toString partName}

	partitionedMap :: !(DBMultiMap k v) -> PartDbMultiMap p k v
	partitionedMap map = PartDbMultiMap map

derive gEq MapStats

getMapStats :: !*txn -> (!Map MapName MapStats, !*txn) | readTxn txn
getMapStats txn
	# info = infoOf txn
	# (ptr, info) = info!ptr
	# (stats, info) = getDbiStats ptr info
	| isError stats = abortLmdbError "getMapStats" (fromError stats) "getDbiStats returned an error"
	= ('Map'.foldlWithKey` withStat 'Map'.newMap $ fromOk stats, txnWith info)
where
	withStat :: !(Map MapName MapStats) !String !(!Int, !Int, !Int, !Int, !Int, !Int) -> Map MapName MapStats
	withStat acc dbiName (ms_psize, _, ms_branch_pages, ms_leaf_pages, ms_overflow_pages, ms_entries) = 'Map'.alter
		(?Just o withStatForDbi o fromMaybe {size = 0, nrOfEntries = 0}) (dbiName % (0, size dbiName - 3)) acc
	where
		withStatForDbi :: !MapStats -> MapStats
		withStatForDbi {size, nrOfEntries} =
			{ size = size + ms_psize * (ms_branch_pages + ms_leaf_pages + ms_overflow_pages)
			// The large value maps do not include actual entries, but parts of large values.
			, nrOfEntries = if (dbiNamePostfix == 'L') nrOfEntries (nrOfEntries + ms_entries)}

		dbiNamePostfix = dbiName.[size dbiName - 1]

put :: !k !v !(dbMap k v) !*Txn -> *Txn | key k & dbMap dbMap
put k v dbMap txn = snd $ putKeyValueExists` k v dbMap txn

// This is only allowed on multi maps as regular maps do not return whether the key data pair already existed.
putKeyValueExists :: !k !v !(DBMultiMap k v) !*Txn -> (Bool, *Txn) | key k
putKeyValueExists k v dbMultiMap txn = putKeyValueExists` k v dbMultiMap txn

// This allows to define `put` and `putKeyValueExists` through the same function but make sure the user is unable to use
// the `keyExists` result for regular maps as the result would be wrong.
putKeyValueExists` :: !k !v !(dbMap k v) !*Txn -> (Bool, *Txn) | key k & dbMap dbMap
putKeyValueExists` k v map (Txn txnInfo=:{dbis})
	# (?Just mapInfo=:{isMultiMap, lowMap, highMap, lowestKeyInHighMap, nrWrites, lowCursor, highCursor}, txnInfo) =
		mapInfoOf map txnInfo
	# useLowMap = maybe True (\lowest = keyStr < lowest) lowestKeyInHighMap
	# dbi = if useLowMap lowMap highMap
	# cur = if useLowMap lowCursor highCursor
	| isMultiMap
		// `MDB_NODUPDATA` prevents duplicate equal values for the same key.
		# (ret, txnInfo) = mdbCursorPut cur keyStr valStr MDB_NODUPDATA txnInfo
		| ret <> 0 && ret <> MDB_KEYEXIST = abortLmdbErrorOnMap "put" map ret "mdbPut returned an error"
		# txnInfo =
			// We only want to compact if data is changed. The check however only works for multi maps.
			// There seems to be no efficient method
			// to determine whether a key/value combination already exists for a non-multi map.
			if (ret == MDB_KEYEXIST)
				txnInfo {txnInfo & dbis = 'Map'.put (name, partName) {mapInfo & nrWrites = inc nrWrites} dbis}
		= (ret == MDB_KEYEXIST, Txn txnInfo)
	| otherwise = putNonMulti dbi cur mapInfo txnInfo
where
	// This function stores a value of a map in chunks of values of size `MAX_CHUNK_SIZE`
	// if the size of the original value exceeds `MAX_CHUNK_SIZE`.
	putNonMulti :: !Int !Pointer !MapInfo !*TxnInfo -> (!Bool, !*Txn)
	putNonMulti dbi cur mapInfo=:{largeValueMap, nrWrites, txnPtr} txnInfo
		| valStrSize < MAX_CHUNK_SIZE
			# (ret, txnInfo) = mdbCursorPut cur keyStr valStr 0 txnInfo
			| ret <> 0 = abortLmdbErrorOnMap "putNonMulti" map ret "mdbPut returned an error"
			= (ret == MDB_KEYEXIST, Txn txnInfo)
		| otherwise
			# (largeValDbi, mapInfo, txnInfo) =
				case largeValueMap of
					?None
						# (ret, txnInfo) = mdb_dbi_open txnPtr (packString $ name +++ "$L") MDB_CREATE tmpArr txnInfo
						| ret <> 0 = abortLmdbErrorOnMap "mapInfoOf" map ret "mdb_dbi_open returned an error ($L)"
						| otherwise
							# dbiLargeValueMap = tmpArr.[0]
							# txnInfo = forceEval dbiLargeValueMap txnInfo
							# mapInfo = {mapInfo & largeValueMap = ?Just dbiLargeValueMap}
							= (dbiLargeValueMap, mapInfo, updateLargeValueMap name partName dbiLargeValueMap txnInfo)
					?Just largeValDbi = (largeValDbi, mapInfo, txnInfo)
			// Store the first chunk in the regular map.
			# (ret, txnInfo) = mdbPutOffsetSize txnPtr dbi keyStr valStr 0 MAX_CHUNK_SIZE 0 txnInfo
			| ret <> 0 = abortLmdbErrorOnMap "putNonMulti" map ret "mdbPutOffsetSize returned an error"
			// Store the rest of the data in the large value map.
			# txnInfo = if (valStrSize == MAX_CHUNK_SIZE)
				(deleteRest largeValDbi 0 txnInfo)
				(storeValLargeValueMap largeValDbi valStr (valStrSize - MAX_CHUNK_SIZE) MAX_CHUNK_SIZE 0 txnInfo)
			= 	(ret == MDB_KEYEXIST
				, Txn {txnInfo & dbis = 'Map'.put (name, partName) {mapInfo & nrWrites = inc nrWrites} dbis})
		where
			storeValLargeValueMap :: !Int !String !Int !Int !Int !*TxnInfo -> *TxnInfo
			storeValLargeValueMap dbi val valSize startIndex largeValIndex txnInfo
				# (ret, txnInfo) = mdbPutOffsetSize
					txnPtr dbi (keyStrLargeValueMapFor keyStr largeValIndex) val startIndex (min valSize MAX_CHUNK_SIZE)
					0 txnInfo
				| ret <> 0 = abortLmdbErrorOnMap "storeValLargeValueMap" map ret "mdbPutOffsetSize returned an error"
				| valSize <= MAX_CHUNK_SIZE = deleteRest dbi (inc largeValIndex) txnInfo
				= storeValLargeValueMap
					dbi val (valSize - MAX_CHUNK_SIZE) (startIndex + MAX_CHUNK_SIZE) (inc largeValIndex) txnInfo

			deleteRest :: !Int !Int !*TxnInfo -> *TxnInfo
			deleteRest dbi largeValIndex txnInfo
				# keyStr = keyStrLargeValueMapFor keyStr largeValIndex
				# (ret, _, txnInfo) = mdbGet txnPtr dbi keyStr txnInfo
				| ret == MDB_NOTFOUND = txnInfo
				| ret <> 0 = abortLmdbErrorOnMap "deleteRest" map ret "mdbGet returned an error"
				# (ret, txnInfo) = mdbDelNoData txnPtr dbi keyStr txnInfo
				| ret <> 0 = abortLmdbErrorOnMap "deleteRest" map ret "mdbDelNoData returned an error"
				= deleteRest dbi (inc largeValIndex) txnInfo

	{encode, name, partName} = commonOf map
	keyStr = toString $ strRepresentation k
	valStr = encode v
	valStrSize = size valStr

keyExists :: !k !(dbMap k v) !*txn -> (!Bool, !*txn) | key k & dbMap dbMap & readTxn txn
keyExists key dbMap txn
	# txnInfo = infoOf txn
	# (?Just mapInfo=:{lowMap, highMap, lowestKeyInHighMap, txnPtr}, txnInfo) =
		mapInfoOf dbMap txnInfo
	# keyStr = toString $ strRepresentation key
	# dbi = if (maybe True (\lowest = keyStr < lowest) lowestKeyInHighMap) lowMap highMap
	# (ret, txnInfo) = mdbGetWithoutValueRead txnPtr dbi keyStr txnInfo
	| ret <> 0 && ret <> MDB_NOTFOUND = abortLmdbError "keyExists" ret "mdbGetWithoutValueRead returned an error"
	= (ret <> MDB_NOTFOUND, txnWith txnInfo)

keyValueExists :: !k !v !(DBMultiMap k v) !*txn -> (!Bool, !*txn) | key k & readTxn txn
keyValueExists key val dbMap txn
	# keyStr = toString $ strRepresentation key
	# txnInfo = infoOf txn
	# (?Just mapInfo=:{lowMap, highMap, lowestKeyInHighMap, txnPtr}, txnInfo) =
		mapInfoOf dbMap txnInfo
	# dbi = if (maybe True (\lowest = keyStr < lowest) lowestKeyInHighMap) lowMap highMap
	# (cursor, txnInfo) = openCursor dbi txnPtr txnInfo
	# valEncode = (commonOf dbMap).encode
	# (ret, txnInfo) = mdbCursorGetNoRead cursor keyStr (valEncode val) MDB_GET_BOTH txnInfo
	# txnInfo = mdb_cursor_close cursor txnInfo
	| ret <> 0 && ret <> MDB_NOTFOUND = abortLmdbErrorOnMap
		"keyValueExists" dbMap ret "mdbCursorGetNoRead returned an error"
	= (ret <> MDB_NOTFOUND, txnWith txnInfo)

delete :: !k !(dbMap k v) !*Txn -> *Txn | key k & dbMap dbMap
delete key map txn=:(Txn info)
	# keyStr = toString $ strRepresentation key
	# (?Just mapInfo=:{highMap, lowMap, lowestKeyInHighMap, largeValueMap, txnPtr}, info) = mapInfoOf map info
	# mapToDeleteFrom = if (maybe True (\lowestInHigh = keyStr < lowestInHigh) lowestKeyInHighMap) lowMap highMap
	# (ret, info) = mdbDelNoData txnPtr mapToDeleteFrom keyStr info
	| ret <> 0 && ret <> MDB_NOTFOUND = abortLmdbErrorOnMap "delete" map ret "mdbDelNoData returned an error"
	# (ret, info) = case largeValueMap of
		?Just largeValueMap = deleteLargeValueMapValues keyStr largeValueMap txnPtr info
		?None
			# (mbLargeValueMap, info) = openLargeValueMapRead txnPtr name partName info
			= case mbLargeValueMap of
				?None = (0, info)
				?Just largeValueMap = deleteLargeValueMapValues keyStr largeValueMap txnPtr info
	| ret <> 0 = abortLmdbErrorOnMap "delete" map ret "deleteLargeValueMapValues returned an error"
	# lowestKeyInHighMapNeedsUpdate = maybe False (\lowestInHigh = keyStr == lowestInHigh) lowestKeyInHighMap
	# info = if lowestKeyInHighMapNeedsUpdate (updateLowestKeyInHighMapOf map info) info
	= Txn info
where
	{name, partName} = commonOf map

getSingle :: !k !(DBMap k v) !*txn -> (!?v, !*txn) | readTxn txn & key k
getSingle k map txn
	# info            = infoOf txn
	# (mapInfo, info) = mapInfoOf map info
	= case mapInfo of
		?Just {highMap, lowMap, lowestKeyInHighMap, largeValueMap, txnPtr}
			# mapToGetFrom = if (maybe True (\lowestInHigh = keyStr < lowestInHigh) lowestKeyInHighMap) lowMap highMap
			# (val, info) = getSingle` mapToGetFrom largeValueMap decode txnPtr info
			= (val, txnWith info)
		?None
			= (?None, txnWith info)
where
	keyStr = toString $ strRepresentation k
	{decode, name, partName} = commonOf map

	getSingle` :: !Int !(?Int) !(String -> v) !Pointer !*TxnInfo -> (!?v, !*TxnInfo)
	getSingle` dbi mbLargeValueMapDbi decode txnPtr info
		# (ret, valStr, info) = mdbGet txnPtr dbi keyStr info
		| ret == MDB_NOTFOUND = (?None, info)
		| ret <> 0 = abortLmdbErrorOnMap "getSingle`" map ret "mdbGet returned an error"
		| size valStr < MAX_CHUNK_SIZE = (?Just $ decode valStr, info)
		| otherwise =
			case mbLargeValueMapDbi of
				?Just largeValueMapDbi = includeLargeValueMapVals largeValueMapDbi valStr info
				?None
					# (mbLargeValueMap, info) = openLargeValueMapRead txnPtr name partName info
					= case mbLargeValueMap of
						?None = (?Just $ decode valStr, info)
						?Just largeValueMap = includeLargeValueMapVals largeValueMap valStr info

	where
		includeLargeValueMapVals :: E.^ v: !Int !String !*TxnInfo -> (!?v, !*TxnInfo)
		includeLargeValueMapVals largeValueMapDbi valStr info
			# (valStrs, info) = readValueFromLargeValueMap largeValueMapDbi keyStr txnPtr info
			= (?Just $ decode $ concat [valStr:valStrs], info)

get :: !(Condition k) !(DBMap k v) !*txn -> (!l (k, v), !*txn) | readTxn txn & key k & List l (k, v)
get cond map txn = getAccumNestedDelete` cond withValue [|] map txn
where
	withValue :: !k !v !(l (k, v)) !*txn -> (!Bool, !Bool, !l (k, v), !*txn) | readTxn txn & List l (k, v)
	withValue k v acc txn = (False, False, [|(k, v): acc], txn)

getAccum :: !(Condition k) !(k v .st -> (Bool, .st)) !.st !(DBMap k v) !*txn -> (!.st, !*txn) | readTxn txn & key k
getAccum cond updFunc st map txn = getAccumNestedDelete` cond withValue st map txn
where
	withValue :: E.^ k v .st: !k !v !.st !*txn -> (!Bool, !Bool, !.st, !*txn)
	withValue k v st txn
		# (stop, st) = updFunc k v st
		= (stop, False, st, txn)

getAccumDelete :: !(Condition k) !(k v .st -> (Bool, Bool, .st)) !.st !(DBMap k v) !*Txn -> (!.st, !*Txn) | key k
getAccumDelete cond updFunc st map txn = getAccumNestedDelete` cond withValue st map txn
where
	withValue :: E.^ k v .st: !k !v !.st !*Txn -> (!Bool, !Bool, !.st, !*Txn)
	withValue k v st txn
		# (stop, deleteCurrent, st) = updFunc k v st
		= (stop, deleteCurrent, st, txn)

getAccumNested ::
	!(Condition k) !(k v .st -> *(*txn -> (Bool, .st, *txn))) !.st !(DBMap k v) !*txn -> (!.st, !*txn)
	| readTxn txn & key k
getAccumNested cond updFunc st map txn = getAccumNestedDelete` cond withValue st map txn
where
	withValue :: E.^ k v .st *txn: !k !v !.st !*txn -> (!Bool, !Bool, !.st, !*txn)
	withValue k v st txn
		# (stop, st, txn) = updFunc k v st txn
		= (stop, False, st, txn)

getAccumNestedDelete ::
	!(Condition k) !(k v .st -> *(*Txn -> (Bool, Bool, .st, *Txn))) !.st !(DBMap k v) !*Txn -> (!.st, !*Txn) | key k
getAccumNestedDelete cond updFunc st map txn = getAccumNestedDelete` cond updFunc st map txn

// This is the most general variant from which all other variants are derived. If used with a `ReadOnlyTxn` the
// accumulator function must never delete items. The result is a runtime error otherwise. This is why the variant is
// not exported and all variants allowing to delete items are restricted to `Txn`.
getAccumNestedDelete` ::
	!(Condition k) !(k v .st -> *(*txn -> (Bool, Bool, .st, *txn))) !.st !(DBMap k v) !*txn -> (!.st, !*txn)
	| readTxn txn & key k
getAccumNestedDelete` {Condition| continuations, isLessThan} updFunc st map=:(DBMap common) txn
	# info = infoOf txn
	# (st, info) = getCond withValue continuations isLessThan (?Just common.decode) map st info
	= (st, txnWith info)
where
	withValue :: E.^ k v .st: x !k idc !(?v) y !*z !*(RunState k v .st) -> (!Bool, !Bool, *z, !*RunState k v .st)
	withValue _ key _ (?Just val) _ nothing st=:{state, txnInfo}
		# (stop, deleteCurrent, state, txn) = updFunc key val state $ txnWith txnInfo
		= (stop, deleteCurrent, nothing, {st & state = state, txnInfo = infoOf txn})

getAccumNestedParallel ::
	!(Condition k) !(k v .st -> *(Bool, ?parParam, .st)) !(parParam *ReadOnlyTxn -> *(partRes, *ReadOnlyTxn))
	!(partRes .resAcc -> *(*ReadOnlyTxn -> *(Bool, .resAcc, *ReadOnlyTxn))) !.st !.resAcc !(DBMap k v) !(?Int)
	!*ReadOnlyTxn
	-> (!.resAcc, !*ReadOnlyTxn) | key k
getAccumNestedParallel cond paramFor nestedTxn updFor st resAcc map nrSubProcs txn =
	getAccumNestedParallelWithLocalState cond paramFor nestedTxn` updFor st (\_ = ()) resAcc map nrSubProcs txn
where
	nestedTxn` :: E.^ parParam partRes: !parParam !() !*ReadOnlyTxn -> (!partRes, !(), !*ReadOnlyTxn)
	nestedTxn` par st txn
		# (res, txn) = nestedTxn par txn
		= (res, st, txn)

getAccumNestedParallelWithLocalState ::
	!(Condition k) !(k v .st -> *(Bool, ?parParam, .st))
	!(parParam .locSt -> *(*ReadOnlyTxn -> *(partRes, .locSt, *ReadOnlyTxn)))
	!(partRes .resAcc -> *(*ReadOnlyTxn -> *(Bool, .resAcc, *ReadOnlyTxn))) !.st !(() -> .locSt) !.resAcc !(DBMap k v)
	!(?Int) !*ReadOnlyTxn
	-> (!.resAcc, !*ReadOnlyTxn) | key k
getAccumNestedParallelWithLocalState
	condition paramFor nestedTxn updFor st locSt resAcc map nrSubProcs txn
	= getAccumNestedParallelWithLocalStateAndContinuationChain
		(GetContinuation \contSt txn =
			(	{condition = condition, map = map, paramFor = paramFor, nextContinuation =  NopContinuation}
				,contSt, txn))
		nestedTxn updFor () st locSt resAcc nrSubProcs txn

getAccumNestedParallelWithLocalStateAndContinuationChain ::
	!(GetContinuation parParam .keyValSt .continuationSt)
	!(parParam .locSt -> *(*ReadOnlyTxn -> *(partRes, .locSt, *ReadOnlyTxn)))
	!(partRes .resAcc -> *(*ReadOnlyTxn -> *(Bool, .resAcc, *ReadOnlyTxn))) !.continuationSt !.keyValSt !(() -> .locSt)
	!.resAcc !(?Int) !*ReadOnlyTxn -> (!.resAcc, !*ReadOnlyTxn)
getAccumNestedParallelWithLocalStateAndContinuationChain
	continuationChain nestedTxn updFor continuationSt keyValSt toLocSt resAcc nrSubProcs txn
	# info = infoOf txn
	# (nrSubProcs, info) = case nrSubProcs of
		?Just nr = (nr, info)
		?None    = getNumberOfAvailableProcessors info
	# (resAcc, info) = getCondPar
		nrSubProcs continuationChain nestedTxn updFor keyValSt continuationSt toLocSt resAcc info
	= (resAcc, txnWith info)

combineContinuations ::
	!(GetContinuation parParam .keyValSt .continuationSt) !(GetContinuation parParam .keyValSt .continuationSt)
	-> (GetContinuation parParam .keyValSt .continuationSt)
combineContinuations NopContinuation c = c
combineContinuations c NopContinuation = c
combineContinuations (GetContinuation continuationFunction0) continuation1 = GetContinuation combined
where
	combined :: E.^ .continuationSt .keyValSt parParam k v dbMap:
		!.continuationSt !*ReadOnlyTxn ->
		*(GetContinuationResult dbMap k v .keyValSt .continuationSt parParam, .continuationSt, *ReadOnlyTxn)
	combined contSt txn
		# (continuationResult=:{nextContinuation}, continuationSt, txn) = continuationFunction0 contSt txn
		= 	({continuationResult & nextContinuation = combineContinuations nextContinuation continuation1}
			, continuationSt, txn)

alter :: !k !((?v) -> ?v) !(DBMap k v) !*Txn -> *Txn | key k
alter key updFunc map txn
	# (mbVal, txn) = getSingle key map txn
	= case updFunc mbVal of
		?Just val` = put key val` map txn
		?None      = delete key map txn

alterMulti :: !k !([v] -> [v]) !(DBMultiMap k v) !*Txn -> *Txn | key k
alterMulti key updFunc map txn
	# (vals, txn) = getSingleMulti key map txn
	# updatedVals = updFunc vals
	# txn = delete key map txn
	= foldl (\txn updatedVal = put key updatedVal map txn) txn updatedVals

getSingleMulti :: !k !(DBMultiMap k v) !*txn -> (![v], !*txn) | readTxn txn & key k
getSingleMulti key map=:(DBMultiMap {common}) txn
	# info            = infoOf txn
	# (mapInfo, info) = mapInfoOf map info
	= case mapInfo of
		?Just {highMap, lowMap, lowestKeyInHighMap, txnPtr}
			# keyStr = toString $ strRepresentation key
			# mapToGetFrom = if (maybe True (\lowestInHigh = keyStr < lowestInHigh) lowestKeyInHighMap) lowMap highMap
			# (vals, info) = getSingleMulti` common keyStr mapToGetFrom txnPtr info
			= (vals, txnWith info)
		?None
			// map does not exist = it's empty
			= ([], txnWith info)
where
	getSingleMulti` :: !(DBMapCommon k v) !String !Int !Int !*TxnInfo -> (![v], !*TxnInfo)
	getSingleMulti` common keyStr dbi txnPtr info
		# (cur, info) = openCursor dbi txnPtr info
		// get values
		# (ret, valStr, info) = mdbCursorGetReadVal cur keyStr MDB_SET info
		| ret == MDB_NOTFOUND
			# info = mdb_cursor_close cur info
			= ([], info)
		| ret <> 0 = abortLmdbErrorOnMap "getSingleMulti`" map ret "mdbCursorGetReadVal returned an error"
		# (vals, info) = getRest False common [common.decode valStr] cur info
		// close cursor
		# info           = mdb_cursor_close cur info
		= (vals, info)

getMulti :: !(Condition k) !(DBMultiMap k v) !*txn -> (!l (k, [v]), !*txn) | readTxn txn & key, == k & List l (k, [v])
getMulti cond map txn
	# (l, txn) = getMultiAccum cond withValue` [|] map txn
	| IsEmpty l = (l, txn)
	# [|kv:kvs] = l
	// We need to reverse the first key-value-set here to order the values ascending such that the ordering
	// of values is consistent, The other key-value-sets have already been reversed by `withValue`.
	= ([|appSnd reverse kv:kvs], txn)
where
	withValue` :: !k !v !(l (k, [v])) -> (!Bool, !l (k, [v])) | List l (k, [v]) & == k
	withValue` k v [|] = (False, [|(k, [v])])
	withValue` k v [|(k`, vs`):r]
		| k == k` = (False, [|(k, [v:vs`]):r])
		| otherwise = (False, [|(k, [v]), (k`, reverse vs`):r])

getMultiAccumKeysOnly ::
	!(Condition k) !(k .st -> (Bool, .st)) !.st !(DBMultiMap k v) !*txn -> (!.st, !*txn)
	| readTxn txn & key k
getMultiAccumKeysOnly cond updFunc st dbMap txn = getMultiAccumKeysOnlyNested cond updFuncPrime st dbMap txn
where
	updFuncPrime :: E.^ k .st: !k !.st !*txn -> (!Bool, !.st, !*txn)
	updFuncPrime k st txn
		# (stop, st) = updFunc k st
		= (stop, st, txn)

getMultiAccumKeysOnlyNested ::
	!(Condition k) !(k .st -> *(*txn -> (Bool, .st, *txn))) !.st !(DBMultiMap k v) !*txn -> (!.st, !*txn)
	| readTxn txn & key k
getMultiAccumKeysOnlyNested {Condition| continuations, isLessThan} updFunc st map=:(DBMultiMap {common}) txn
	# info       = infoOf txn
	# (st, info) = getCond getRest` continuations isLessThan ?None map st info
	= (st, txnWith info)
where
	getRest` ::
		E.^ k .st: Bool !k idc (?v) !Pointer !*x !*(RunState k v .st) -> (!Bool, !Bool, !*x, !*RunState k v .st)
	getRest` _ key _ _ _ nothing runSt=:{state, txnInfo}
		# (stop, state, txn) = updFunc key state $ txnWith txnInfo
		= (stop, False, nothing, {runSt & state = state, txnInfo = infoOf txn})

getMultiAccum ::
	!(Condition k) !(k v .st -> (Bool, .st)) !.st !(DBMultiMap k v) !*txn -> (!.st, !*txn) | readTxn txn & key k
getMultiAccum cond updFunc st map txn = getMultiAccumNested cond withVal st map txn
where
	withVal :: E.^ k v .st: !k !v !.st !*txn -> (!Bool, !.st, !*txn)
	withVal key vals acc txn
		# (stop, acc) = updFunc key vals acc
		= (stop, acc, txn)

getMultiAccumDelete ::
	!(Condition k) !(k v .st -> (Bool, Bool, .st)) !.st !(DBMultiMap k v) !*Txn -> (!.st, !*Txn)
	| key k
getMultiAccumDelete condition updFunc st map txn = getMultiAccumNestedDelete condition updFunc` st map txn
where
	updFunc` :: E.^ k v .st: !k !v !.st !*Txn -> (!Bool, !Bool, !.st, !*Txn)
	updFunc` k v st txn
		# (stop, del, st) = updFunc k v st
		= (stop, del, st, txn)

getMultiAccumNested ::
	!(Condition k) !(k v .st -> *(*txn -> (Bool, .st, *txn))) !.st !(DBMultiMap k v) !*txn -> (!.st, !*txn)
	| readTxn txn & key k
getMultiAccumNested condition updFunc st map txn
	= getMultiAccumNestedDelete` condition updFunc` st map txn
where
	updFunc` :: E.^ k v .st *txn: !k !v !.st !*txn -> (!Bool, !Bool, !.st, !*txn)
	updFunc` k v st txn
		# (stop, st, txn) = updFunc k v st txn
		= (stop, False, st, txn)

// This is the most general variant from which all other variants are derived. If used with a `ReadOnlyTxn` the
// accumulator function must never delete items. The result is a runtime error otherwise. This is why the variant is
// not exported and all variants allowing to delete items are restricted to `Txn`.
getMultiAccumNestedDelete ::
	!(Condition k) !(k v .st -> *(*Txn -> (Bool, Bool, .st, *Txn))) !.st !(DBMultiMap k v) !*Txn -> (!.st, !*Txn)
	| key k
getMultiAccumNestedDelete condition withValue st map txn = getMultiAccumNestedDelete` condition withValue st map txn

getMultiAccumNestedDelete` ::
	!(Condition k) !(k v .st -> *(*txn -> (Bool, Bool, .st, *txn))) !.st !(DBMultiMap k v) !*txn -> (!.st, !*txn)
	| key k & readTxn txn
getMultiAccumNestedDelete` {Condition| continuations, isLessThan} updFunc st map=:(DBMultiMap {common}) txn
	# info       = infoOf txn
	# (st, info) = getCond (getRest` $ commonOf map) continuations isLessThan (?Just common.decode) map st info
	= (st, txnWith info)
where
	getRest` ::
		E.^ k v .st: !(DBMapCommon k v) !Bool !k !(KeyStr k) !(?v) !Pointer !*x !*(RunState k v .st)
		-> (!Bool, !Bool, *x, !*RunState k v .st) | key k
	getRest` common lastMoveWasBackwards key keyStr (?Just val) cur nothing st=:{state, txnInfo}
		# (stop, state, txnInfo) = multiAccumRestNestedDelete
			(toString keyStr) val lastMoveWasBackwards common updFunc` state cur txnInfo
		= (stop, False, nothing, {st & state = state, txnInfo = txnInfo})
	where
		updFunc` :: E.^ v .st: !v !.st !*TxnInfo -> (!Bool, !Bool, !.st, !*TxnInfo)
		updFunc` val st info
			# (stop, del, st, txn) = updFunc key val st $ txnWith info
			= (stop, del, st, infoOf txn)

getMultiAccumNestedParallel ::
	!(Condition k) !(k v .st -> *(Bool, ?parParam, .st)) !(parParam *ReadOnlyTxn -> *(partRes, *ReadOnlyTxn))
	!(partRes .resAcc -> *(*ReadOnlyTxn -> *(Bool, .resAcc, *ReadOnlyTxn))) !.st !.resAcc !(DBMultiMap k v) !(?Int)
	!*ReadOnlyTxn
	-> *(!.resAcc, !*ReadOnlyTxn) | key k
getMultiAccumNestedParallel cond paramFor nestedTxn updFor st resAcc map nrSubProcs txn =
	getMultiAccumNestedParallelWithLocalState
		cond paramFor nestedTxn` updFor st (\_ = ()) resAcc map nrSubProcs txn
where
	nestedTxn` :: E.^ parParam partRes: !parParam !() !*ReadOnlyTxn -> (!partRes, !(), !*ReadOnlyTxn)
	nestedTxn` par st txn
		# (res, txn) = nestedTxn par txn
		= (res, st, txn)

getMultiAccumNestedParallelWithLocalState ::
	!(Condition k) !(k v .st -> *(Bool, ?parParam, .st))
	!(parParam .locSt -> *(*ReadOnlyTxn -> *(partRes, .locSt, *ReadOnlyTxn)))
	!(partRes .resAcc -> *(*ReadOnlyTxn -> *(Bool, .resAcc, *ReadOnlyTxn))) !.st !(() -> .locSt) !.resAcc
	!(DBMultiMap k v) !(?Int) !*ReadOnlyTxn
	-> *(!.resAcc, !*ReadOnlyTxn) | key k
getMultiAccumNestedParallelWithLocalState
	condition paramFor nestedTxn updFor st locSt resAcc map nrSubProcs txn
	= getAccumNestedParallelWithLocalStateAndContinuationChain
		(GetContinuation \contSt txn =
			(	{ condition = condition, map = map, paramFor = paramFor, nextContinuation = NopContinuation}
				, contSt, txn))
		nestedTxn updFor () st locSt resAcc nrSubProcs txn

sendParamToSubProcess :: !Int !(?parParam) !*(ParInfo parParam .locSt partRes) -> *(ParInfo parParam .locSt partRes)
sendParamToSubProcess _ ?None parInfo
	= parInfo
sendParamToSubProcess nrSubProcs (?Just parParam) parInfo=:{subProcesses, nextSubProcessNr}
	# {outChannel, inChannel} = subProcesses.[nextSubProcessNr].subprocess
	# (err, parInfo) = send (DoTransactionFor parParam) outChannel parInfo
	| isError err = abortError "sendTransactionToSubProcess" err "send returned an error"
	# parInfo = {parInfo & subProcesses = subProcesses, nextSubProcessNr = (inc nextSubProcessNr) rem nrSubProcs}
	= parInfo

deleteFromMulti :: !k !v !(DBMultiMap k v) !*Txn -> *Txn | key k
deleteFromMulti key val map (Txn info)
	# (?Just {highMap, lowMap, lowestKeyInHighMap, txnPtr}, info) = mapInfoOf map info
	# keyStr      = toString $ strRepresentation key
	# valStr      = (commonOf map).encode val
	# mapToDeleteFrom = if (maybe True (\lowestInHigh = keyStr < lowestInHigh) lowestKeyInHighMap) lowMap highMap
	# (ret, info) = mdbDel txnPtr mapToDeleteFrom keyStr valStr info
	| ret <> 0 && ret <> MDB_NOTFOUND = abortLmdbErrorOnMap "deleteFromMulti" map ret "mdbDel returned an error"
	# lowestKeyInHighMapNeedsUpdate = maybe False (\lowestInHigh = keyStr == lowestInHigh) lowestKeyInHighMap
	# info = if lowestKeyInHighMapNeedsUpdate (updateLowestKeyInHighMapOf map info) info
	= Txn info

//* The state used for running a condition on a map.
:: *RunState k v st =
	{ state :: !st //* The accumulator for the result.
	, runs :: ![!Run k!] //* All runs currently active.
	, suspendedRuns :: ![!SuspendedRuns k v!] //* Runs suspended, ordered by key to preserve order of results.
	, txnInfo :: !*TxnInfo //* The current transaction's info.
	}

instance SysCallEnv (RunState k v .st) where
	accWorld f runState
		# res = accUnsafe $ f
		= (res, runState)

//* State keeping track of parallel execution.
:: *ParInfo parParam locSt partRes =
	{ subProcesses     :: !{#NestedSubProcess parParam locSt partRes} //* The subprocesses to perform nested transactions on.
	, nextSubProcessNr :: !Int
		//* The next subprocess a transaction is assigned to. Must be within the range of `subProcesses`.
	, stopChannel :: !InChannel GetAndPassProcMsg
		//* Channel for indicating the end of the operation to the "get and pass" process.
	, operationStopped :: !Bool //* This indicate whether the operation was stopped by the stop predicate.
	}

instance SysCallEnv (ParInfo a .b c) where
	accWorld f parInfo
		# res = accUnsafe f
		= (res, parInfo)

:: GetCondAndPassResultsSubProcess :== SubProcess GetAndPassProcMsg ()

//* Subprocess processing nested transactions.
:: NestedSubProcess parParam locSt partRes =
	{ subprocess :: !SubProcess (NestedSubProcessOperation parParam locSt partRes) (UpdateParam partRes) //* The subprocess.
	, stopSignalChannel :: !OutChannel ()
		//* An additional channel to signal stopping the current operation. On the ordinary channel all queued message
		//* have to be read, before the stop signal is seen, wasting performance.
	}

//* The update parameter for updating the result state.
:: UpdateParam partRes
	= UpdateParam !partRes //* An update param for the result state.
	| NoMoreDataAvailable
	//* No more data is available. No other process in the pool will send a parameter after this is received from a
	//* single one.
	| OperationStopped
	//* The operation was stopped on request. This has to be received from all processes in the pool to make sure no
	//* messages related to the current operation are left on the channels.

//* Common information for DB map and multi-maps.
:: DBMapCommon k v =
	{ name   :: !MapName //* The map's name.
	, encode :: !v -> String //* Encoder for values.
	, decode :: !String -> v //* Decoder for values.
	, partName :: !?String //* The optional partition name.
	}

/**
 * Similar to `PredContinuation` without a continuation and `Stop` constructor. The flag of the `Key` constructor
 * indicates that it is known that the key is not in the lower map.
 */
:: NextKey k = Next | Key !Bool !((?(KeyStr k)) -> ?(PredContinuation k)) !(KeyStr k) | First

/**
 * If we retrieve data using multiple cursors at the same time (using `disjunctionOfWithCustomOrder`)
 * we use a `Run` per cursor to keep track of the state.
 */
:: Run k =
	{ continuation :: !PredContinuation k //* the current retrieval continuation
	, cursorLow    :: !Pointer            //* the cursor to the low map
	, cursorHigh   :: !Pointer            //* the cursor to the high map
	, onLowMap     :: !Bool               //* indicates on which map (low or high) data retrieval works currently
	}

//* The current cursor of the provided run.
currentCursorOf :: !(Run k) -> Pointer
currentCursorOf {onLowMap, cursorLow, cursorHigh} = if onLowMap cursorLow cursorHigh

withCursorAndContinuation :: !Pointer !(PredContinuation k) !(Run k) -> Run k
withCursorAndContinuation cursor cont run=:{onLowMap}
	| onLowMap  = {run & cursorLow = cursor,  continuation = cont}
	| otherwise = {run & cursorHigh = cursor, continuation = cont}

/**
 * As we have to yield results ordered by key, we suspend runs yielding a result until other runs yielded all results
 * with smaller keys. We collect all suspended runs for the same key/value, as we may only yield results for each key
 * once.
 */
:: SuspendedRuns k v =
	{ key :: !k //* The key for which the suspended runs yield a result.
	, keyStr :: !KeyStr k //* The key string corresponding to `key`.
	, fstRun :: !Run k
	  //* The first of the suspended runs for the key, used to yield the results. For multi-maps additional values have
	  //* to be retrieved using the run's cursor. As all runs share the same key, it does not matter which one we use to
	  //* yield the result.
	, valueOfFstRun :: !?v //* The value retrieved for `fstRun`.
	, lastMovementBackwardsForFstRun :: !Bool
	  //* Indicates whether the last cursor's movement for `fstRun` was backwards.
	, otherRuns :: ![!Run k!] //* The other suspended runs.
	}

/**
 * Retrieves data given a number of initial continuations.
 * The result is accumulated by a provided function.
 * This is the non-parallel variant.
 *
 * @param The accumulator function to accumulate all retrieved key-value pairs
 * @param A list of initial continuations
 * @param An optional custom order
 *        for determining the order of results in case multiple intial continuations are provided
 * @param Function decoding the values, if ?None is provided, no values are retrieved.
 * @param The map to retrieve data from
 * @param The initial accumulator state
 * @param The transaction as part of which the data is retrieved
 * @result The resulting accumulator state
 */
getCond ::
	!(Bool k (KeyStr k) (?v) Pointer -> *(*() -> *(*(RunState k v .st) -> (Bool, Bool, *(), *RunState k v .st))))
	![!PredContinuation k!]
	!(?(k k -> Bool))
	!(?(String -> v))
	!(dbMap k v)
	!.st
	!*TxnInfo
	-> (!.st, !*TxnInfo)
	| key k & dbMap dbMap
getCond withValue conts isLessThan mbValueCorrespondingTo map st info
	# (mapInfo, info) = mapInfoOf map info
	= case mapInfo of
		?None
			= (st, info)
		?Just mapInfo
			# {name, partName, decode} = commonOf map
			# (st, _, info) = executeCond
				withValue conts isLessThan mbValueCorrespondingTo mapInfo name partName st () info
			= (st, info)

forGetAndPassProcess :: !(GetCondAndPassResultsSubProcess *CDB -> *CDB) !*CDB -> *CDB
forGetAndPassProcess f db=:{CDB| getCondAndPassResultsProcess = GetCondAndPassResultsSubProcess proc} = f proc db
forGetAndPassProcess _ db = db

sendToGetAndPassProcess :: !GetAndPassProcMsg !*CDB -> *CDB
sendToGetAndPassProcess msg db =
	forGetAndPassProcess
		(\{outChannel} db
			# (mbError, db) = send msg outChannel db
			| isError mbError = abortError "sendToGetAndPassProcess" mbError "send returned an error"
			= db
		)
		db

forEachProcess :: !((NestedSubProcess parParam .locSt partRes) *CDB -> *CDB) !*CDB -> *CDB
forEachProcess f db=:{CDB| processPool} = forEachProcessIn processPool f db

forEachProcessIn :: !ProcessPool !((NestedSubProcess parParam .locSt partRes) *env -> *env) !*env -> *env
forEachProcessIn (ProcessPool processes) f env = Foldl (\env idx = f processes.[idx] env) env [!0..size processes - 1]

sendToAllProcesses :: !(NestedSubProcessOperation parParam .locSt partRes) !*CDB -> *CDB
sendToAllProcesses operation db=:{CDB| processPool} = sendToAllProcessesIn processPool operation db

sendToAllProcessesIn :: !ProcessPool !(NestedSubProcessOperation parParam .locSt partRes) !*env -> *env | SysCallEnv env
sendToAllProcessesIn pool operation env = forEachProcessIn pool sendMsg env
where
	sendMsg :: E.^ parParam .locSt partRes: !(NestedSubProcess parParam .locSt partRes) !*env -> *env | SysCallEnv env
	sendMsg {subprocess = {outChannel}} env
		# (mbError, env) = send operation outChannel env
		| isError mbError = abortError "sendToAllProcesses" mbError "send returned an error"
		= env

/**
 * This waits until a message for which the predicate holds is received from each process in the pool. As we cannot
 * know how many messages are already queued before such message and whether a process is blocked writing to the
 * channel, we have to peek each process's channel in turn to avoid deadlocks.
 */
waitForAllProcessesIn :: !ProcessPool !((UpdateParam partRes) -> Bool) !*TxnInfo -> *TxnInfo
waitForAllProcessesIn (ProcessPool pool) pred info =
	waitForAllProcessesIn` [!inChannel \\ {subprocess = {inChannel}} <-: pool] info
where
	waitForAllProcessesIn` :: E.^ partRes: ![!InChannel (UpdateParam partRes)] !*TxnInfo -> *TxnInfo
	// For a single channel left we can just do a non-busy wait without the danger of deadlocks.
	waitForAllProcessesIn` [!singleChannel] info = waitFor pred singleChannel info
	waitForAllProcessesIn` pool info
		# (pool, info) = Foldl
			(\(acc, info) inChannel
				# (msg, info) = peek inChannel info
				| isError msg = abortError "waitForAllProcessesIn" msg "peek returned an error"
				= if (maybe False pred $ fromOk msg) (acc, info) ([!inChannel: acc], info))
			([!], info) pool
		= if (IsEmpty pool) info (waitForAllProcessesIn` pool info)

/**
 * Retrieves data given a number of initial continuations.
 * The result is accumulated by a provided function.
 * This is the parallel variant of `getCond`.
 * @param The number of nested transactions performed concurrently.
 * @param The `GetContinuation` specifying which data to retrieve from several maps.
 * @param A nested transaction function running in parallel which processes the `parParam`s yielded by the
 *        `GetContinuation` chain to yield partial results.
 * @param A function which combines the partial results determined by the nested transaction function
 *        into a final/resulting accumulator state.
 * @param An initial state local to the function which processes found key-value pairs from any of the maps provided
 *        by the `GetContinuation` chain.
 * @param An initial continuation state.
 * @param Function which generates a local state for the nested transaction function running in parallel.
 * @param The initial accumulator state.
 * @param The transaction as part of which the data is retrieved.
 * @result The resulting accumulator state.
 */
getCondPar ::
	!Int
	!(GetContinuation parParam .keyValSt .continuationSt)
	!(parParam .locSt -> *(*ReadOnlyTxn -> *(partRes, .locSt, *ReadOnlyTxn)))
	!(partRes .resAcc -> *(*ReadOnlyTxn -> *(Bool, .resAcc, *ReadOnlyTxn)))
	!.keyValSt !.continuationSt !(() -> .locSt) !.resAcc !*TxnInfo
	-> (!.resAcc, !*TxnInfo)
getCondPar
	nrSubProcs continuation nestedTxn updFor keyValSt continuationSt locSt resAcc
	info=:{TxnInfo| getCondAndPassResultsProcess, dbis, processPool = pool=:ProcessPool processes}
	# (getAndPassProc, info) =
		case getCondAndPassResultsProcess of
			ParallelGetDisabled =
				abortError
					"getCondPar" (Error "parallel operations are disabled")
					"parallel operations have to be enabled in the database options"
			AlreadyInsideNestedTransaction =
				abortError
					"getCondPar" (Error "parallel operations cannot be nested")
					"this is not supported by the current implementation and would probably not provide optimal performance"
			GetCondAndPassResultsSubProcess proc=:{outChannel}
				| nrSubProcs > size processes =
						abortError
							"getCondPar"
							(Error "the subprocess pool does not contain enough processes for the operation")
							"increase the number of subprocesses in the database options"
				# (err, info) = send
					(DoOperation
						{continuation = continuation, nestedTxn = nestedTxn, locSt = locSt, keyValSt = keyValSt
						, continuationSt = continuationSt, nrSubProcs = nrSubProcs}) outChannel info
				| isError err = abortError "getCondPar" err "send returned an error"
				= (proc, info)
	# (resAcc, info) = collectResultFromSubProcess 0 resAcc processes getAndPassProc.outChannel info
	= (resAcc, info)
where
	collectResultFromSubProcess :: E.^ partRes .resAcc:
		!Int !.resAcc !{#NestedSubProcess parParam .locSt partRes} !(OutChannel GetAndPassProcMsg) !*TxnInfo
		-> (!.resAcc, *TxnInfo)
	collectResultFromSubProcess idx st processes stopChannel info
		# {subprocess = {inChannel}} = processes.[idx]
		# (partRes, info) = receive inChannel info
		| isError partRes = abortError "collectResultFromSubProcess" partRes "receive returned an error"
		= case fromOk partRes of
			NoMoreDataAvailable = (st, info)
			UpdateParam partRes
				# txn = txnWith info
				# (stop, st, txn) = updFor partRes st txn
				# info = infoOf txn
				| stop
					# (err, info) = send EndOfOperationForGetAndPass stopChannel info
					| isError err = abortError "collectResultFromSubProcess" err "send returned an error"
					// After we signaled to stop the operation we have to wait until we got an `OperationStopped` from
					// all processes in the pool, to make sure there are no messages left in the channels.
					# info = waitForAllProcessesIn pool (\msg = msg =: OperationStopped) info
					= (st, info)
				= collectResultFromSubProcess ((inc idx) rem nrSubProcs) st processes stopChannel info

:: DoOperationParams parParam keyValSt continuationSt locSt partRes =
	{ continuation :: !GetContinuation parParam keyValSt continuationSt
		//* The continuation chain to process.
	, nestedTxn :: !parParam locSt -> *(*ReadOnlyTxn -> *(partRes, locSt, *ReadOnlyTxn))
		//* The nested transaction which processes the `parParam`s provided by the continuation chain.
	, locSt :: !(() -> locSt)
		//* Function which yields a local state for each subprocess running the `nestedTxn`.
	, keyValSt :: !keyValSt
		//* The initial state used by the continuation chain when processing key-value pairs of a map.
	, continuationSt :: !continuationSt
		//* The initial continuation state used by the continuation chain.
	, nrSubProcs :: !Int // How many nested transaction are ran in parallel.
	}

//* Messages for the "get and pass" process for parallel operations.
:: *GetAndPassProcMsg
	= E. parParam .keyValSt .continuationSt .locSt partRes: DoOperation
		!*(DoOperationParams parParam keyValSt continuationSt locSt partRes)
	//* Do the provided operation, given the continuation chain, the nested txn with initial local state,
	//* the number of subprocesses, `.keyValSt` and `.continuationSt`.
	//* The number of subprocesses `Int` is provided seperately (not part of the record) due to a bug that is most
	//* likely resides within graph-copy.
	| EndOfOperationForGetAndPass //* Ends the current operation.
	| EndOfTransactionForGetAndPass //* Ends the current transaction.
	| CloseDatabaseForGetAndPass //* Closes the database and ends the subprocess.

//* Defines a `ParallelGetOperation`, basically  just a wrapper around `ParallelGetOperationState` to be able to provide
//* class constraints on the existentially quantified type variables `dbMap, `k` and `v`.
:: *ParallelGetOperation st parParam locSt partRes =
	E. dbMap k v: ParallelGetOperation !(ParallelGetOperationState dbMap k v st parParam locSt partRes)
	& dbMap dbMap & key k

//* Defines a parallel get operation state.
:: *ParallelGetOperationState dbMap k v st parParam locSt partRes =
	{ state :: !st //* The non-parallel accumulator state.
	, withValue ::
		!Bool k v Pointer -> *(*(ParInfo parParam locSt partRes) -> *(*(RunState k v st)
		-> *(Bool, *ParInfo parParam locSt partRes, *RunState k v st))) //* Function to process values found.
	, continuations :: ![!PredContinuation k!] //* The initial continuations defining which keys to retrieve.
	, isLessThan :: !?(k k -> Bool) //* Custom "<" operator for keys if provided.
	, map :: !dbMap k v //* The map the operation is performed on.
	, valueCorrespondingTo :: !String -> v //* Decoding function for values.
	}

getCondAndPassResults :: !ProcessPool !FilePath !CleanDBOptions !(InChannel GetAndPassProcMsg) x !*World -> *World
getCondAndPassResults
	processPool path
	{ syncMode, readahead, compactionFactor, maxNumberPages, readaheadPartitionDatabases
	, maxNumberPagesPartitionDatabases}
	inChannel _ world
	// Use SIGKILL to make sure the child process really terminates in the the parent does.
	# (ret, world) = prctl1 PR_SET_PDEATHSIG SIGKILL world
	# (db, world) = openDatabaseWithoutCreatingSubProcs
		path syncMode readahead readaheadPartitionDatabases maxNumberPagesPartitionDatabases compactionFactor
		maxNumberPages AlreadyInsideNestedTransaction emptyProcessPool world
	# db = getCondAndPassResultsNoActiveTransaction db
	= closeDatabase db world
where
	getCondAndPassResultsNoActiveTransaction :: !*CDB -> *CDB
	getCondAndPassResultsNoActiveTransaction db
		# (msg, db) = receive inChannel db
		// If the parent process has terminated and the channel closed we can just exit.
		| msg =: (Error (OUTPUT_CHANNEL_CLOSED, _)) = exit 0 db
		| isError msg = abortError "getCondAndPassResultsNoActiveTransaction" msg "receive returned an error"
		= case fromOk msg of
			DoOperation {continuation, nestedTxn, locSt, keyValSt, continuationSt, nrSubProcs = nrSubProcs}
				# db = sendToAllProcessesIn processPool (StartOfOperation nestedTxn locSt) db
				# (_, db) = doReadOnlyTransaction
					(getCondAndPassResultsTransaction
						processPool (?Just (continuation, keyValSt, continuationSt)) nrSubProcs) db
				= getCondAndPassResultsNoActiveTransaction db
			EndOfTransactionForGetAndPass
				# db = sendToAllProcessesIn processPool EndOfTransaction db
				= getCondAndPassResultsNoActiveTransaction db // Ignore message as no transaction is active.
			CloseDatabaseForGetAndPass
				# db = sendToAllProcessesIn processPool CloseDatabase db
				= forEachProcess
					(\{subprocess, stopSignalChannel} db
						# (err, db) = waitForSubProcessToTerminate subprocess db
						| isError err = abortError
							"getCondAndPassResultsNoActiveTransaction" err
							"waitForSubProcessToTerminate returned an error"
						# status = fromOk err
						# (exitCode, exitedWithCoreDump, signal) =
							(exitCodeIn status, hadCoreDump status, signalIn status)
						| exitCode <> ?Just 0 || exitedWithCoreDump || isJust signal = abortError
							"getCondAndPassResultsNoActiveTransaction" (Error "error waiting for subproces")
							"subprocess did not exit normally"
						= closeOutChannelAbortOnError "getCondAndPassResultsNoActiveTransaction" stopSignalChannel db)
					db

	getCondAndPassResultsTransaction ::
		!ProcessPool !(?(!GetContinuation parParam .keyValSt .continuationSt, !.keyValSt, !.continuationSt)) !Int
		!*ReadOnlyTxn -> (!(), !*ReadOnlyTxn)
	getCondAndPassResultsTransaction pool=:(ProcessPool processes) mbContinuation nrSubProcs txn
		# txn = case mbContinuation of
			?None = txn
			?Just (continuation, keyValSt, continuationSt)
				= executeContinuationChain
					{subProcesses = processes, nextSubProcessNr = 0, stopChannel = inChannel, operationStopped = False}
					continuation nrSubProcs keyValSt continuationSt txn
		# (msg, txn) = receive inChannel txn
		// If the parent process has terminated and the channel closed we can just exit.
		| msg =: (Error (OUTPUT_CHANNEL_CLOSED, _)) = ((), exit 0 txn)
		| isError msg = abortError "getCondAndPassResultsTransaction" msg "receive returned an error"
		= case fromOk msg of
			DoOperation {continuation, nestedTxn, locSt, keyValSt, continuationSt, nrSubProcs = nrSubProcs}
				# txn = sendToAllProcessesIn pool (StartOfOperation nestedTxn locSt) txn
				= getCondAndPassResultsTransaction
					pool (?Just (continuation, keyValSt, continuationSt)) nrSubProcs txn
			// This message can only be received if the operation is stopped, but we have already reached the
			// end of the data anyhow. We still have to send stop signals, as the main process waits until it
			// receives stop-confirmations from all processes.
			EndOfOperationForGetAndPass
				# txn = sendStopSignalsToEachProcess txn
				= getCondAndPassResultsTransaction pool ?None nrSubProcs txn
			EndOfTransactionForGetAndPass
				# txn = sendToAllProcessesIn pool EndOfTransaction txn
				= ((), txn)
	where
		executeContinuationChain ::
			!(ParInfo parParam locSt partRes) !(GetContinuation parParam .keyValSt .continuationSt) !Int !.keyValSt
			!.continuationSt !*ReadOnlyTxn -> *ReadOnlyTxn
		executeContinuationChain parInfo=:{ParInfo|subProcesses, nextSubProcessNr} continuation nrSubProcs keyValSt
			continuationSt txn
			# (mbParallelGetOperationAndRemainingChain, continuationSt, txn) = currentContinuationToParallelGetOperation
				continuation nrSubProcs keyValSt continuationSt txn
			= case mbParallelGetOperationAndRemainingChain of
				?Just
					(ParallelGetOperation
						{state, withValue, continuations, isLessThan, map, valueCorrespondingTo}, remainingChain)
					# (mbMapInfo, info) = mapInfoOf map $ infoOf txn
					| isNone mbMapInfo = executeContinuationChain
						parInfo remainingChain nrSubProcs state continuationSt (txnWith info)
					# mapInfo = fromJust mbMapInfo
					# common=:{name, partName} = commonOf map
					# (keyValSt, parInfo=:{operationStopped}, info) = executeCond
						(withValue` withValue mapInfo common) continuations isLessThan (?Just valueCorrespondingTo)
						mapInfo name partName state parInfo info
					| operationStopped
						# info = sendStopSignalsToEachProcess info
						= txnWith info
					= executeContinuationChain parInfo remainingChain nrSubProcs keyValSt continuationSt (txnWith info)
				?None
					# {subprocess = {outChannel}, stopSignalChannel} = subProcesses.[nextSubProcessNr]
					// Send signal to indicate end.
					# (err, txn) = send EndOfOperation outChannel txn
					| isError err = abortError "getCondAndPassResultsTransaction" err "send returned an error"
					= txn

		withValue` ::
			!(	Bool k v Pointer -> *(*(ParInfo parParam .locSt partRes) -> *(*(RunState k v .st)
				-> (Bool, *ParInfo parParam .locSt partRes, *RunState k v .st))))
			!MapInfo !(DBMapCommon k v)
			!Bool !k !(KeyStr k) !(?v) !Pointer !*(ParInfo parParam .locSt partRes) !*(RunState k v .st)
			-> (!Bool, !Bool, !*ParInfo parParam .locSt partRes, !*RunState k v .st) | key k
		withValue` withValue mapInfo common lastMovementBackwards key keyStr (?Just val) cursor parInfo=:{stopChannel}
			st
			# (stopMsg, st=:{state, txnInfo, runs, suspendedRuns}) = peek stopChannel st
			| isError stopMsg = abortError "withValue`" stopMsg "peek returned an error"
			| stopMsg =: (Ok (?Just _)) = (True, False, {parInfo & operationStopped = True}, st)
			= case mapInfo.isMultiMap of
				True
					// We still have to read the rest of the values for the given key.
					# (stop, (state, runs, suspendedRuns, parInfo), txnInfo) = multiAccumRestNested
						(toString keyStr) val lastMovementBackwards common transformedWithValue
						(state, runs, suspendedRuns, parInfo) cursor txnInfo
					= (stop, False, parInfo, {state = state, runs = runs, suspendedRuns = suspendedRuns, txnInfo = txnInfo})
				False
					# (stop, parInfo, st) = withValue lastMovementBackwards key val cursor parInfo st
					= (stop, False, parInfo, st)
		where
			transformedWithValue ::
				E.^ k v parParam partRes .locSt .st:
				!v !(!.st, [!Run k!], [!SuspendedRuns k v!], !*(ParInfo parParam .locSt partRes)) !*TxnInfo
				-> *(Bool, !(!.st, [!Run k!], [!SuspendedRuns k v!],!*(ParInfo parParam .locSt partRes)), *TxnInfo)
			transformedWithValue v (state, runs, suspendedRuns, parInfo) txnInfo
				# (stop, parInfo, st=:{state, runs, suspendedRuns, txnInfo}) = withValue
					lastMovementBackwards key v cursor parInfo
					{state = state, runs = runs, suspendedRuns = suspendedRuns, txnInfo = txnInfo}
				= (stop, (state, runs, suspendedRuns, parInfo), txnInfo)

		sendStopSignalsToEachProcess :: !*env -> *env | SysCallEnv env
		sendStopSignalsToEachProcess env = forEachProcessIn
			pool
			// We send stop signals on both channels. Processes react to whichever they see first.
			(\{stopSignalChannel, subprocess = {outChannel}} env
				# (err, env) = send StopOperation outChannel env
				# (err, env) = send () stopSignalChannel env
				= env)
			env

		currentContinuationToParallelGetOperation ::
			!(GetContinuation parParam .keyValSt .continuationSt) !Int !.keyValSt !.continuationSt !*ReadOnlyTxn
			-> (!?(*ParallelGetOperation .keyValSt parParam .locSt partRes
				, !GetContinuation parParam .keyValSt .continuationSt)
				, !.continuationSt
				, !*ReadOnlyTxn)
		currentContinuationToParallelGetOperation (GetContinuation continuation) nrSubProcs keyValSt continuationSt txn
			# ({condition={Condition|continuations, isLessThan}, map, paramFor, nextContinuation}, continuationSt, txn)
				= continuation continuationSt txn
			# parallelGetOperation =
				{state = keyValSt, withValue = withValue paramFor nrSubProcs, continuations = continuations
				, isLessThan = isLessThan, map = map, valueCorrespondingTo = (commonOf map).decode}
			= (?Just (ParallelGetOperation parallelGetOperation, nextContinuation), continuationSt, txn)
		where
			withValue ::
				!(k v .keyValSt -> *(Bool, ?parParam, .keyValSt)) !Int x !k !v y !*(ParInfo parParam .locSt partRes)
				!*(RunState k v .keyValSt) -> (!Bool, !*ParInfo parParam .locSt partRes, !*RunState k v .keyValSt)
			withValue withKeyVal nrSubProcs _ key val _ parInfo st=:{txnInfo, state}
				# (stop, parParam, state) = withKeyVal key val state
				# parInfo = sendParamToSubProcess nrSubProcs parParam parInfo
				= (stop, parInfo, {st & txnInfo = txnInfo, state = state})
		currentContinuationToParallelGetOperation _ _ _ continuationSt txn = (?None, continuationSt, txn)

//* Messages for the subprocesses performing the nested parallel transaction.
:: NestedSubProcessOperation parParam locSt partRes
	= DoTransactionFor !parParam //* Perform the operation for the given parameter.
	| StartOfOperation !(parParam locSt -> *(*ReadOnlyTxn -> *(partRes, locSt, *ReadOnlyTxn))) !(() -> locSt)
	//* Start an operation given a nested transaction function.
	| EndOfOperation //* Ends the current operation due to no data being available anymore.
	| StopOperation //* Stops the current operation due to the stop predicate.
	| EndOfTransaction //* Ends the current transaction.
	| CloseDatabase //* Closes the database and terminates the process.

doNestedSubProcess ::
	![!UntypedInChannel] ![!UntypedOutChannel] !FilePath !CleanDBOptions !(InChannel ())
	!(InChannel (NestedSubProcessOperation parParam .locSt partRes)) !(OutChannel (UpdateParam partRes)) !*World
	-> *World
doNestedSubProcess
	inChannelsToClose outChannelsToClose path
	{ syncMode, readahead, compactionFactor, maxNumberPages, readaheadPartitionDatabases
	, maxNumberPagesPartitionDatabases}
	stopSigInChannel inChannel outChannel world
	// Use SIGKILL to make sure the child process really terminates in the the parent does.
	# (ret, world) = prctl1 PR_SET_PDEATHSIG SIGKILL world
	# world = Foldl
		(\w (UntypedInChannel ch) = closeInChannelAbortOnError "doNestedSubProcess" ch w) world inChannelsToClose
	# world = Foldl
		(\w (UntypedOutChannel ch) = closeOutChannelAbortOnError "doNestedSubProcess" ch w) world outChannelsToClose
	// Increase buffersize of channel for receiving parameters to improve performance.
	# (size, world) = setBufferSize (64*PAGESIZE) inChannel world
	# world = traceSetBufferSizeWarning size world
	# (db, world) = openDatabaseWithoutCreatingSubProcs
		path syncMode readahead readaheadPartitionDatabases maxNumberPagesPartitionDatabases compactionFactor
		maxNumberPages AlreadyInsideNestedTransaction emptyProcessPool world
	# db = doNestedSubProcessNoActiveTransaction db
	= closeDatabase db world
where
	doNestedSubProcessNoActiveTransaction :: !*CDB -> *CDB
	doNestedSubProcessNoActiveTransaction db
		# (operation, db) = receive inChannel db
		// If the parent process has terminated and the channel closed we can just exit.
		| operation =: (Error (OUTPUT_CHANNEL_CLOSED, _)) = exit 0 db
		| isError operation = abortError "doNestedSubProcessNoActiveTransaction" operation "receive returned an error"
		= case fromOk operation of
			StartOfOperation txnFunc locSt
				# (_, db) = doReadOnlyTransaction (doNestedSubProcessTransaction ?None txnFunc (locSt ())) db
				= doNestedSubProcessNoActiveTransaction db
			EndOfTransaction
				= doNestedSubProcessNoActiveTransaction db // Ignore message as no transaction is active.
			CloseDatabase = db
			DoTransactionFor _ =
				abortError "doNestedSubProcessNoActiveTransaction" (Error "operation not started yet") ""
			EndOfOperation = abortError
				"doNestedSubProcessNoActiveTransaction" (Error "cannot end operation which is not started yet") ""
			StopOperation = abortError
				"doNestedSubProcessNoActiveTransaction" (Error "cannot stop operation which is not started yet") ""

	doNestedSubProcessTransaction :: E.^ parParam .locSt partRes:
		!(?parParam) !(parParam .locSt -> .(*ReadOnlyTxn -> *(partRes, .locSt, *ReadOnlyTxn))) !.locSt !*ReadOnlyTxn
		-> (!(), !*ReadOnlyTxn)
	doNestedSubProcessTransaction param txnFunc locSt txn
		# (locSt, txn) = case param of
			?Just param
				# (partRes, locSt, txn) = txnFunc param locSt txn
				# (err, txn) = send (UpdateParam partRes) outChannel txn
				| isError err = abortError "doNestedSubProcessTransaction" err "send returned an error"
				= (locSt, txn)
			_
				= (locSt, txn)
		# (stopSignal, txn) = peek stopSigInChannel txn
		| stopSignal =: (Error (OUTPUT_CHANNEL_CLOSED, _)) = ((), exit 0 txn)
		| isError stopSignal = abortError "doNestedSubProcessTransaction" stopSignal "peek returned an error"
		| stopSignal =: (Ok (?Just _))
			// We have to wait for the stop signal on `inChannel` as well, to make sure the message does not remain on
			// the channel. With this we also discard any remaining superfluous transaction requests.
			# txn = waitFor (\msg = msg =: StopOperation) inChannel txn
			// Send `OperationStopped` to signal that no more messages will be sent.
			# (err, txn) = send OperationStopped outChannel txn
			| isError err = abortError "doNestedSubProcessTransaction" err "send returned an error"
			= doNestedSubProcessTransaction ?None txnFunc locSt txn
		# (operation, txn) = receive inChannel txn
		// If the parent process has terminated and the channel closed we can just exit.
		| operation =: (Error (OUTPUT_CHANNEL_CLOSED, _)) = ((), exit 0 txn)
		| isError operation = abortError "doNestedSubProcessTransaction" operation "receive returned an error"
		= case fromOk operation of
			StartOfOperation txnFunc locSt = doNestedSubProcessTransaction ?None txnFunc (locSt ()) txn
			EndOfOperation
				// Provide `?None` stopping execution if no operation is provided anymore.
				# (err, txn) = send NoMoreDataAvailable outChannel txn
				| isError err = abortError "doNestedSubProcessTransaction" err "send returned an error"
				= doNestedSubProcessTransaction ?None txnFunc locSt txn
			StopOperation
				// We have to wait for the stop signal on `stopSigInChannel` as well, to make sure the message does not
				// remain on the channel.
				# (stopSignal, txn) = receive stopSigInChannel txn
				| isError stopSignal = abortError "doNestedSubProcessTransaction" stopSignal "receive returned an error"
				# (err, txn) = send OperationStopped outChannel txn
				| isError err = abortError "doNestedSubProcessTransaction" err "send returned an error"
				= doNestedSubProcessTransaction ?None txnFunc locSt txn
			DoTransactionFor param = doNestedSubProcessTransaction (?Just param) txnFunc locSt txn
			EndOfTransaction = ((), txn)

// This is a parametrised implementation of executing conditions. It is used for non-parallel and parallel execution.
// `mbValueCorrespondingTo` is `?None` if it is not necessary to retrieve values for `withValue`.
executeCond ::
	!(Bool k (KeyStr k) (?v) Pointer -> *(*extraSt -> *(*(RunState k v .st)
	-> (Bool, Bool, *extraSt, *RunState k v .st))))
	![!PredContinuation k!] !(?(k k -> Bool)) !(?(String -> v)) !MapInfo !String !(?String) !.st
	!*extraSt // e.g. `()` for non-parallel execution and `ParInfo`for parallel execution
	!*TxnInfo -> (!.st, !*extraSt, !*TxnInfo) | key k
executeCond
	withValue conts isLessThan mbValueCorrespondingTo {lowMap, highMap, lowestKeyInHighMap, largeValueMap, txnPtr}
	mapName partName st extraSt info=:{ptr}
	# (runs, info) = Foldl (withRun lowMap highMap) ([!!], info) conts
	# (extraSt, {txnInfo, state}) =
		resultOfRuns
			((\str = KeyStr str) <$> lowestKeyInHighMap) extraSt
			{state = st, runs = runs, suspendedRuns = [!!], txnInfo = info}
	# info =
		Foldl
			(\txn {cursorLow, cursorHigh} = mdb_cursor_close cursorLow $ mdb_cursor_close cursorHigh txn) txnInfo runs
	= (state, extraSt, info)
where
	withRun :: !Int !Int !(![!Run k!], !*TxnInfo) !(PredContinuation k) -> (![!Run k!], !*TxnInfo)
	withRun dbiLow dbiHigh (runs, info) cont
		# (curLow,  info) = openCursor dbiLow txnPtr info
		# (curHigh, info) = openCursor dbiHigh txnPtr info
		= ([!{Run| continuation = cont, cursorLow = curLow, cursorHigh = curHigh, onLowMap = True}: runs!], info)

	resultOfRuns ::
		E.^ k v *extraSt .st: !(?(KeyStr k)) !*extraSt !*(RunState k v .st) -> (!*extraSt, !*RunState k v .st) | key k
	resultOfRuns lowestKeyInHighMap extraSt st=:{runs = [!!], suspendedRuns}
		// If there are no runs left, we re-activate the suspended runs for the next key.
		= case suspendedRuns of
			[!!] = (extraSt, st)
			[!{key, keyStr, valueOfFstRun, fstRun, lastMovementBackwardsForFstRun, otherRuns}: restSuspendedRuns!]
				# cur = currentCursorOf fstRun
				# (stop, deleteCurrent, extraSt, st) =
					withValue lastMovementBackwardsForFstRun key keyStr valueOfFstRun cur extraSt st
				# (ret, st) = case deleteCurrent of
					True
						# (ret, st) = mdb_cursor_del cur 0 st
						| ret <> 0 = abortLmdbError "resultOfRuns" ret "mdb_cursor_del returned an error"
						# stringForKey = toString $ strRepresentation key
						# (ret, txnInfo) = case largeValueMap of
							?Just largeValueMap = deleteLargeValueMapValues stringForKey largeValueMap txnPtr st.txnInfo
							?None
								# (mbLargeValueMap, txnInfo) = openLargeValueMapRead
									ptr mapName partName st.txnInfo
								= case mbLargeValueMap of
									?None = (0, txnInfo)
									?Just largeValueMap = deleteLargeValueMapValues
										stringForKey largeValueMap txnPtr txnInfo
						| ret <> 0 = abortLmdbError "resultOfRuns" ret "deleteLargeValueMapValues returned an error"
						= (ret, {st & txnInfo = txnInfo})
					False = (0, st)
				| ret <> 0 = abortLmdbError "resultOfRuns" ret "mdb_cursor_del returned an error"
				| stop = (extraSt, st)
				# st = {st & runs = [!fstRun: otherRuns!], suspendedRuns = restSuspendedRuns}
				= resultOfRuns lowestKeyInHighMap extraSt st
	resultOfRuns lowestKeyInHighMap extraSt st=:{RunState| runs = [!run: restRuns!]}
		# st = {RunState| st & runs = restRuns}
		= case run.Run.continuation of
			NextKey pred = getCondNextKey lowestKeyInHighMap Next pred run extraSt st
			GivenKey {targetKey, usePreviousKey, targetKeyPred} = getCondNextKey
				lowestKeyInHighMap (Key False (fromMaybe (\_ -> ?None) usePreviousKey) targetKey) targetKeyPred run
				extraSt st
			Stop = resultOfRuns lowestKeyInHighMap extraSt st
	where
		getCondNextKey ::
			E.^ k v *extraSt .st:
				!(?(KeyStr k)) !(NextKey k) !(Pred k) !(Run k) !*extraSt !*(RunState k v .st)
				-> (!*extraSt, !*RunState k v .st) | key k
		getCondNextKey lowestKeyInHighMap nextKey prd=:(Pred pred) run=:{onLowMap} extraSt st=:{txnInfo={ptr}}
			# continueOnLowMap =
					not onLowMap
				&&
					case nextKey of
						Key notPresentOnLow _ targetKey = not notPresentOnLow
						_  = False
			| continueOnLowMap = getCondNextKey lowestKeyInHighMap nextKey prd {run & onLowMap = True} extraSt st
			# targetKey          = keyStrOf nextKey
			# mdbOperation       = mdbOperationFor nextKey
			# (ret, keyStr, st) = mdbCursorGetReadKey (currentCursorOf run) targetKey valVal mdbOperation st
			# contIfNoPrevKey =
				case nextKey of
					Key _ contIfNoPrevKeyFor _ =
						contIfNoPrevKeyFor (if (ret == MDB_NOTFOUND) ?None (?Just $ KeyStr keyStr))
					_ =
						?None
			# gotoPrev = isJust contIfNoPrevKey
			| ret == MDB_NOTFOUND && onLowMap
				# nextKey = case nextKey of
					Next = First
					First = First
					Key _ contIfNoPrevKeyFor key = Key True contIfNoPrevKeyFor key
				= getCondNextKey lowestKeyInHighMap nextKey prd {run & onLowMap = False} extraSt st
			| ret == MDB_NOTFOUND && not gotoPrev = resultOfRuns lowestKeyInHighMap extraSt st
			# (backwardsMove, ret, keyStr, run, st) =
				if gotoPrev (gotoPrevious (ret == MDB_NOTFOUND) run st) (False, ret, keyStr, run, st)
			# cursor = currentCursorOf run
			| ret == MDB_NOTFOUND =
				case contIfNoPrevKey of
					?None -> resultOfRuns lowestKeyInHighMap extraSt st
					?Just cont ->
						resultOfRuns
							lowestKeyInHighMap
							extraSt
							{RunState| st & runs = [!withCursorAndContinuation cursor cont run: st.RunState.runs!]}
			| ret <> 0 = abortLmdbError "getCondNextKey" ret ""
			# keyStr       = KeyStr keyStr
			# (mbValStr, st) = case mbValueCorrespondingTo of
				?None = (?None, st)
				?Just valueCorrespondingTo
					# (valStr, st) = readStringFromVal valVal st
					# (valStrsLargeMap, txnInfo) = case largeValueMap of
						?None
							# (mbLargeValueMap, txnInfo) = openLargeValueMapRead ptr mapName partName st.txnInfo
							= case mbLargeValueMap of
								?None = ([], txnInfo)
								?Just largeValueMap = readValueFromLargeValueMap
									largeValueMap (toString keyStr) txnPtr txnInfo
						?Just largeValueMap = readValueFromLargeValueMap
							largeValueMap (toString keyStr) txnPtr st.txnInfo
					# st = {st & txnInfo = txnInfo}
					# valStr = concat [valStr:valStrsLargeMap]
					= (?Just valStr, st)
			# res = pred keyStr backwardsMove
			# run = withCursorAndContinuation cursor res.PredResult.continuation run
			| res.holds && st.RunState.runs =: [!!] && st.suspendedRuns =: [!!]
				# (stop, deleteCurrent, extraSt, st) =
					withValue
						backwardsMove (fromStrRepresentation keyStr) keyStr
						((\valCorrespondingTo str = valCorrespondingTo str) <$> mbValueCorrespondingTo <*> mbValStr)
						cursor extraSt st
				# (ret, st) = case deleteCurrent of
					True
						# (ret, st) = mdb_cursor_del cursor 0 st
						| ret <> 0 = abortLmdbError "getCondNextKey" ret "mdb_cursor_del returned an error"
						# (ret, txnInfo) = case largeValueMap of
							?Just largeValueMap =
								deleteLargeValueMapValues (toString keyStr) largeValueMap txnPtr st.txnInfo
							?None
								# (mbLargeValueMap, txnInfo) = openLargeValueMapRead ptr mapName partName st.txnInfo
								= case mbLargeValueMap of
									?None = (0, txnInfo)
									?Just largeValueMap =
										deleteLargeValueMapValues (toString keyStr) largeValueMap txnPtr txnInfo
						| ret <> 0 = abortLmdbError "getCondNextKey" ret "deleteLargeValueMapValues returned an error"
						= (ret, {st & txnInfo = txnInfo})
					False = (0, st)
				| stop = (extraSt, st)
				= resultOfRuns lowestKeyInHighMap extraSt {RunState| st & runs = [!run!]}
			# st = if res.holds
				// We suspend the run, as there may be other runs yielding a result for a smaller key and we have to
				// keep results sorted.
				{st & suspendedRuns = withSuspended
					(fromStrRepresentation keyStr) keyStr mbValStr run backwardsMove st.suspendedRuns}
				{RunState| st & runs = [!run: st.RunState.runs!]}
			= resultOfRuns lowestKeyInHighMap extraSt st
		where
			gotoPrevious :: !Bool !(Run k) !*st -> (!Bool, !Int, !String, !Run k, !*st)
			gotoPrevious cursorBeyondEnd run=:{onLowMap} st
				# (ret, keyStr, st) =
					// Using `MDB_PREV` always would be correct, but could cause fatal crashes, corrupting the database,
					// in earlier versions. As the cause of the issue is not known, there is no certainty that the bug
					// does not reappear. As the impact of the check in terms of performance and code complexity is
					// minor compared to the risk of getting corrupt databases, the check remains in the code.
					mdbCursorGetReadKey (currentCursorOf run) "" valVal (if cursorBeyondEnd MDB_LAST MDB_PREV) st
				| ret == MDB_NOTFOUND =
					if onLowMap (True, ret, "", run, st) (gotoPrevious True {run & onLowMap = True} st)
				= (True, ret, keyStr, run, st)

			withSuspended :: E.^ k v:
				!k !(KeyStr k) !(?String) !(Run k) !Bool ![!SuspendedRuns k v!] -> [!SuspendedRuns k v!]
			withSuspended newKey newKeyStr mbValStr run lastMoveWasBackwards [!!] =
				[!	{ key = newKey, keyStr = newKeyStr, fstRun = run, otherRuns = [!!]
					, lastMovementBackwardsForFstRun = lastMoveWasBackwards
					, valueOfFstRun =
						(\valCorrespondingTo str = valCorrespondingTo str) <$> mbValueCorrespondingTo <*> mbValStr}!]
			withSuspended
				newKey newKeyStr mbValStr run lastMoveWasBackwards suspRuns=:[!fst=:{key, keyStr, otherRuns}: rest!]
				| newIsLessThan =
					[! { key = newKey, keyStr = newKeyStr , fstRun = run
						, valueOfFstRun =
							(\valCorrespondingTo str = valCorrespondingTo str) <$> mbValueCorrespondingTo <*> mbValStr
						, lastMovementBackwardsForFstRun = lastMoveWasBackwards, otherRuns = [!!]}
					:  suspRuns!]
				| curIsLessThan = [!fst: withSuspended newKey newKeyStr mbValStr run lastMoveWasBackwards rest!]
				| otherwise     = [!{SuspendedRuns| fst & otherRuns = [!run: otherRuns!]}: rest!]
			where
				newIsLessThan =
					case isLessThan of
						?None            -> newKeyStr < keyStr
						?Just isLessThan -> isLessThan newKey key
				curIsLessThan =
					case isLessThan of
						?None            -> keyStr < newKeyStr
						?Just isLessThan -> isLessThan key newKey

mdbOperationFor :: !(NextKey k) -> Int
mdbOperationFor Next = MDB_NEXT_NODUP
mdbOperationFor (Key _ _ _) = MDB_SET_RANGE
mdbOperationFor First = MDB_FIRST

keyStrOf :: !(NextKey k) -> String
keyStrOf (Key _ _ (KeyStr str)) = str
keyStrOf _ = ""

getRest :: !Bool !(DBMapCommon k v) ![v] !Pointer !*TxnInfo -> (![v], !*TxnInfo)
getRest lastMoveWasBackwards common acc cur info
	# (ret, valStr, info) = mdbCursorGetReadVal cur "" (if lastMoveWasBackwards MDB_PREV_DUP MDB_NEXT_DUP) info
	| ret == MDB_NOTFOUND = (if lastMoveWasBackwards acc (reverse acc), info)
	| ret <> 0            = abortLmdbErrorOnMapCommon "getRest" common ret "mdbCursorGetReadVal returned an error"
	= getRest lastMoveWasBackwards common [common.decode valStr: acc] cur info

multiAccumRestNested ::
	!String !v !Bool !(DBMapCommon k v) !(v .st -> *(*TxnInfo -> *(Bool, .st, *TxnInfo))) !.st !Pointer !*TxnInfo
	-> (!Bool, !.st, !*TxnInfo) | key k
multiAccumRestNested key fstVal lastMoveWasBackwards common withValue acc cur info = multiAccumRestNestedDelete key
	fstVal lastMoveWasBackwards common withValue` acc cur info
where
	withValue` :: E.^ v .st: !v !.st !*TxnInfo -> (!Bool, !Bool, !.st, !*TxnInfo)
	withValue` v st info
		# (stop, st, info) = withValue v st info
		= (stop, False, st, info)

// NB: this cannot be used within a function that is based on a `ReadOnlyTxn` if values are actually deleted.
multiAccumRestNestedDelete ::
	!String !v !Bool !(DBMapCommon k v) !(v .st -> *(*TxnInfo -> *(Bool, Bool, .st, *TxnInfo))) !.st !Pointer !*TxnInfo
	-> (!Bool, !.st, !*TxnInfo) | key k
multiAccumRestNestedDelete keyStr fstVal lastMoveWasBackwards common withValue acc cur info
	# (fstVal, info) = case lastMoveWasBackwards of
		// This is done to make sure values for a key are yielded in proper order.
		True
			# (ret, valStr, info) = mdbCursorGetReadVal cur "" MDB_FIRST_DUP info
			| ret <> 0 = abortLmdbErrorOnMapCommon
				"multiAccumRestNestedDelete" common ret "mdbCursorGetReadVal returned an error"
			= (common.decode valStr, info)
		False = (fstVal, info)
	= withValueAndRepositionCursor fstVal acc info
where
	multiAccumRestNestedDelete` ::
		E.^ v .st: !Bool !(DBMapCommon k v) !(v .st -> *(*TxnInfo -> *(Bool, Bool, .st, *TxnInfo))) !.st !Pointer
		!*TxnInfo -> (!Bool, !.st, !*TxnInfo)
	multiAccumRestNestedDelete` cursorAlreadyInCorrectPosition common withValue acc cur info
		// If the previous val was deleted the cursor is already in the correct position, otherwise we need to go next.
		# (ret, valStr, info) = mdbCursorGetReadVal
			cur "" (if cursorAlreadyInCorrectPosition MDB_GET_CURRENT MDB_NEXT_DUP) info
		| ret == MDB_NOTFOUND = (False, acc, info)
		| ret <> 0 = abortLmdbErrorOnMapCommon
			"multiAccumRestNestedDelete" common ret "mdbCursorGetReadVal returned an error"
		# val = common.decode valStr
		= withValueAndRepositionCursor val acc info

	withValueAndRepositionCursor :: E.^ v .st: !v !.st !*TxnInfo -> (!Bool, !.st, !*TxnInfo)
	withValueAndRepositionCursor val acc info
		# (stop, del, acc, info) = withValue val acc info
		// mdb_cursor_del will go to the next data record.
		# (ret, info) = if del (mdb_cursor_del cur 0 info) (0, info)
		| stop = (True, acc, info)
		# (ret, keyStr`, info) = if del (mdbCursorGetReadKey cur keyStr valVal MDB_GET_CURRENT info) (0, keyStr, info)
		// We check if performing `mdb_cursor_del` caused the key to change...
		| keyStr` <> keyStr
			// If so, we need to go to the previous key because `executeCond` will go to the next key again,
			// causing the cursor to remain in position
			# (ret, keyStr`, info) = if del (mdbCursorGetReadKey cur keyStr valVal MDB_PREV info) (0, keyStr, info)
			= (stop, acc, info)
		// The key did not change so there might be more values for the same key.
		= multiAccumRestNestedDelete` del common withValue acc cur info

deleteAll :: !(dbMap k v) !*Txn -> *Txn | dbMap dbMap
deleteAll map (Txn info=:{ptr})
	# (?Just {highMap, lowMap}, info) = mapInfoOf map info
	# (ret, info) = mdb_drop ptr highMap 0 info
	| ret <> 0 = abortLmdbErrorOnMap "deleteAll" map ret "mdb_drop returned an error (highMap)"
	# (ret, info) = mdb_drop ptr lowMap 0 info
	| ret <> 0 = abortLmdbErrorOnMap "deleteAll" map ret "mdb_drop returned an error (lowMap)"
	= Txn info

deleteAllMaps :: !*CDB -> *CDB
deleteAllMaps cdb = snd $ doTransaction (\txn = ((), deleteAllMapsTxn True txn)) cdb

deleteDataFromAllMaps :: !*Txn -> *Txn
deleteDataFromAllMaps txn = deleteAllMapsTxn False txn

// If `dropDbis`is set, this transaction is only safe if the database has not been modified in the same transaction, as
// all dbis are dropped and closed. See the documentation of `mdb_dbi_close` for the caveats of doing this.
deleteAllMapsTxn :: !Bool !*Txn -> *Txn
deleteAllMapsTxn dropDbis txn=:(Txn {ptr, partDbs})
	# (ret, txn) = deleteAllSubDbs dropDbis ptr txn
	| ret <> 0 = abortLmdbError "deleteAllMaps" ret "deleteAllSubDbs returned an error"
	= deleteAllPartDbs txn
where
	deleteAllPartDbs :: !*Txn -> *Txn
	deleteAllPartDbs txn = accUnsafe
		(\world
			# (world, txn) = foldedPartitions forPartition world txn
			= (txn, world))
	where
		forPartition :: !String !*World !*Txn -> (!*World, !*Txn)
		forPartition partName world txn = case 'Map'.get partName partDbs of
			// If the partition DB is open, it is more efficient to keep it open and remove its content.
			?Just (PartDb env) = (deleteAllMapsPartDb env world, txn)
			?None = (world, deletePartition partName txn)

	deleteAllMapsPartDb :: !Int !*World -> *World
	deleteAllMapsPartDb env world
		# (ptr, world) = startTransactionPtr False False env world
		# (ret, world) = deleteAllSubDbs dropDbis ptr world
		| ret <> 0 = abortLmdbError "deleteAllMapsPartDb" ret "deleteAllSubDbs returned an error"
		= commitTxn ptr world

deleteEmptyMaps :: !*CDB -> *CDB
deleteEmptyMaps cdb = snd $ doTransaction deleteEmptyMapsTxn cdb
where
	// This transaction is only safe if the database has not been modified in the same transaction, as some dbis are
	// dropped and closed. See the documentation of `mdb_dbi_close` for the caveats of doing this.
	deleteEmptyMapsTxn :: !*Txn -> ((), !*Txn)
	deleteEmptyMapsTxn txn=:(Txn {ptr})
		# (ret, txn) = deleteEmptyMapsC ptr txn
		| ret <> 0 = abortLmdbError "deleteAllMaps" ret "deleteAllSubDbs returned an error"
		= ((), txn)

startTransaction :: !*CDB -> *Txn
startTransaction db = Txn (startTransaction` False Unconstrained db)

startReadOnlyTransaction :: !*CDB -> *ReadOnlyTxn
startReadOnlyTransaction db = ReadOnlyTxn (startTransaction` True Unconstrained db)

startTransaction` :: !Bool !PartitionConstraint !*CDB -> *TxnInfo
startTransaction`
	isReadOnly partConstr db=:{env, syncMode, readaheadPartitionDatabases, maxNumberPagesPartitionDatabases}
	# (ptr, {path, compactionFactor, processPool, getCondAndPassResultsProcess, partDbs}) = case partConstr of
		OnlyPartition _ = (0, db)
		_ = startTransactionPtr False isReadOnly env db
	=	{ ptr = ptr, dbEnv = env, dbis = 'Map'.newMap, getCondAndPassResultsProcess = getCondAndPassResultsProcess
		, processPool = processPool, compactionFactor = compactionFactor, partDbs = partDbs, partDbTxns = 'Map'.newMap
		, isReadOnly = isReadOnly, dbPath = path, syncMode = syncMode
		, readaheadPartitionDatabases = readaheadPartitionDatabases
		, maxNumberPagesPartitionDatabases = maxNumberPagesPartitionDatabases, partitionConstraint = partConstr}

startTransactionPtr :: !Bool !Bool !Pointer !*env -> (!Pointer, !*env)
startTransactionPtr retry isReadOnly env world
	// Check for stale readers. Should be done before write transactions to prevent that free pages are not reused
	// because of open read-transactions from dead processes. Is also done on retry, in case no reader slot was
	// available, in the hope that a slot is available after freeing slots of state readers.
	# (ret, world) = if (isReadOnly && not retry) (0, world) (mdb_reader_check env 0 world)
	| ret <> 0 = abortLmdbError "startTransaction`" ret "mdb_reader_check returned an error"
	// open transaction
	# (ret, world) = mdb_txn_begin env 0 (if isReadOnly MDB_RDONLY 0) tmpArr world
	| not retry && ret == MDB_READERS_FULL = startTransactionPtr True isReadOnly env world
	| ret <> 0 = abortLmdbError "startTransaction`" ret "mdb_txn_begin returned an error"
	= (tmpArr.[0], world)

endTransaction :: !*txn -> *CDB | readTxn txn
endTransaction txn
	# (ptr, info) = (infoOf txn)!ptr
	# (env, info) = info!dbEnv
	// `ptr` is `0` if transaction is restricted to a partition DB, so we only have to commit the txn for the partition.
	# info = if (ptr <> 0) (commitTxn ptr info) info
	# (partDbTxns, info) = info!TxnInfo.partDbTxns
	# {compactionFactor, getCondAndPassResultsProcess, processPool, dbPath, partDbs, syncMode
		, readaheadPartitionDatabases, maxNumberPagesPartitionDatabases} =
		'Map'.foldlWithKey` (\info _ (PartDbTxn ptr) = commitTxn ptr info) info partDbTxns
	=	{ path = dbPath, env = env, compactionFactor = compactionFactor
		, getCondAndPassResultsProcess = getCondAndPassResultsProcess, processPool = processPool, partDbs = partDbs
		, syncMode = syncMode, readaheadPartitionDatabases = readaheadPartitionDatabases
		, maxNumberPagesPartitionDatabases = maxNumberPagesPartitionDatabases}

commitTxn :: !Pointer !*env -> *env
commitTxn ptr env
	# (ret, env) = mdb_txn_commit ptr env
	| ret <> 0 = abortLmdbError "commitTxn" ret "mdb_txn_commit returned an error"
	= env

mapInfoOf :: !(dbMap k v) !*TxnInfo -> (!?MapInfo, !*TxnInfo) | dbMap dbMap
mapInfoOf
	map
	info=:
		{ ptr, partDbTxns, partDbs, isReadOnly, dbPath, syncMode, readaheadPartitionDatabases
		, maxNumberPagesPartitionDatabases}
	# common=:{name, partName} = commonOf map
	= case partName of
		?None = mapInfoOf` name partName map ptr info
		?Just partName`
			# (ptr, info) = case 'Map'.get partName` partDbTxns of
				?Just partTxn = (?Just partTxn, info)
				?None
					# (ptr, info) = case 'Map'.get partName` partDbs of
						?Just (PartDb env) = appFst ?Just $ startTransactionPtr False isReadOnly env info
						?None
							# path = dbPath </> partName`
							# (ret, info) = if isReadOnly (access (packString path) F_OK info) (0, info)
							| ret <> 0 = (?None, info)
							# env = accUnsafe $ openDatabasePtr
								path syncMode readaheadPartitionDatabases maxNumberPagesPartitionDatabases
							# (ptr, info) = startTransactionPtr False isReadOnly env info
							= (?Just ptr, {TxnInfo| info & partDbs = 'Map'.put partName` (PartDb env) partDbs})
					= case ptr of
						?Just ptr =
							( ?Just $ PartDbTxn ptr
							, {info & partDbTxns = 'Map'.put partName` (PartDbTxn ptr) partDbTxns})
						?None = (?None, info)
			= case ptr of
				?None = (?None, info)
				?Just (PartDbTxn ptr) = mapInfoOf` name partName map ptr info
where
	mapInfoOf` :: !String !(?String) !(dbMap k v) !Int !*TxnInfo -> (!?MapInfo, !*TxnInfo) | dbMap dbMap
	mapInfoOf` name partName map ptr info=:{dbis, partitionConstraint}
		= case partitionConstraint of
			OnlyMainDatabase | isJust partName = abortError
				"mapInfoOf" (Error "operation on any partition database excluded by constraint")
				("attempt to use partition " +++ fromJust partName)
			OnlyPartition part | ?Just part <> partName = abortError
				"mapInfoOf"
				(Error $
					"operation on " +++  maybe "main database" (\p = "partition " +++ p) partName +++
					" excluded by constraint")
				("allowed partition is " +++ part)
			_ = case 'Map'.get dbisKey dbis of
				?Just dbi = (?Just dbi, info)
				?None
					// create dbi -> open database
					# (ret, info) = mdb_dbi_open ptr (packString $ name +++ "$A") flags tmpArr info
					| ret == EACCES = (?None, info) // the sub-database does not exist and we can't create it
					| ret <> 0 = abortLmdbErrorOnMap "mapInfoOf" map ret "mdb_dbi_open returned an error ($A)"
					# dbiA = tmpArr.[0]
					# info = forceEval dbiA info
					# (ret, info) = mdb_dbi_open ptr (packString $ name +++ "$B") flags tmpArr info
					| ret <> 0 = abortLmdbErrorOnMap "mapInfoOf" map ret "mdb_dbi_open returned an error ($B)"
					# dbiB = tmpArr.[0]
					# info = forceEval dbiB info
					# isMultiMap = isJust valSizeOfMultiMap
					// Determine which map is the current high/low map.
					# (curA, info) = openCursor dbiA ptr info
					# (curB, info) = openCursor dbiB ptr info
					# (keyA, info) = getFirstKey curA info
					# (keyB, info) = getFirstKey curB info
					// For rw-transaction the cursors are closed by `moveFromHighToLowForAllMaps`.
					# info = if isReadOnly (mdb_cursor_close curB $ mdb_cursor_close curA info) info
					# mapInfo =
						if (keyA < keyB)
							{ highMap = dbiB, highCursor = curB, lowMap = dbiA, lowCursor = curA
							, lowestKeyInHighMap = keyB, nrWrites = 0, largeValueMap = ?None, isMultiMap = isMultiMap
							, txnPtr = ptr}
							{ highMap = dbiA, highCursor = curA, lowMap = dbiB, lowCursor = curB
							, lowestKeyInHighMap = keyA, nrWrites = 0, largeValueMap = ?None, isMultiMap = isMultiMap
							, txnPtr = ptr}
					= (?Just mapInfo, {TxnInfo| info & dbis = 'Map'.put dbisKey mapInfo dbis})
	where
		dbisKey = (name, partName)
		valSizeOfMultiMap = valueSizeOfMultiMap map
		flags = MDB_CREATE bitor multiMapFlags

		multiMapFlags = case valSizeOfMultiMap of
			?None = 0
			?Just size =
					MDB_DUPSORT
				bitor
					(case size of
						Dynamic = 0
						Fixed   = MDB_DUPFIXED
						Integer = MDB_DUPFIXED bitor MDB_INTEGERDUP)

//* Updates the lowest key in the high map in the adminitration if this may not be up-to-date anymore,
//* e.g. after a delete operation.
updateLowestKeyInHighMapOf :: !(dbMap k v) !*TxnInfo -> *TxnInfo | dbMap dbMap
updateLowestKeyInHighMapOf map info
	# (?Just mapInfo=:{highCursor}, info) = mapInfoOf map info
	# (lowestInHigh, info=:{TxnInfo| dbis}) = getFirstKey highCursor info
	= {TxnInfo| info & dbis = 'Map'.put (name, partName) {mapInfo & lowestKeyInHighMap = lowestInHigh} dbis}
where
	{name, partName} = commonOf map

getFirstKey :: !Pointer !TxnInfo -> (!?String, !TxnInfo)
getFirstKey cur info
	# (ret, keyStr, info) = mdbCursorGetReadKey cur "" valVal MDB_FIRST info
	# (keyStr, info) = if (ret == MDB_NOTFOUND) (?None, info) (?Just keyStr, info)
	= (keyStr, info)

openCursor :: !Int !Int !*TxnInfo -> (!Pointer, !*TxnInfo)
openCursor dbi ptr info
	# (ret, info) = mdb_cursor_open ptr dbi tmpArr info
	| ret <> 0 = abortLmdbError "openCursor" ret "mdb_cursor_open returned an error"
	= (tmpArr.[0], info)

instance primitiveKey String where
	keyLength       = DynamicLength ?None
	nextLargerKey k = ?Just $ k +++ "\0"
	smallestKey     = "\0"

instance key String
where
	strRepresentation k = KeyStr k
	fromStrRepresentation (KeyStr str) = str
	hasFixedSize _ = False

instance primitiveKey UTF8 where
	keyLength = DynamicLength ?None
	nextLargerKey u0 = utf8StringCorrespondingTo (toString u0 +++ "\0")
	smallestKey = fromJust $ utf8StringCorrespondingTo "\0"

instance key UTF8
where
	strRepresentation u0 = KeyStr (toString u0)
	fromStrRepresentation k = unsafeCoerce k
	hasFixedSize _ = False

instance primitiveKey Int where
	keyLength = DynamicLength $ ?Just lengthOf
	where
		// The length can be derived from the first 5 bits of the key string's header byte (see documentation of `instance key Int`).
		lengthOf :: !String -> Int
		lengthOf str = if (l < 8) (8 - l) (l - 7) + if extraHeaderBitsUsed 0 1
		where
			extraHeaderBitsUsed = (0x8 bitand headerByte) == 0
			l = headerByte >> 4
			headerByte = toInt str.[0]

	nextLargerKey i
		| i == LargestInt = ?None
		| otherwise       = ?Just $ inc i

	smallestKey = SmallestInt

/**
 * The encoding uses a dynamic number of bytes and is most space efficient for integers close to `0`.
 *
 * The first byte of the string representation is a header byte, which is always present.
 * The first 4 bits of the header byte indicate whether the number is negative or positive and
 * encode the number of bytes required to encode the number.
 * If the first 4 bits of the header byte have a value=x of 0 <= x <= 7 this indicates that the number is negative and has a length of 8-x bytes. Otherwise,
 * if the first 4 bits of the header byte have a value=y of 8 <= y <= 15 this indicates that the number is positive and has a length of y-8 bytes.
 * This representation at the beginning of the header byte makes sure that the string representation preserves the order of the original numbers.
 *
 * If the most significant byte after the header is representable by 3 bits,
 * they are stored in the last 3 bits of the header. This is done to increase space efficiency.
 * Bit 5 of the header byte is set if the last 3 bits of the header byte are unused, in which case they are all `0`.
 *
 * The bytes after the header represent the absolute value of the number, of which `1` is subtracted for negative numbers.
 * `-1` is therefore represented by `0`.
 * This can be distinguished from the representation of `0` by the information on whether the number is negative or positive contained within the header byte.
 * In order to preserve the order of the string representation, bytes of negative numbers are inverted.
 * All `0` bytes at the beginning are omitted, which makes the representation compact.
 *
 * `0` and `-1` are special cases, as they are both represented by `0`, so all bytes could be omitted.
 * The length in the header is still `1`.
 * `0` is represented by the header byte only, using the final 3 bits in the header byte instead of the most significicant byte following the header byte (see above).
 * This is not possible for `-1`, as the order of the string representation would not be preserved. `-1` is therefore encoded using 2 bytes.
 */
instance key Int
where
	strRepresentation :: !Int -> KeyStr Int
	strRepresentation 0  = KeyStr "\x80"     // special case for 0 (see above)
	strRepresentation -1 = KeyStr "\x78\xff" // specail case for -1 (see above)
	strRepresentation i = KeyStr {b \\ b <|- [#fromInt headerByte: bytes`!]}
	where
		(headerByte, bytes`) =
			case bytes of
				// first byte fits into the extra bit of the header
				[#fstByte: bytes!] | fstByte < '\x08' = (headerByteLength bitor (toInt fstByte), bytes)
				// first byte does not fit into header; set the usage bit (5th bit) of the header byte to indicate that extra bits are unused (see above)
				_                                     = (headerByteLength bitor 0x08,            bytes)

		// header byte with extra bits all 0 and usage bit (5th bit) unset
		headerByteLength = if isNegative (8 - nrBytes) (nrBytes + 7) << 4

		(bytes, nrBytes) = bytesFor (if isNegative ((abs i) - 1) i) [#!] 0

		bytesFor :: !Int ![#Char!] !Int -> (![#Char!], !Int)
		bytesFor 0 bytes nr = (bytes, nr)
		bytesFor i bytes nr = bytesFor (i >> 8) [#fromInt byte: bytes!] (inc nr)
		where
			// invert bytes for negative numbers
			byte = if isNegative (255 - i) i

		isNegative = i < 0

	fromStrRepresentation :: !(KeyStr Int) -> Int
	fromStrRepresentation (KeyStr str)
		| isNegative = ~absInteger - 1
		| otherwise  = absInteger
	where
		absInteger = integerFor initRes 1
		where
			initRes
				| extraHeaderBitsUsed = byteFromExtraBits bitxor invByte
				| otherwise           = 0
			byteFromExtraBits = (toInt str.[0]) bitand 7

		integerFor :: !Int !Int -> Int
		integerFor result curByteIdx
			| curByteIdx > nrBytes = result
			| otherwise =
				integerFor (result << 8 + (byte bitxor invByte)) (inc curByteIdx)
		where
			byte = fromChar str.[curByteIdx]

		nrBytes = size str - 1
		extraHeaderBitsUsed = (0x8 bitand toInt str.[0]) == 0
		isNegative = str.[0] < '\x80'
		invByte = if isNegative 0xff 0

	hasFixedSize _ = False

instance primitiveKey Bool where
	keyLength = FixedLength 1

	nextLargerKey True  = ?None
	nextLargerKey False = ?Just True

	smallestKey = False

instance key Bool where
	strRepresentation     b               = KeyStr {if b '\1' '\0'}
	fromStrRepresentation (KeyStr keyStr) = keyStr.[0] == '\1'
	hasFixedSize _ = True

instance primitiveKey Char where
	keyLength = FixedLength 1

	nextLargerKey :: !Char -> ?Char
	nextLargerKey c
		| c == '\xff' = ?None
		| otherwise   = ?Just $ inc c

	smallestKey = '\0'

instance key Char where
	strRepresentation     c               = KeyStr {c}
	fromStrRepresentation (KeyStr keyStr) = keyStr.[0]
	hasFixedSize _ = True

instance primitiveKey (?a) | primitiveKey a where
	keyLength = keyLengthFor keyLength
	where
		keyLengthFor :: !(KeyLength a) -> KeyLength (?a)
		keyLengthFor (FixedLength valLength) =
			DynamicLength $ ?Just \keyStr = let firstChar = keyStr.[0] in if (firstChar == '\0') 1 (valLength + 1)
		keyLengthFor (DynamicLength lengthFor) =
			DynamicLength $
				maybe
					?None
					(\lengthFor = ?Just
						\keyStr
							# firstChar = keyStr.[0]
							= if (firstChar == '\0') 1 (lengthFor (keyStr % (1, size keyStr - 1)) + 1)
					)
					lengthFor

	nextLargerKey ?None       = ?Just $ ?Just smallestKey
	nextLargerKey (?Just val) = ?Just <$> nextLargerKey val

	smallestKey = ?None

instance key (?a) | key a where
	strRepresentation ?None       = KeyStr "\0"
	strRepresentation (?Just val) = KeyStr ("\1" +++ valStr)
	where
		(KeyStr valStr) = strRepresentation val

	fromStrRepresentation (KeyStr keyStr)
		| firstChar == '\0' = ?None
		| otherwise         = ?Just $ fromStrRepresentation $ KeyStr (keyStr % (1, size keyStr - 1))
	where
		firstChar = keyStr.[0]

	hasFixedSize _ = False

instance primitiveKey () where
	keyLength       = FixedLength 1
	nextLargerKey _ = ?None
	smallestKey     = ()

instance key () where
	strRepresentation     _ = KeyStr "\0"
	fromStrRepresentation _ = ()
	hasFixedSize _ = True

instance key (!k1, !k2) | primitiveKey k1 & primitiveKey k2
where
	strRepresentation :: !(!k1, !k2) -> KeyStr (!k1, !k2) | primitiveKey k1 & primitiveKey k2
	strRepresentation (key1, key2) = joinedKeyStr (strRepresentation key1) (strRepresentation key2)

	fromStrRepresentation :: !(KeyStr (!k1, !k2)) -> (!k1, !k2) | primitiveKey k1 & primitiveKey k2
	fromStrRepresentation keyStr = (fromStrRepresentation keyStr1, fromStrRepresentation keyStr2)
	where
		(keyStr1, keyStr2) = splitKeyStr keyStr

	hasFixedSize t = hasFixedSize1 t undef && hasFixedSize2 t undef
	where
		hasFixedSize1 :: (k1, k2) k1 -> Bool | key k1
		hasFixedSize1 _ v1 = hasFixedSize v1

		hasFixedSize2 :: (k1, k2) k2 -> Bool | key k2
		hasFixedSize2 _ v2 = hasFixedSize v2

instance key (!k1, !k2, !k3) | primitiveKey k1 & primitiveKey k2 & primitiveKey k3
where
	strRepresentation :: !(!k1, !k2, !k3) -> KeyStr (!k1, !k2, !k3)
		| primitiveKey k1 & primitiveKey k2 & primitiveKey k3
	strRepresentation (key1, key2, key3) = unsafeCoerce $ strRepresentation ((key1, key2), key3)

	fromStrRepresentation :: !(KeyStr (!k1, !k2, !k3)) -> (!k1, !k2, !k3)
		| primitiveKey k1 & primitiveKey k2 & primitiveKey k3
	fromStrRepresentation keyStr = (k1, k2, fromStrRepresentation keyStr3)
	where
		(keyStr12, keyStr3) = splitKeyStr $ unsafeCoerce keyStr
		(k1, k2)            = fromStrRepresentation keyStr12

	hasFixedSize t = hasFixedSize1 t undef && hasFixedSize2 t undef && hasFixedSize3 t undef
	where
		hasFixedSize1 :: (k1, k2, k3) k1 -> Bool | key k1
		hasFixedSize1 _ v1 = hasFixedSize v1

		hasFixedSize2 :: (k1, k2, k3) k2 -> Bool | key k2
		hasFixedSize2 _ v2 = hasFixedSize v2

		hasFixedSize3 :: (k1, k2, k3) k3 -> Bool | key k3
		hasFixedSize3 _ v3 = hasFixedSize v3

instance key (!k1, !k2, !k3, !k4) | primitiveKey k1 & primitiveKey k2 & primitiveKey k3 & primitiveKey k4
where
	strRepresentation :: !(!k1, !k2, !k3, !k4) -> KeyStr (!k1, !k2, !k3, !k4)
		| primitiveKey k1 & primitiveKey k2 & primitiveKey k3 & primitiveKey k4
	strRepresentation (key1, key2, key3, key4) = unsafeCoerce $ strRepresentation (((key1, key2), key3), key4)
	fromStrRepresentation :: !(KeyStr (!k1, !k2, !k3, !k4)) -> (!k1, !k2, !k3, !k4)
		| primitiveKey k1 & primitiveKey k2 & primitiveKey k3 & primitiveKey k4
	fromStrRepresentation keyStr = (k1, k2, k3, fromStrRepresentation keyStr4)
	where
		(keyStr123, keyStr4) = splitKeyStr $ unsafeCoerce keyStr
		((k1, k2), k3)       = fromStrRepresentation keyStr123

	hasFixedSize t = hasFixedSize1 t undef && hasFixedSize2 t undef && hasFixedSize3 t undef && hasFixedSize4 t undef
	where
		hasFixedSize1 :: (k1, k2, k3, k4) k1 -> Bool | key k1
		hasFixedSize1 _ v1 = hasFixedSize v1

		hasFixedSize2 :: (k1, k2, k3, k4) k2 -> Bool | key k2
		hasFixedSize2 _ v2 = hasFixedSize v2

		hasFixedSize3 :: (k1, k2, k3, k4) k3 -> Bool | key k3
		hasFixedSize3 _ v3 = hasFixedSize v3

		hasFixedSize4 :: (k1, k2, k3, k4) k4 -> Bool | key k4
		hasFixedSize4 _ v4 = hasFixedSize v4

instance key (!k1, !k2, !k3, !k4, !k5)
	| primitiveKey k1 & primitiveKey k2 & primitiveKey k3 & primitiveKey k4 & primitiveKey k5
where
	strRepresentation :: !(!k1, !k2, !k3, !k4, !k5) -> KeyStr (!k1, !k2, !k3, !k4, !k5)
		| primitiveKey k1 & primitiveKey k2 & primitiveKey k3 & primitiveKey k4 & primitiveKey k5
	strRepresentation (key1, key2, key3, key4, key5) =
		unsafeCoerce $ strRepresentation ((((key1, key2), key3), key4), key5)

	fromStrRepresentation :: !(KeyStr (!k1, !k2, !k3, !k4, !k5)) -> (!k1, !k2, !k3, !k4, !k5)
		| primitiveKey k1 & primitiveKey k2 & primitiveKey k3 & primitiveKey k4 & primitiveKey k5
	fromStrRepresentation keyStr = (k1, k2, k3, k4, fromStrRepresentation keyStr5)
	where
		(keyStr1234, keyStr5) = splitKeyStr $ unsafeCoerce keyStr
		(((k1, k2), k3), k4)  = fromStrRepresentation keyStr1234

	hasFixedSize t =
		hasFixedSize1 t undef && hasFixedSize2 t undef && hasFixedSize3 t undef && hasFixedSize4 t undef &&
		hasFixedSize5 t undef
	where
		hasFixedSize1 :: (k1, k2, k3, k4, k5) k1 -> Bool | key k1
		hasFixedSize1 _ v1 = hasFixedSize v1

		hasFixedSize2 :: (k1, k2, k3, k4, k5) k2 -> Bool | key k2
		hasFixedSize2 _ v2 = hasFixedSize v2

		hasFixedSize3 :: (k1, k2, k3, k4, k5) k3 -> Bool | key k3
		hasFixedSize3 _ v3 = hasFixedSize v3

		hasFixedSize4 :: (k1, k2, k3, k4, k5) k4 -> Bool | key k4
		hasFixedSize4 _ v4 = hasFixedSize v4

		hasFixedSize5 :: (k1, k2, k3, k4, k5) k5 -> Bool | key k5
		hasFixedSize5 _ v5 = hasFixedSize v5

instance key (!k1, !k2, !k3, !k4, !k5, !k6)
	| primitiveKey k1 & primitiveKey k2 & primitiveKey k3 & primitiveKey k4 & primitiveKey k5 & primitiveKey k6
where
	strRepresentation :: !(!k1, !k2, !k3, !k4, !k5, !k6) -> KeyStr (!k1, !k2, !k3, !k4, !k5, !k6)
		| primitiveKey k1 & primitiveKey k2 & primitiveKey k3 & primitiveKey k4 & primitiveKey k5 & primitiveKey k6
	strRepresentation (key1, key2, key3, key4, key5, key6) =
		unsafeCoerce $ strRepresentation (((((key1, key2), key3), key4), key5), key6)

	fromStrRepresentation :: !(KeyStr (!k1, !k2, !k3, !k4, !k5, !k6)) -> (!k1, !k2, !k3, !k4, !k5, !k6)
		| primitiveKey k1 & primitiveKey k2 & primitiveKey k3 & primitiveKey k4 & primitiveKey k5 & primitiveKey k6
	fromStrRepresentation keyStr = (k1, k2, k3, k4, k5, fromStrRepresentation keyStr6)
	where
		(keyStr12345, keyStr6)     = splitKeyStr $ unsafeCoerce keyStr
		((((k1, k2), k3), k4), k5) = fromStrRepresentation keyStr12345

	hasFixedSize t =
		hasFixedSize1 t undef && hasFixedSize2 t undef && hasFixedSize3 t undef && hasFixedSize4 t undef &&
		hasFixedSize5 t undef && hasFixedSize6 t undef
	where
		hasFixedSize1 :: (k1, k2, k3, k4, k5, k6) k1 -> Bool | key k1
		hasFixedSize1 _ v1 = hasFixedSize v1

		hasFixedSize2 :: (k1, k2, k3, k4, k5, k6) k2 -> Bool | key k2
		hasFixedSize2 _ v2 = hasFixedSize v2

		hasFixedSize3 :: (k1, k2, k3, k4, k5, k6) k3 -> Bool | key k3
		hasFixedSize3 _ v3 = hasFixedSize v3

		hasFixedSize4 :: (k1, k2, k3, k4, k5, k6) k4 -> Bool | key k4
		hasFixedSize4 _ v4 = hasFixedSize v4

		hasFixedSize5 :: (k1, k2, k3, k4, k5, k6) k5 -> Bool | key k5
		hasFixedSize5 _ v5 = hasFixedSize v5

		hasFixedSize6 :: (k1, k2, k3, k4, k5, k6) k6 -> Bool | key k6
		hasFixedSize6 _ v6 = hasFixedSize v6

all :: Condition k
all =: conditionWithDefaultOrder [!statelessPred NextKey (const (True, NextKey))!]

none :: Condition k
none =: conditionWithDefaultOrder [!!]

smallest :: Condition k
smallest =: conditionWithDefaultOrder [!statelessPred NextKey (const (True, const Stop))!]

equals :: !k -> Condition k | key k
equals key =
	conditionWithDefaultOrder
		[! GivenKey
			{ targetKey       = condKeyStr
			, keyMayBeSkipped = True
			, usePreviousKey  = ?None
			, targetKeyPred   = Pred \keyStr _ -> {holds = keyStr == condKeyStr, continuation = Stop}
			}
		!]
where
	condKeyStr = strRepresentation key

largestLessThanOrEq :: !k -> Condition k | key k
largestLessThanOrEq key =
	conditionWithDefaultOrder
		[! statelessPred
			( \pred ->
				GivenKey
					{ targetKey      = keyStr
					, keyMayBeSkipped = False
					, usePreviousKey =
						?Just $ maybe contIfNoPrevKey (\keyStr` -> if (keyStr` == keyStr) ?None contIfNoPrevKey)
					, targetKeyPred  = pred
					}
			)
			(\_ -> (True, \_ -> Stop))
		!]
where
	keyStr = strRepresentation key
	contIfNoPrevKey = ?Just $ Stop

smallestGreaterThanOrEq :: !k -> Condition k | key k
smallestGreaterThanOrEq key =
	conditionWithDefaultOrder
		[! statelessPred
			(\pred ->
				GivenKey
					{ targetKey       = strRepresentation key
					, keyMayBeSkipped = False
					, usePreviousKey  = ?None
					, targetKeyPred   = pred
					}
			)
			(\_ -> (True, const Stop))
		!]

lessThan :: !k -> Condition k | key k
lessThan val  =
	conditionWithDefaultOrder
		[!statelessPred NextKey (\keyStr -> if (keyStr <  strRepresentation val) (True, NextKey) (False, const Stop))!]

lessThanOrEqual :: !k -> Condition k | key k
lessThanOrEqual val =
	conditionWithDefaultOrder
		[!statelessPred NextKey (\keyStr -> if (keyStr <= strRepresentation val) (True, NextKey) (False, const Stop))!]

greaterThan :: !k -> Condition k | primitiveKey k
greaterThan val =
	case strRepresentation <$> nextLargerKey val of
		?Just valStr =
			conditionWithDefaultOrder
				[! statelessPred
					(\pred ->
						GivenKey
							{targetKey        = valStr
							, keyMayBeSkipped = True
							, usePreviousKey  = ?None
							, targetKeyPred   = pred
							}
					)
					(\_ -> (True, NextKey))
				!]
		?None =
			none

greaterThanOrEqual :: !k -> Condition k | key k
greaterThanOrEqual val =
	conditionWithDefaultOrder
		[! statelessPred
			(\pred ->
				GivenKey
					{ targetKey       = strRepresentation val
					, keyMayBeSkipped = True
					, usePreviousKey  = ?None
					, targetKeyPred   = pred
					}
			)
			(const (True, NextKey))
		!]

isBetween :: !k !k -> Condition k | key k
isBetween first last = conditionWithDefaultOrder [!conditionFor first last!]
where
	conditionFor :: !k !k -> PredContinuation k | key k
	conditionFor first last
		| firstKey <= lastKey =
			statelessPred
				(\pred ->
					GivenKey
						{ targetKey       = firstKey
						, keyMayBeSkipped = True
						, usePreviousKey  = ?None
						, targetKeyPred   = pred
						}
				)
				pred
		| otherwise =
			Stop
	where
		firstKey = strRepresentation first
		lastKey  = strRepresentation last

		pred keyStr
			| keyStr == lastKey = (True,  const Stop)
			| keyStr <  lastKey = (True,  NextKey)
			| otherwise         = (False, const Stop)

isBetweenOptional :: !(?k) !(?k) -> Condition k | key k
isBetweenOptional ?None         ?None         = all
isBetweenOptional ?None         (?Just upper) = lessThanOrEqual upper
isBetweenOptional (?Just lower) ?None         = greaterThanOrEqual lower
isBetweenOptional (?Just lower) (?Just upper) = isBetween lower upper

isIn :: !(Set k) -> Condition k | key k
isIn keySet = conditionWithDefaultOrder [!conditionFor keySet!]
where
	conditionFor :: !(Set k) -> PredContinuation k | key k
	conditionFor keySet
		| 'Set'.null keySet =
			Stop
		| otherwise
			# keysToFind=:[!fstKey: _] = 'Set'.toList keySet
			= statefulPred
				( \pred ->
					GivenKey
						{ targetKey       = strRepresentation fstKey
						, keyMayBeSkipped = True
						, usePreviousKey  = ?None
						, targetKeyPred   = pred
						}
				)
				pred
				keysToFind
	where
		pred :: !(KeyStr k) ![!k] -> (!Bool, !(Pred k) -> PredContinuation k, ![!k]) | key k
		pred keyStr restKeys=:[!] = (False, \_ -> Stop, restKeys)
		pred keyStr keysToFind=:[!nextKey: restKeys]
			| keyStr == nextKeyStr =
				(True, nextKeyFor restKeys, restKeys)
			| keyStr <  nextKeyStr =
				( False
				, \pred ->
					GivenKey
						{targetKey        = strRepresentation nextKey
						, keyMayBeSkipped = True
						, usePreviousKey  = ?None
						, targetKeyPred   = pred
						}
				, keysToFind
				)
			| otherwise =
				pred keyStr restKeys
		where
			nextKeyStr = strRepresentation nextKey

		nextKeyFor :: ![!k] !(Pred k) -> PredContinuation k | key k
		nextKeyFor [!] pred = Stop
		nextKeyFor [!nextKey: _] pred =
			GivenKey
				{ targetKey       = strRepresentation nextKey
				, keyMayBeSkipped = True
				, usePreviousKey  = ?None
				, targetKeyPred   = pred
				}

(OR) infixr 2 :: !(Condition k) !(Condition k) -> Condition k
(OR) {Condition| continuations = conts1} {Condition| continuations = conts2} =
	conditionWithDefaultOrder $ conts1 ++$ conts2

disjunctionOf :: ![!Condition k!] -> Condition k
disjunctionOf conditions =
	conditionWithDefaultOrder $ Flatten [!continuations \\ {Condition| continuations} <|- conditions!]

disjunctionOfWithCustomOrder :: !(k k -> Bool) ![!Condition k!] -> Condition k
disjunctionOfWithCustomOrder isLessThan conditions =
	{ Condition
	| continuations =
		Flatten [!continuations \\ {Condition| continuations} <|- conditions!], isLessThan = ?Just isLessThan
	}

>& :: !(Condition k1) !(Condition k2) -> Condition (k1, k2) | primitiveKey k1 & primitiveKey k2
>& {Condition| continuations = conts1} {Condition| continuations = conts2} =
	conditionWithDefaultOrder $ [!unsafeCoerce $ combinedCondition cont1 cont2 \\ cont1 <|- conts1, cont2 <|- conts2!]
where
	combinedCondition ::
		!(PredContinuation k1) !(PredContinuation k2) -> PredContinuation (!k1, !k2) | primitiveKey k1 & primitiveKey k2
	combinedCondition initCont1 initCont2 = if (initCont2 =: Stop) Stop (combinedCont1 ?None initCont2 initCont1)
	where
		combinedCont1 ::
			!(?(KeyStr k1)) !(PredContinuation k2) !(PredContinuation k1) -> PredContinuation (!k1, !k2)
			| primitiveKey k1 & primitiveKey k2
		combinedCont1 currentKey1 initCont2 (NextKey pred1) =
			case currentKey1 of
				// We are at the beginning so goto first key.
				?None -> NextKey $ combinedPred1 pred1 initCont2
				// goto next keyStr1, jumping over all `(currentKeyStr1, someKeyStr2)` in between
				?Just currentKey1 ->
					case incrementedKeyStr currentKey1 of
						?None ->
							Stop
						?Just nextKeyStr1 ->
							GivenKey
								{ targetKey       = partialKeyStr nextKeyStr1
								, keyMayBeSkipped = False
								, usePreviousKey  = ?None
								, targetKeyPred   = combinedPred1 pred1 initCont2
								}
		combinedCont1 _ initCont2 (GivenKey {targetKey, keyMayBeSkipped, usePreviousKey, targetKeyPred}) =
			GivenKey
				{ targetKey =
					if keyMayBeSkipped
						(case initCont2 of
							GivenKey {targetKey = targetKey2, usePreviousKey = usePreviousKey2}
								| isNone usePreviousKey2 = joinedKeyStr targetKey targetKey2
							_ = partialKeyStr targetKey
						)
						(partialKeyStr targetKey)
				, keyMayBeSkipped = keyMayBeSkipped && initCont2 =: (GivenKey {keyMayBeSkipped = True})
				, usePreviousKey =
					(\usePreviousKey keyStr ->
						(\cont1 -> combinedCont1 ?None initCont2 cont1) <$>
							(usePreviousKey $ (fst o splitKeyStr) <$> keyStr)
					) <$>
						usePreviousKey
				, targetKeyPred = combinedPred1 targetKeyPred initCont2
				}
		combinedCont1 _ _ Stop = Stop

		combinedPred1 :: !(Pred k1) !(PredContinuation k2) -> Pred (!k1, !k2) | primitiveKey k1 & primitiveKey k2
		combinedPred1 pred1 initCont2 = Pred (pred pred1 initCont2)
		where
			pred ::
				!(Pred k1) !(PredContinuation k2) !(KeyStr (!k1, !k2)) !Bool -> PredResult (k1, k2)
				| primitiveKey k1 & primitiveKey k2
			pred pred1 initCont2 keyStr lastMoveWasBackwards =
				combinedPredResult1 lastMoveWasBackwards pred1 keyStr1 keyStr2 initCont2
			where
				(keyStr1, keyStr2) = splitKeyStr keyStr

		combinedPredResult1 ::
			!Bool !(Pred k1) !(KeyStr k1) !(KeyStr k2) !(PredContinuation k2) -> PredResult (k1, k2)
			| primitiveKey k1 & primitiveKey k2
		combinedPredResult1 lastMoveWasBackwards (Pred pred1) keyStr1 keyStr2 initCont2
			# res1 = pred1 keyStr1 lastMoveWasBackwards
			| res1.holds
				// go back to first element with current keyStr1
				| lastMoveWasBackwards =
					{ holds = False
					, continuation =
						GivenKey
							{ targetKey       = partialKeyStr keyStr1
							, keyMayBeSkipped = False
							, usePreviousKey  = ?None
							, targetKeyPred   = combinedPred1 (Pred pred1) initCont2
							}
					}
				| otherwise =
					case initCont2 of
						NextKey pred2 =
							combinedPredResult2
								lastMoveWasBackwards res1.PredResult.continuation pred2 keyStr1 keyStr2 initCont2
						GivenKey {targetKey, targetKeyPred}
							| keyStr2 == targetKey =
								combinedPredResult2
									lastMoveWasBackwards
									res1.PredResult.continuation
									targetKeyPred
									keyStr1
									keyStr2
									initCont2
							| otherwise =
								{ holds        = False
								, continuation =
									combinedCont2
										lastMoveWasBackwards res1.PredResult.continuation keyStr1 initCont2 initCont2
								}
			| otherwise =
				{holds = False, continuation = combinedCont1 (?Just keyStr1) initCont2 res1.PredResult.continuation}

		combinedPredResultForCont1 ::
			!Bool !(PredContinuation k1) !(KeyStr k1) !(KeyStr k1) !(KeyStr k2) !(PredContinuation k2)
			-> PredResult (k1, k2) | primitiveKey k1 & primitiveKey k2
		combinedPredResultForCont1 lastMoveWasBackwards (NextKey pred1) currentKeyStr1 keyStr1 keyStr2 initCont2
			| keyStr1 < currentKeyStr1 =
				case incrementedKeyStr currentKeyStr1 of
					?Just nextKeyStr1 ->
						{ holds = False
						, continuation =
							GivenKey
								{ targetKey       = partialKeyStr nextKeyStr1
								, keyMayBeSkipped = False
								, usePreviousKey  = ?None
								, targetKeyPred   = combinedPred1 pred1 initCont2
								}
						}
					?None ->
						{holds = False, continuation = Stop}
			| otherwise = combinedPredResult1 lastMoveWasBackwards pred1 keyStr1 keyStr2 initCont2
		combinedPredResultForCont1 lastMoveWasBackwards cont1=:(GivenKey _) _ keyStr1 keyStr2 initCont2 =
			{holds = False, continuation = combinedCont1 (?Just keyStr1) initCont2 cont1}
		combinedPredResultForCont1 _ Stop _ _ _ _ = {holds = False, continuation = Stop}

		combinedPred2 ::
			!(PredContinuation k1) !(KeyStr k1) !(Pred k2) !(PredContinuation k2) -> Pred (!k1, !k2)
			| primitiveKey k1 & primitiveKey k2
		combinedPred2 cont1 currentKeyStr1 pred2 initCont2 = Pred (pred cont1 currentKeyStr1 pred2 initCont2)
		where
			pred ::
				!(PredContinuation k1) !(KeyStr k1) !(Pred k2) !(PredContinuation k2) !(KeyStr (!k1, !k2)) !Bool
				-> PredResult (k1, k2) | primitiveKey k1 & primitiveKey k2
			pred cont1 currentKeyStr1 pred2 initCont2 keyStr lastMoveWasBackwards
				| currentKeyStr1 == keyStr1 =
					combinedPredResult2        lastMoveWasBackwards cont1 pred2          keyStr1 keyStr2 initCont2
				| otherwise =
					combinedPredResultForCont1 lastMoveWasBackwards cont1 currentKeyStr1 keyStr1 keyStr2 initCont2
			where
				(keyStr1, keyStr2) = splitKeyStr keyStr

		combinedPredResult2 ::
			!Bool !(PredContinuation k1) !(Pred k2) !(KeyStr k1) !(KeyStr k2) !(PredContinuation k2)
			-> PredResult (k1, k2) | primitiveKey k1 & primitiveKey k2
		combinedPredResult2 lastMoveWasBackwards cont1 (Pred pred2) keyStr1 keyStr2 initCont2
			# res2 = pred2 keyStr2 lastMoveWasBackwards
			=	{ PredResult| res2
				& continuation = combinedCont2 lastMoveWasBackwards cont1 keyStr1 initCont2 res2.PredResult.continuation
				}

		combinedCont2 ::
			!Bool !(PredContinuation k1) !(KeyStr k1) !(PredContinuation k2) !(PredContinuation k2)
			-> PredContinuation (!k1, !k2) | primitiveKey k1 & primitiveKey k2
		combinedCont2 lastMoveWasBackwards cont1 keyStr1 initCont2 (NextKey pred2) =
			NextKey (combinedPred2 cont1 keyStr1 pred2 initCont2)
		combinedCont2 _ cont1 keyStr1 initCont2 (GivenKey {targetKey, keyMayBeSkipped, usePreviousKey, targetKeyPred}) =
			GivenKey
				{ targetKey       = joinedKeyStr keyStr1 targetKey
				, keyMayBeSkipped = False
				, usePreviousKey  = contIfNoPrevKeyFor <$> usePreviousKey
				, targetKeyPred   = combinedPred2 cont1 keyStr1 targetKeyPred initCont2
				}
		where
			contIfNoPrevKeyFor ::
				E.^ k1 k2:
					!((?(KeyStr k2)) -> ?(PredContinuation k2)) !(?(KeyStr (k1, k2))) -> ?(PredContinuation (k1, k2))
				| primitiveKey k1 & primitiveKey k2
			contIfNoPrevKeyFor usePreviousKey ?None = contIfNoPrevKey12 <$> usePreviousKey ?None
			contIfNoPrevKeyFor usePreviousKey (?Just keyStr)
				| keyStr1 == keyStr1`= contIfNoPrevKey12 <$> usePreviousKey (?Just keyStr2`)
				| otherwise          = contIfNoPrevKey12 <$> usePreviousKey ?None
			where
				(keyStr1`, keyStr2`) = splitKeyStr keyStr

			contIfNoPrevKey12 :: E.^ k1 k2:
				!(PredContinuation k2) -> PredContinuation (k1, k2) | primitiveKey k1 & primitiveKey k2
			contIfNoPrevKey12 contIfNoPrevKey2 =
				GivenKey
					{ targetKey = partialKeyStr keyStr1
					, keyMayBeSkipped = False
					, usePreviousKey = ?None
					, targetKeyPred =
						combinedPred2
							cont1 keyStr1 (Pred (\_ _ -> {holds = False, continuation = contIfNoPrevKey2})) initCont2
					}
		combinedCont2 _ cont1 keyStr1 initCont2 Stop = combinedCont1 (?Just keyStr1) initCont2 cont1

	partialKeyStr :: !(KeyStr k1) -> KeyStr (k1, k2) | primitiveKey k1
	partialKeyStr keyStr = partialKeyStr` keyStr keyLength
	where
		partialKeyStr` :: !(KeyStr k1) !(KeyLength k1) -> KeyStr (k1, k2)
		partialKeyStr` (KeyStr keyStr) (DynamicLength ?None) = KeyStr (escaped keyStr)
		partialKeyStr` (KeyStr keyStr) _ = KeyStr keyStr

>>& ::
	!(Condition k1) !(Condition k2) !(Condition k3) -> Condition (k1, k2, k3)
	| primitiveKey k1 & primitiveKey k2 & primitiveKey k3
>>& c1 c2 c3 = unsafeCoerce $ >& (>& c1 c2) c3

>>>& ::
	!(Condition k1) !(Condition k2) !(Condition k3) !(Condition k4) -> Condition (k1, k2, k3, k4)
	| primitiveKey k1 & primitiveKey k2 & primitiveKey k3 & primitiveKey k4
>>>& c1 c2 c3 c4 = unsafeCoerce $ >& (>& (>& c1 c2) c3) c4

>>>>& ::
	!(Condition k1) !(Condition k2) !(Condition k3) !(Condition k4) !(Condition k5) -> Condition (k1, k2, k3, k4, k5)
	| primitiveKey k1 & primitiveKey k2 & primitiveKey k3 & primitiveKey k4 & primitiveKey k5
>>>>& c1 c2 c3 c4 c5 = unsafeCoerce $ >& (>& (>& (>& c1 c2) c3) c4) c5

>>>>>& ::
	!(Condition k1) !(Condition k2) !(Condition k3) !(Condition k4) !(Condition k5) !(Condition k6)
	-> Condition (k1, k2, k3, k4, k5, k6)
	| primitiveKey k1 & primitiveKey k2 & primitiveKey k3 & primitiveKey k4 & primitiveKey k5 & primitiveKey k6
>>>>>& c1 c2 c3 c4 c5 c6 = unsafeCoerce $ >& (>& (>& (>& (>& c1 c2) c3) c4) c5) c6

foldedPartitions :: !(String .acc -> .(*txn -> (.acc, *txn))) !.acc !*txn -> (!.acc, !*txn) | readTxn txn
foldedPartitions withPartition acc txn
	# (dbPath, info) = (infoOf txn)!dbPath
	= accUnsafe $ foldedPartitions` dbPath $ txnWith info
where
	foldedPartitions` :: E.^ .acc *txn: !FilePath !*txn !*World -> (!(!.acc, !*txn), !*World)
	foldedPartitions` path txn world
		# (_, acc, world) = scanDirectory forFile (acc, txn) path world
		= (acc, world)
	where
		forFile :: E.^ .acc *txn: !FilePath !FileInfo !(!.acc, !*txn) !*World -> (!(!.acc, !*txn), !*World)
		forFile path` {directory} st=:(acc, txn) world
			| not directory || path` == path = (st, world)
			= (withPartition (dropDirectory path`) acc txn, world)

deletePartition :: !String !*Txn -> *Txn
deletePartition partName (Txn info=:{partDbTxns, partDbs, dbPath, dbis})
	# (partDbTxn, partDbTxns) = 'Map'.delU partName partDbTxns
	# info = maybeSt info (\(PartDbTxn ptr) = commitTxn ptr) partDbTxn
	# dbis = 'Map'.filterWithKey (\(_, part) _ = part <> ?Just partName) dbis
	# (partDb, partDbs) = 'Map'.delU partName partDbs
	# info = maybeSt info (\(PartDb env) = closeDatabase env) partDb
	# info = deleteDbFile info
	= Txn {info & partDbTxns = partDbTxns, partDbs = partDbs, dbis = dbis}
where
	// Operations using `accUnsafe` are defined as separate functions on `*TxnInfo` to enforce evaluation order.
	closeDatabase :: !Int !*TxnInfo -> *TxnInfo
	closeDatabase env info = accUnsafe $ \world = (info, closeDatabasePtr env world)

	deleteDbFile :: !*TxnInfo -> *TxnInfo
	deleteDbFile info
		# (err, info) = accUnsafe \world
			# (err, world) = recursiveDelete (dbPath </> partName) world
			= ((err, info), world)
		| isError err = abortError "deletePartition" err "recursiveDelete returned an error"
		= info

// this instance should not be exported, it's only used to define the cond instances for tuples
instance primitiveKey (!k1, !k2) | primitiveKey k1 & primitiveKey k2 where
	keyLength = combinedKeyLength keyLength keyLength
	where
		combinedKeyLength :: !(KeyLength k1) !(KeyLength k2) -> KeyLength (!k1, !k2)
		combinedKeyLength (FixedLength l1) length2 =
			case length2 of
				FixedLength l2 ->
					FixedLength $ l1 + l2
				DynamicLength lengthOf2 ->
					case lengthOf2 of
						?Just lengthOf2 ->
							DynamicLength $ ?Just \str -> l1 + lengthOf2 (str % (l1, size str - 1))
						?None ->
							DynamicLength $ ?None
		combinedKeyLength (DynamicLength lengthOf1) length2 =
			case lengthOf1 of
				?Just lengthOf1 ->
					case length2 of
						FixedLength l2 ->
							DynamicLength $ ?Just \str -> let length1 = lengthOf1 str in length1 + l2
						DynamicLength lengthOf2 ->
							case lengthOf2 of
								?Just lengthOf2 ->
									DynamicLength $
										?Just
											\str ->
												let length1 = lengthOf1 str
												in length1 + lengthOf2 (str % (length1, size str - 1))
								?None ->
									DynamicLength $ ?None
				?None ->
					DynamicLength $ ?None

	nextLargerKey :: !(!k1, !k2) -> ?(!k1, !k2) | primitiveKey k1 & primitiveKey k2
	nextLargerKey (key1, key2) =
		case nextLargerKey key2 of
			?Just key2 -> ?Just (key1, key2)
			?None      -> (\key1 -> (key1, smallestKey)) <$> nextLargerKey key1

	smallestKey = (smallestKey, smallestKey)

incrementedStrWithFixedLength :: !String -> ?String
incrementedStrWithFixedLength str = incrementedStrElement str (size str - 1)
where
	incrementedStrElement :: !String !Int -> ?String
	incrementedStrElement str i
		| i == -1 = ?None
		# element = str.[i]
		| element == '\xff' = incrementedStrElement {if (j == i) '\0' c \\ c <-: str & j <- [0..]} (i - 1)
		| otherwise         = ?Just {if (j == i) (inc element) c \\ c <-: str & j <- [0..]}

derive gEq DBEnvironmentInfo

dbEnvironmentInfoFor :: !*CDB -> (!DBEnvironmentInfo, !*CDB)
dbEnvironmentInfoFor cdb=:{env}
	# ((mapSize, pageSize, maxPages, nrPagesUsed, lastTxnId, maxReaders, nrReadersUsed), cdb) = getMdbEnvInfo env cdb
	= 	({ mapSize = mapSize, pageSize = pageSize, maxPages = maxPages, nrPagesUsed = nrPagesUsed, lastTxnId = lastTxnId
		 , maxReaders = maxReaders, nrReadersUsed = nrReadersUsed}
		, cdb)

dbEnvironmentInfoForPath :: !String !*env -> (!DBEnvironmentInfo, !*env) | SysCallEnv env
dbEnvironmentInfoForPath path env
	# ((mapSize, pageSize, maxPages, nrPagesUsed, lastTxnId, maxReaders, nrReadersUsed), env) =
		getMdbEnvInfoForPath (packString path) env
	= 	({ mapSize = mapSize, pageSize = pageSize, maxPages = maxPages, nrPagesUsed = nrPagesUsed, lastTxnId = lastTxnId
		 , maxReaders = maxReaders, nrReadersUsed = nrReadersUsed}
		, env)

instance == (KeyStr k)
where
	(==) :: !(KeyStr k) !(KeyStr k) -> Bool
	(==) a b =
		code inline {
			.d 2 0
			jsr eqAC
			.o 0 1 b
		}

instance < (KeyStr k)
where
	(<) :: !(KeyStr k) !(KeyStr k) -> Bool
	(<) a b =
		code inline {
			.d 2 0
			jsr cmpAC
			.o 0 1 i
			pushI 0
			gtI
		}

instance toString (KeyStr k)
where
	toString :: !(KeyStr k) -> {#Char}
	toString a = code inline {no_op}

joinedKeyStr :: !(KeyStr a) !(KeyStr b) -> KeyStr (a, b) | primitiveKey a & primitiveKey b
joinedKeyStr keyStr1=:(KeyStr str1) keyStr2=:(KeyStr str2) =
	case keyLengthOf keyStr1 of
		DynamicLength ?None
			// If the key has dynamic length without fixed-size-prefix (argument is `?None`), we have to escape all
			// 0-bytes, as we use the 0-bytes to indicate the end of the key. This is required to preserve the order
			// with data of the second key appended. For instance, we have `("a", "b") < ("aa", "a")`, but the naive
			// combination of the key string in the tuple would give `"ab"`and `"aaa"`. As `"ab" > `"aaa"`, the order
			// of the key strings does not preserve the order of the tuple-key-values. This is solved by ending the
			// first key with `\0`, which is smaller than all values occuring in the string (as we escaped all `\0`s).
			// We then get `"a\0b"` and `"aa\0a", which preserves order.
			# str1Size = escapedLengthOf str1
			= case keyLengthOf keyStr2 of
				FixedLength _
					# acc = unsafeCreateArray $ str1Size + size str2 + 1
					# acc = copyEscaped 0 0 str1 acc
					# acc = {acc & [str1Size] = '\0'}
					# acc = copy 0 (str1Size + 1) str2 acc
					= KeyStr acc
				DynamicLength lengthOf
					// If the second key has dynamic size, we have to store the length of the first key as last byte
					// (to preserve order). If the second key has no fixed-size-prefix (argument is `?None`), we have to
					// escape the 0-bytes and append 0, for the same reasons as for the first key (see above).
					| str1Size > 255 =
						abort "CleanDB: The length of dynamically sized keys is restricted to 255 bytes.\n"
					# escapedStr2Size = escapedLengthOf str2
					# totalSize =
						if (isJust lengthOf) (str1Size + size str2 + 2) (str1Size + escapedStr2Size + 3)
					# acc = unsafeCreateArray totalSize
					# acc = copyEscaped 0 0 str1 acc
					# acc = {acc & [str1Size] = '\0'}
					| isJust lengthOf
						# acc = copy 0 (str1Size + 1) str2 acc
						# acc = {acc & [totalSize - 1] = fromInt str1Size}
						= KeyStr acc
					| otherwise
						# acc = copyEscaped 0 (str1Size + 1) str2 acc
						# acc = {acc & [totalSize - 2] = '\0', [totalSize - 1] = fromInt str1Size}
						= KeyStr acc
		_ = KeyStr (str1 +++ str2)
where
	copy :: !Int !Int !String !*String -> *String
	copy nSrc nDst str acc = if (nSrc == size str) acc (copy (inc nSrc) (inc nDst) str {acc & [nDst] = str.[nSrc]})

// We replace `"\0"` with `"\1\1"` and `"\1"` with `"\1\2"`, so no `\0`s are left in the string and order is preserved.
escaped :: !String -> String
escaped str = if (escapedLength == size str) str (copyEscaped 0  0 str $ unsafeCreateArray escapedLength)
where
	escapedLength = escapedLengthOf str

copyEscaped :: !Int !Int !String !*String -> *String
copyEscaped nSrc nDst str acc
	| nSrc == size str = acc
	# c = str.[nSrc]
	# (acc, nDst) = case c of
		'\0' = ({acc & [nDst] = '\1', [nDst + 1] = '\1'}, nDst + 2)
		'\1' = ({acc & [nDst] = '\1', [nDst + 1] = '\2'}, nDst + 2)
		_ = ({acc & [nDst] = c}, nDst + 1)
	= copyEscaped (inc nSrc) nDst str acc

escapedLengthOf :: !String -> Int
escapedLengthOf str = foldlArr (\l c = if (c == '\0' || c == '\1') (l + 2) (l + 1)) 0 str

// The joined key encoding is described in the comments of `joinedKeyStr`.
splitKeyStr :: !(KeyStr (!a, !b)) -> (!KeyStr a, !KeyStr b) | primitiveKey a & primitiveKey b
splitKeyStr key = splitKeyStr` key keyLength keyLength
where
	splitKeyStr` :: !(KeyStr (!a, !b)) !(KeyLength a) !(KeyLength b) -> (!KeyStr a, !KeyStr b)
	splitKeyStr` (KeyStr str) (FixedLength fstLength) _ =
		(KeyStr (str % (0, fstLength - 1)), KeyStr (str % (fstLength, size str - 1)))
	splitKeyStr` (KeyStr str) (DynamicLength lengthOf) keyLength2 = case lengthOf of
		?Just lengthOf
			# sepIdx = lengthOf str
			= (KeyStr (str % (0, sepIdx - 1)), KeyStr (str % (sepIdx, keySize - 1)))
		?None = case keyLength2 of
			FixedLength length2
				# endIdx1 = keySize - length2 - 2
				=	( KeyStr (copyUnescaped 0 endIdx1 0 $ unsafeCreateArray $ unescapedLength 0 endIdx1 0)
					, KeyStr (str % (keySize - length2, keySize - 1)))
			DynamicLength lenghtOf
				# sepIdx = toInt str.[keySize - 1]
				# endIdx1 = sepIdx - 1
				# startIdx2 = sepIdx + 1
				# keyStr1 = KeyStr (copyUnescaped 0 endIdx1 0 $ unsafeCreateArray $ unescapedLength 0 endIdx1 0)
				| isJust lenghtOf
					= (keyStr1, KeyStr (str % (startIdx2, keySize - 2)))
				| otherwise
					# endIdx2 = keySize - 3
					# keyStr2 = KeyStr
						(copyUnescaped startIdx2 endIdx2 0 $ unsafeCreateArray $ unescapedLength startIdx2 endIdx2 0)
					= (keyStr1, keyStr2)
	where
		keySize = size str

		// See comments on `escaped`.
		unescapedLength :: !Int !Int !Int -> Int
		unescapedLength n end l
			| n > end = l
			# c = str.[n]
			= if (c == '\1') (unescapedLength (n + 2) end $ l + 1) (unescapedLength (n + 1) end $ l + 1)

		copyUnescaped :: !Int !Int !Int !*String -> String
		copyUnescaped nSrc end nDst acc
			| nSrc > end = acc
			# c = str.[nSrc]
			| c == '\1'
				# c2 = str.[nSrc + 1]
				# cDst = if (c2 == '\1') '\0' '\1'
				= copyUnescaped (nSrc + 2) end (nDst + 1) {acc & [nDst] = cDst}
			= copyUnescaped (nSrc + 1) end (nDst + 1) {acc & [nDst] = c}

incrementedKeyStr :: !(KeyStr k) -> ?(KeyStr k) | primitiveKey k
incrementedKeyStr keyStr = strRepresentation <$> nextLargerKey (fromStrRepresentation keyStr)

keyLengthOf :: (KeyStr k) -> KeyLength k | primitiveKey k
keyLengthOf _ = keyLength

coercedKeyStr :: !(KeyStr k) -> KeyStr l
coercedKeyStr (KeyStr k) = KeyStr k

// TODO: some better error handling instead of aborting
abortLmdbError :: !String !Int !String -> .a
abortLmdbError func ret info =
	abortError func (Error $ concat4 (derefString $ mdb_strerror ret) " (" (toString ret) ")") info

//* Abort on error of operations done on a map.
abortLmdbErrorOnMap :: !String !(dbMap k v) !Int !String -> .a | dbMap dbMap
abortLmdbErrorOnMap func map ret info = abortLmdbErrorOnMapCommon func (commonOf map) ret info

abortLmdbErrorOnMapCommon :: !String !(DBMapCommon k v) !Int !String -> .a
abortLmdbErrorOnMapCommon func {name, partName} ret info =
	abortLmdbError func ret $ concat5 "map: " name (maybe "" (\p = concat3 " (partition " p ")") partName) "; " info

abortError :: !String !(MaybeError e a) !String -> .x | toString e
abortError func (Error e) info =
	abort $ concat ["Internal CleanDB Error in function ", func, ": ", toString e, "\nAdditional info: ", info, "\n"]

traceSetBufferSizeWarning :: !(MaybeError OSError a) !*World -> *World
traceSetBufferSizeWarning (Ok _) world = world
traceSetBufferSizeWarning (Error e) world =
	trace_n ("[WARNING] Could not increase buffersize of channel: " +++ toString e) world

// Use global arrays to avoid memory allocations.
valVal :: {#Int}
valVal =: unsafeCreateArray 2

tmpArr :: {#Int}
tmpArr =: unsafeCreateArray 1

readStringFromVal :: !{#Int} !*env -> (!String, !*env)
readStringFromVal val env = (derefCharArray val.[1] val.[0], env)

instance == CleanDBOptions derive gEq
derive gEq CleanDBOptions

instance == SyncMode derive gEq
derive gEq SyncMode

instance == ParallelOperationsSetting derive gEq
derive gEq ParallelOperationsSetting

readValueFromLargeValueMap :: !Int !String !Pointer !*TxnInfo -> (![String], !*TxnInfo)
readValueFromLargeValueMap dbi keyStr txnPtr info = appFst reverse $ readValueFromLargeValueMap` dbi keyStr 0 [] info
where
	readValueFromLargeValueMap` :: !Int !String !Int ![String] !*TxnInfo -> (![String], !*TxnInfo)
	readValueFromLargeValueMap` dbi keyStr index acc info
		# (ret, valStr, info) = mdbGet txnPtr dbi keyStrWithIndex info
		| ret == MDB_NOTFOUND = (acc, info)
		| ret <> 0 = abortLmdbError "readValueFromLargeValueMap" ret "mdbGet returned an error"
		| size valStr < MAX_CHUNK_SIZE = ([valStr:acc], info)
		| otherwise
			= readValueFromLargeValueMap` dbi keyStr (inc index) [valStr:acc] info
	where
		keyStrWithIndex = keyStrLargeValueMapFor keyStr index

deleteLargeValueMapValues :: !String !Int !Int !*TxnInfo -> (!Int, !*TxnInfo)
deleteLargeValueMapValues keyStr largeValueMap txnPtr info = deleteLargeValueMapValues` keyStr 0 largeValueMap info
where
	deleteLargeValueMapValues` :: !String !Int !Int !*TxnInfo -> (!Int, !*TxnInfo)
	deleteLargeValueMapValues` keyStr index largeValueMap info
		# (ret, info) = mdbDelNoData txnPtr largeValueMap keyStrWithIndex info
		| ret == MDB_NOTFOUND = (0, info)
		| ret <> 0 = abortLmdbError "deleteLargeValueMapValues" ret "mdbDelNoData returned an error"
		| otherwise = deleteLargeValueMapValues` keyStr (inc index) largeValueMap info
	where
		keyStrWithIndex = keyStrLargeValueMapFor keyStr index

openLargeValueMapRead :: !Pointer !String !(?String) !*TxnInfo -> (!?Int, !*TxnInfo)
openLargeValueMapRead ptr mapName partName info
	# (ret, info) = mdb_dbi_open ptr (packString $ mapName +++ "$L") 0 tmpArr info
	| ret == MDB_NOTFOUND = (?None, info)
	| ret <> 0 = abortLmdbError "openLargeValueMap" ret "mdb_dbi_open returned an error"
	# largeValueMap = tmpArr.[0]
	# info = forceEval largeValueMap info
	# info = updateLargeValueMap mapName partName largeValueMap info
	= (?Just largeValueMap, info)

updateLargeValueMap :: !String !(?String) !Int !*TxnInfo -> *TxnInfo
updateLargeValueMap mapName partName largeValueMapDbi txnInfo =
	{txnInfo & dbis =
		'Map'.alter
			(maybe ?None (\mapInfo =  ?Just {mapInfo & largeValueMap = ?Just largeValueMapDbi}))
			(mapName, partName) txnInfo.dbis}

waitFor :: !(a -> Bool) !(InChannel a) !*env -> *env | SysCallEnv env
waitFor pred channel env
	# (msg, env) = receive channel env
	| isError msg = abortError "waitFor" msg "receive returned an error"
	| pred (fromOk msg) = env
	= waitFor pred channel env

closeInChannelAbortOnError :: !String !(InChannel x) !*World -> *World
closeInChannelAbortOnError context channel world
	# (err, world) = closeInChannel channel world
	= if (isError err) (abortError context err "closeInChannel returned an error") world

closeOutChannelAbortOnError :: !String !(OutChannel x) !*env -> *env | SysCallEnv env
closeOutChannelAbortOnError context channel env
	# (err, env) = closeOutChannel channel env
	= if (isError err) (abortError context err "closeOutChannel returned an error") env

instance SysCallEnv CDB where
	accWorld :: !.(*World -> *(.a, *World)) !*CDB -> *(!.a, !*CDB)
	accWorld f cdb
		# res = accUnsafe f
		= (res, cdb)

instance SysCallEnv ReadOnlyTxn where
	accWorld :: !.(*World -> *(.a, *World)) !*ReadOnlyTxn -> *(!.a, !*ReadOnlyTxn)
	accWorld f cdb
		# res = accUnsafe f
		= (res, cdb)

instance SysCallEnv Txn where
	accWorld :: !.(*World -> *(.a, *World)) !*Txn -> *(!.a, !*Txn)
	accWorld f cdb
		# res = accUnsafe f
		= (res, cdb)

derive gEq TxnDurationTable

criticalSectionRetry ::
	!Int !(*env -> (a, *env)) !*LockFile !*env -> (!MaybeOSError a, !*LockFile, !*env) | SysCallEnv env
criticalSectionRetry retryCount f file env
	# (ret, file, env) = criticalSection f file env
	= case ret of
		// If there are no locks available, we retry a number of times.
		Error (r, _) | r == 37 && retryCount < CRITICAL_SECTION_MAX_RETRIES
			# (_, env) = timespecSleep {tv_sec = 0, tv_nsec = retryCount * 100000000} env
			= criticalSectionRetry (inc retryCount) f file env
		ret = (ret, file, env)
where
	CRITICAL_SECTION_MAX_RETRIES = 10

maximumKeySize :: !*txn -> (!Int, !*txn) | readTxn txn
maximumKeySize txn
	# (ptr, info) = (infoOf txn)!ptr
	# (env, info) = (infoOf txn)!dbEnv
	= mdb_env_get_maxkeysize env (txnWith info)

// The 0-bytes are not required, but are used to be compatible with CleanDB 10.
keyStrLargeValueMapFor :: !String !Int -> String
keyStrLargeValueMapFor keyStr idx =
	concat [keyStr, "\0", toString $ strRepresentation idx, {'\0', toChar $ size keyStr}]
