definition module Database.CleanDB

/**
 * This provides a database which is based on maps from Clean values to (sets of) other Clean values.
 * The database is stored persistently and can be accessed concurrently by using the same path.
 * The implementation is based on LMDB (https://symas.com/lmdb/),
 * of which CleanDB inherits its performance characteristics.
 *
 * @property-bootstrap
 *     import Data.Maybe
 *     import Gast.Gen
 *     import StdEnv => qualified all
 *     import Text.GenPrint, Text.Unicode.Encodings.UTF8.GenPrint
 *     import Database.CleanDB.Gast
 *     instance < Bool where
 *         (<) False True = True
 *         (<) _     _    = False
 *     ggen{|UTF8|} gs = [! fromJust $ utf8StringCorrespondingTo k \\ (KeyString k) <|- ggen{|*|} gs]
 *     derive genShow UTF8
 *
 * @property-test-with k1 = Bool
 * @property-test-with k1 = Int
 * @property-test-with k1 = KeyString
 * @property-test-with k2 = Bool
 * @property-test-with k2 = Int
 * @property-test-with k2 = KeyString
 * @property-test-with k3 = Bool
 * @property-test-with k3 = Int
 * @property-test-with k3 = KeyString
 */

from StdEnv            import class List
from StdFunc           import const
from StdOverloaded     import class < (..), class == (..), class toString
from Data.Error        import :: MaybeError
from Data.Func         import $
from Data.Functor      import class Functor
from Data.GenEq        import generic gEq
from Data.Map          import :: Map
from Data.Set          import :: Set
from System.FilePath   import :: FilePath
from System.SubProcess import :: SubProcessId
from System.SysCall    import class SysCallEnv
from System._Pointer   import :: Pointer
import qualified System._Unsafe
from Text.Unicode.Encodings.UTF8 import :: UTF8, utf8StringCorrespondingTo, instance < UTF8, instance == UTF8

/**
 * Opens a database.
 *
 * @param The database's path
 * @param The options to use for the database
 * @param The world
 * @result The opened database.
 */
openDatabase :: !FilePath !CleanDBOptions !*World -> (!*CDB, !*World)

/**
 * Closes a database.
 *
 * @param The database to close.
 */
closeDatabase :: !*CDB !*World -> *World

/**
 * withDatabase path options dbFunc world = (result, world):
 *     `result` is the result of performing `dbFunc` on the database at `path` using `options`.
 */
withDatabase :: !FilePath !CleanDBOptions !.(*CDB -> *(*World -> *(.a, *CDB, *World))) !*World -> (.a, !*World)

/**
 * withDatabaseSt path options dbFunc st world = (result, st, world):
 *     `result` is the result of performing `dbFunc` on the database at `path` using `options`.
 *     `st` is a (possibly unique) state which can be modified by `dbFunc`.
 */
withDatabaseSt ::
	!FilePath !CleanDBOptions !.(.st -> *(*CDB -> *(*World -> *(.a, .st, *CDB, *World)))) .st !*World
	-> (.a, .st, !*World)

/**
 * This represents a database instance.
 */
:: *CDB

/**
 * Default options.
 */
defaultOptions :: CleanDBOptions

:: CleanDBOptions =
	{ syncMode  :: !SyncMode
	  //* this influences durability and write performance
	, readahead :: !Bool
	  //* Turns on readahead (no effect on Windows, as always turned on).
	  //* This might improve performance of sequential accesses on small DBs,
	  //* but can significantly degrade performance for DB larger than the size of the memory available.
	, readaheadPartitionDatabases :: !Bool
	  //* Turns on readahead for partition databases; see {{`readahead`}}.
	, compactionFactor :: !Int
	  //* Higher values reduce storage size, but decrease write performance.
	  //* For each write `compactionFactor`-many items are compacted.
	, maxNumberPages :: !Int
	  //* The maximum number of memory pages used by the database, i.e. the maximum size is `maxNumberPages * 4KB`.
	  //* The size on storage is determined by the actual size of the database,
	  //* a large size can however result in out-of-memory errors
	  //* when opening multiple databases within the same process.
	, maxNumberPagesPartitionDatabases :: !Int
	  //* The maximum number of memory pages used by any partition database; see {{`maxNumberPages`}}.
	, parallelOperationsSetting :: !ParallelOperationsSetting
	  //* Determines whether parallel transactions are enabled and how many transactions can at most be run in parallel
	  //* by {{`getAccumNestedParallel`}} and similar functions. If disabled, parallel operations cannot be used
	  //* on the database. If enabled, when opening the database `maxNumberOfNestedParallelTransactions + 1` subprocesses are
	  //* already forked, so the maximum number of transactions should not exceed the number of parallel processes required.
	}

instance == CleanDBOptions

:: SyncMode
	= FullSync   //* no data is lost
	| NoMetaSync //* the last transaction may be lost
	| NoSync     //* data may be lost and the DB may get corrupted

instance == SyncMode

//* Configures whether parallel operations are allowed and how many operations may run in parallel at the same time.
:: ParallelOperationsSetting
	= ParallelOperationsDisabled //* Parallel operations disabled for the database.
	| ParallelOperationsEnabled !(?Int)
		//* Parallel operations enabled for the database with a provided maximum number of parallel transactions.
		//* If not provided the number equals the number of CPUs available.

instance == ParallelOperationsSetting

/**
 * doTransaction txnFunc cdb = (result, cdb):
 *     `result` is the result of performing `txnFunc` on `cdb`.
 *     The transaction is guaranteed to be performed atomically.
 */
doTransaction :: !(*Txn -> (a, *Txn)) !*CDB -> (!a, !*CDB)

/**
 * doTransactionSt txnFunc st cdb = (result, st, cdb):
 *     `result` is the result of performing `txnFunc` on `cdb`.
 *     `st` is a (possibly unique) state which can be modified by `txnFunc`.
 *     The transaction is guaranteed to be performed atomically.
 */
doTransactionSt :: !(.st -> *(*Txn -> (a, .st, *Txn))) !.st !*CDB -> (!a, !.st, !*CDB)

/**
 * doTransactionStWithOptions options txnFunc st cdb = (result, st, cdb):
 *     `result` is the result of performing `txnFunc` on `cdb` with `options`.
 *     `st` is a (possibly unique) state which can be modified by `txnFunc`.
 *     The transaction is guaranteed to be performed atomically.
 */
doTransactionStWithOptions :: !TransactionOptions !(.st -> *(*Txn -> (a, .st, *Txn))) !.st !*CDB -> (!a, !.st, !*CDB)

//* The default transaction options.
defaultTransactionOptions :: TransactionOptions

:: TransactionOptions =
	{ partitionConstraint :: !PartitionConstraint
		//* This restricts how usage of partition databases is constrained for this transaction.
	}

/**
 * This constrains which (partition) databases may be used in a transaction, which is useful for parallelising writes
 * to the main database and the partition databases. Using a database not allowed by the constraint results in a runtime
 * error!
 */
:: PartitionConstraint
	= Unconstrained //* The main database and all partitions may be used.
	| OnlyMainDatabase //* Only the main database, thus no partition, may be used.
	| OnlyPartition !String
	  //* Only the given partition, thus not the main database and no other partitions may be used.

/**
* doReadOnlyTransaction txnFunc cdb = (result, cdb):
 *     `result` is the result of performing `txnFunc` on `cdb`.
 *     The transaction may not modify the database's content.
 *     It is guaranteed that the view provided to `txnFunc` is consistent.
 */
doReadOnlyTransaction :: !.(*ReadOnlyTxn -> (a, *ReadOnlyTxn)) !*CDB -> (!a, !*CDB)

/**
 * doReadOnlyTransactionSt txnFunc st cdb = (result, st, cdb):
 *     `result` is the result of performing `txnFunc` on `cdb`.
 *     `st` is a (possibly unique) state which can be modified by `txnFunc`.
 *     The transaction may not modify the database's content.
 *     It is guaranteed that the view provided to `txnFunc` is consistent.
 */
doReadOnlyTransactionSt :: !.(.st -> *(*ReadOnlyTxn -> (a, .st, *ReadOnlyTxn))) !.st !*CDB -> (!a, !.st, !*CDB)

/**
 * This represents a transaction. The internal `TxnInfo` type is not meant to be used by users of the library.
 */
:: *Txn =: Txn TxnInfo

/**
 * A read/write transaction is a read transaction as well.
 * This enabled read operations to be performed in such a transaction.
 */
instance readTxn Txn

/**
 * This represents a read-only transaction.
 */
:: *ReadOnlyTxn (=: ReadOnlyTxn TxnInfo)

:: *TxnInfo

/**
 * A read-only transaction is a read transaction.
 * This enabled read operations to be performed in such a transaction.
 */
instance readTxn ReadOnlyTxn

/**
 * dbMap mapName toString fromString = dbMap:
 *     `dbMap` is a database map with name `mapName` using `toString`/`fromString` for en-/decoding values.
 */
dbMap :: !MapName !(v -> String) !(String -> v) -> DBMap k v

/**
 * The unique identifier for a map.
 */
:: MapName :== String

/**
 * A map from values of type `k` to values of type `v`.
 */
:: DBMap k v (=: DBMap (DBMapCommon k v))

/**
 * This enables using a `DBMap` in operations which work on any kind of map.
 */
instance dbMap DBMap

/**
 * dbMultiMap mapName toString fromString = dbMap:
 *     `dbMap` is a database multi map (mapping keys to set of values)
 *     with name `mapName` using `toString`/`fromString` for en-/decoding values.
 *     `dbMultiMap` supports values of dynamic sizes.
 */
dbMultiMap :: !MapName !(v -> String) !(String -> v) -> DBMultiMap k v

/**
 * dbMultiMapFixedValueSize mapName toString fromString = dbMap:
 *     `dbMap` is a database multi map (mapping keys to set of values)
 *     with name `mapName` using `toString`/`fromString` for en-/decoding values.
 *     `dbMultiMapFixedValueSize` only supports values with a fixed size string representation.
 *     Using `dbMultiMapFixedValueSize` is more efficient than `dbMultiMap` when it may be used.
 */
dbMultiMapFixedValueSize :: !MapName !(v -> String) !(String -> v) -> DBMultiMap k v

/**
 * dbMultiMapIntValues mapName toInt fromInt = dbMap:
 *     `dbMap` is a database multi map (mapping keys to set of values)
 *     with name `mapName` using `toInt`/`fromInt` for en-/decoding values.
 *     `dbMultiMapIntValues` only supports Int values.
 *     Using `dbMultiMapIntValues` is more efficient than `dbMultiMap` when it may be used.
 */
dbMultiMapIntValues :: !MapName !(v -> Int) !(Int -> v) -> DBMultiMap k v

/**
 * A map from values of type `k` to sets of values of type `v`.
 */
:: DBMultiMap k v (=: DBMultiMap (DBMultiMapContext k v))

/**
 * This enables using a `DBMultiMap` in operations which work on any kind of map.
 */
instance dbMap DBMultiMap

//* Only exported as it's used in the definition of `DBMultiMap`.
:: DBMultiMapContext k v

class isPartitionedMap ~partDbMap dbMap where
	//* Selects a partition of a partitioned map, turning it into an ordinary map.
	withSelectedPartition :: !p !(partDbMap p k v) -> dbMap k v | toString p

	//* Turns an ordinary map into a partitioned map.
	partitionedMap :: !(dbMap k v) -> partDbMap p k v

/**
 * A partitioned map with partition key of type `p`. Partitioned maps can be more efficient if there is an efficient way
 * to determine which partitions relevant data is stored in. Especially, deleting entire partitions is very efficient.
 * */
:: PartDbMap p k v (=: PartDbMap (DBMap k v))

instance isPartitionedMap PartDbMap DBMap

/**
 * A partitioned multi-map with partition key of type `p`. Partitioned maps can be more efficient if there is an
 * efficient way to determine which partitions relevant data is stored in. Especially, deleting entire partitions is
 * very efficient.
 */
:: PartDbMultiMap p k v (=: PartDbMultiMap (DBMultiMap k v))

instance isPartitionedMap PartDbMultiMap DBMultiMap

//* Statistics about a map or multi-map.
:: MapStats =
	{ size :: !Int //* The total size of the map in bytes.
	, nrOfEntries :: !Int //* The number of entries in the map.
	}

derive gEq MapStats

//* Gets the statistics about all maps in the database.
getMapStats :: !*txn -> (!Map MapName MapStats, !*txn) | readTxn txn

/**
 * This puts a key-value pair into a map.
 * For ordinary map this overwrites the current value for the key if existing,
 * for multi maps the value is added to the set associated to the key.
 *
 * @param the key.
 * @param the value.
 * @param the map.
 * @param the transaction.
 * @result the updated transaction.
 */
put :: !k !v !(dbMap k v) !*Txn -> *Txn | key k & dbMap dbMap

/**
 * This puts a key-value pair into a multimap. Additionally, this function returns whether the key-value pair already
 * existed. The value is added to the set associated to the key.
 *
 * @param the key.
 * @param the value.
 * @param the multi map.
 * @param the transaction.
 * @result Whether the key-value pair already existed within the multi map.
 * @result the updated transaction.
 */
putKeyValueExists :: !k !v !(DBMultiMap k v) !*Txn -> (Bool, *Txn) | key k

/**
 * Deletes the specified key and all associated values from a db map.
 *
 * @param The key to delete.
 * @param The map.
 * @param The transaction.
 * @param The updated transaction.
 */
delete :: !k !(dbMap k v) !*Txn -> *Txn | key k & dbMap dbMap

/**
 * Returns whether the given key is a member of the provided `dbMap`.
 * Using this function is more efficient for retrieving whether a key is part of a map compared to using
 * the `get` related functions.
 *
 * @param The key of which the existence should be checked.
 * @param The `dbMap`.
 * @result `True` if the provided key is part of the `dbMap`, `False` otherwise.
 */
keyExists :: !k !(dbMap k v) !*txn -> (!Bool, !*txn) | key k & dbMap dbMap & readTxn txn

/**
 * Returns whether the given key-value pair is a member of the provided `DBMultiMap`.
 * Using this function is more efficient for retrieving whether a key-value pair is part of a map compared to using
 * the `get` related functions.
 *
 * @param The key of the key-value pair for which the existence should be checked.
 * @param The value of the key-value pair for which the existence should be checked.
 * @param The `dbMap`.
 * @result `True` if the provided key is part of the `dbMap`, `False` otherwise.
 */
keyValueExists :: !k !v !(DBMultiMap k v) !*txn -> (!Bool, !*txn) | key k & readTxn txn

/**
 * Tries to get a single value from a map.
 *
 * @param the key
 * @param the map
 * @param the transaction
 * @result the value associated to the key if present
 */
getSingle :: !k !(DBMap k v) !*txn -> (!?v, !*txn) | readTxn txn & key k

/**
 * Gets a set of key-value pairs from a map.
 *
 * @param the condition defining which key-value pairs to retrieve
 * @param the transaction
 * @result all key-value pairs matching the condition
 */
get :: !(Condition k) !(DBMap k v) !*txn -> (!l (k, v), !*txn) | readTxn txn & key k & List l (k, v)

/**
 * Accumulates a value for a set of key-value pairs from a map.
 * This can be more efficient than first using `get` and then further process the result.
 *
 * @param the condition defining which key-value pairs to update the state for
 * @param The state update function used to update the state for each key-value pair found.
 *        This function can additionally indicate to stop the accumulation.
 * @param the initial state
 * @param the map
 * @param the transaction
 * @result the updated state
 */
getAccum :: !(Condition k) !(k v .st -> (Bool, .st)) !.st !(DBMap k v) !*txn -> (!.st, !*txn) | readTxn txn & key k

/**
 * Accumulates a value for a set of key-value pairs from a map.
 * This can be more efficient than first using `get` and then further process the result.
 * Additionally, keys can be deleted from the map while accumulating.
 *
 * @param The condition defining which key-value pairs to update the accumulator for.
 * @param The state update function used to update the state for each key-value pair found.
 *        This function can additionally indicate to stop the accumulation (first tuple result) and to delete the
 *        current key (second tuple result).
 * @param The initial accumulator.
 * @param The map.
 * @param The transaction.
 * @result The updated state.
 */
getAccumDelete :: !(Condition k) !(k v .st -> (Bool, Bool, .st)) !.st !(DBMap k v) !*Txn -> (!.st, !*Txn) | key k

/**
 * Accumulates a value for a set of key-value pairs from a map.
 * This can be more efficient than first using `get` and then further process the result.
 * Additionally, running nested other transactions within the accumulator function is possible.
 * Inside the nested transaction keys of the map we operate on should not be deleted.
 *
 * @param the condition defining which key-value pairs to update the state for
 * @param The state update function used to update the state for each key-value pair found.
 *        This function can additionally indicate to stop the accumulation.
 * @param the initial state
 * @param the map
 * @param the transaction
 * @result the updated state
 */
getAccumNested ::
	!(Condition k) !(k v .st -> *(*txn -> (Bool, .st, *txn))) !.st !(DBMap k v) !*txn -> (!.st, !*txn)
	| readTxn txn & key k

/**
 * Accumulates a value for a set of key-value pairs from a map.
 * This can be more efficient than first using `get` and then further process the result.
 * Additionally, keys can be deleted from the map while accumulating.
 * Also running nested other transactions within the accumulator function is possible.
 * Inside the nested transaction keys of the map we operate on should not be deleted.
 *
 * @param The condition defining which key-value pairs to update the accumulator for.
 * @param The state update function used to update the state for each key-value pair found.
 *        This function can additionally indicate to stop the accumulation (first tuple result) and to delete the
 *        current key (second tuple result).
 * @param The initial accumulator.
 * @param The map.
 * @param The transaction.
 * @result The updated state.
 */
getAccumNestedDelete ::
	!(Condition k) !(k v .st -> *(*Txn -> (Bool, Bool, .st, *Txn))) !.st !(DBMap k v) !*Txn -> (!.st, !*Txn) | key k

/**
 * This function retrieves keys and their values from the {{`DBMap`}} which adhere to the condition, for each of the
 * key value pairs a nested transaction may be performed to accumulate a result. The nested transactions are
 * performed in parallel, in contrast to {{`getAccumNested`}}.
 * The function can only be used if the {{`maxNumberOfNestedParallelTransactions`}} database option is set to at
 * least the number of parallel transactions used by the operation.
 * NB: The current implementation cannot guarantee a consistent view on the database for all nested transactions.
 * @param The condition defining which key-value pairs to retrieve.
 * @param For each key-value pair which adhere to the condition, this may yield a parameter which is passed to a nested
 *        transaction. `?None` means that no nested transaction is performed. A state not shared with the
 *        nested transactions is additionally accumulated. The function can additionally indicate to stop the
 *        operation.This transaction is only run single-threaded and thus not in parallel.
 * @param This defines the nested transaction for each provided parameter. The function yields a partial result.
 *        Multiple of such transactions are performed in parallel.
 * @param This accumulates the final results by combining the partial results provided by the nested transactions. The
 *        order of the partial results match the order of the parameters provided to the nested transactions. The
 *        function can additonally indicate to stop the operation. Accumulating the result is done in parallel with
 *        the nested transactions, as soon as partial results are available.
 * @param The initial state determining the parameters of the nested transactions.
 * @param The initial final result state state.
 * @param The map.
 * @param Determines how many nested transaction are ran in parallel.
 *        If not provided the number is determined based on the number of processors available.
 * @param The transaction.
 * @result The updated final result state.
  */
getAccumNestedParallel ::
	!(Condition k) !(k v .st -> *(Bool, ?parParam, .st)) !(parParam *ReadOnlyTxn -> *(partRes, *ReadOnlyTxn))
	!(partRes .resAcc -> *(*ReadOnlyTxn -> *(Bool, .resAcc, *ReadOnlyTxn))) !.st !.resAcc !(DBMap k v) !(?Int)
	!*ReadOnlyTxn
	-> (!.resAcc, !*ReadOnlyTxn) | key k

/**
 * This function retrieves keys and their values from the {{`DBMap`}} which adhere to the condition, for each of the
 * key value pairs a nested transaction may be performed to accumulate a result. The nested transactions are
 * performed in parallel, in contrast to {{`getAccumNested`}}.
 * The function can only be used if the {{`maxNumberOfNestedParallelTransactions`}} database option is set to at
 * least the number of parallel transactions used by the operation.
 * NB: The current implementation cannot guarantee a consistent view on the database for all nested transactions.
 * In constract to {{`getAccumNestedParallel`}} a local state is used per subprocess. The result may therefore depend on
 * the number of concurrent subprocesses, if not used carefully. The local states are usually used for optimisations,
 * such as keeping track of computations already done or caching.
 * @param The condition defining which key-value pairs to retrieve.
 * @param For each key-value pair which adhere to the condition, this may yield a parameter which is passed to a nested
 *        transaction. `?None` means that no nested transaction is performed. A state not shared with the
 *        nested transactions is additionally accumulated. The function can additionally indicate to stop the
 *        operation. This transaction is only run single-threaded and thus not in parallel.
 * @param This defines the nested transaction for each provided parameter. The function yields a partial result.
 *        Multiple of such transactions are performed in parallel.
 * @param This accumulates the final results by combining the partial results provided by the nested transactions. The
 *        order of the partial results match the order of the parameters provided to the nested transactions. The
 *        function can additonally indicate to stop the operation. Accumulating the result is done in parallel with
 *        the nested transactions, as soon as partial results are available.
 * @param The initial state determining the parameters of the nested transactions.
 * @param A function yielding a (possibly unique) initial local state. (This is a function to allow unique data
 *        structures to be used as local state. Such state has to be generated for each subprocess.)
 * @param The initial final result state state.
 * @param The map.
 * @param Determines how many nested transaction are ran in parallel.
 *        If not provided the number is determined based on the number of processors available.
 * @param The transaction.
 * @result The updated final result state.
  */
getAccumNestedParallelWithLocalState ::
	!(Condition k) !(k v .st -> *(Bool, ?parParam, .st))
	!(parParam .locSt -> *(*ReadOnlyTxn -> *(partRes, .locSt, *ReadOnlyTxn)))
	!(partRes .resAcc -> *(*ReadOnlyTxn -> *(Bool, .resAcc, *ReadOnlyTxn))) !.st !(() -> .locSt) !.resAcc !(DBMap k v)
	!(?Int) !*ReadOnlyTxn
	-> (!.resAcc, !*ReadOnlyTxn) | key k

/**
 * This function retrieves key-value pairs from several `DBMap` or `DBMultiMap`s which are converted to a common
 * `parParam` according to a specific `Condition` for the the given map.
 * Each of such determined `parParams` is sent to a subprocess which runs a transaction based on the `parParam`,
 * yielding a partial result (these transactions determining the partial results run in parallel).
 * Finally the main process combines the partial results to a final result, which is yielded by this function.
 * NB: The current implementation cannot guarantee a consistent view on the database for all nested transactions.
 * In constract to {{`getAccumNestedParallel`}} a local state is used per subprocess. The result may therefore depend on
 * the number of concurrent subprocesses, if not used carefully. The local states are usually used for optimisations,
 * such as keeping track of computations already done or caching.
 * Using this function and providing a continuation chain is more efficient than using multiple calls of
 * `getAccumNestedParallelWithLocalState`.
 * @param The `GetContinuation` chain, allowing to read key-value pairs from several maps and yielding `parParam`s
 *        which are provided to a nested transaction described below.
 * @param This defines the nested transaction for each provided parameter. The function yields a partial result.
 *        Multiple of such transactions are performed in parallel.
 * @param This accumulates the final results by combining the partial results provided by the nested transactions. The
 *        order of the partial results match the order of the parameters provided to the nested transactions. The
 *        function can additonally indicate to stop the operation. Accumulating the result is done in parallel with
 *        the nested transactions, as soon as partial results are available.
 * @param The initial state which is local to the `GetContinuation` chain.
 * @param The initial state which is local to the function which is called for key-value pairs found in one of the
 *        `dbMap`s given by the `GetContinuation` chain.
 * @param A function yielding a (possibly unique) initial local state. (This is a function to allow unique data
 *        structures to be used as local state. Such state has to be generated for each subprocess.)
 * @param The initial final result state state.
 * @param Determines how many nested transaction are ran in parallel.
 *        If not provided the number is determined based on the number of processors available.
 * @param The transaction.
 * @result The updated final result state.
  */
getAccumNestedParallelWithLocalStateAndContinuationChain ::
	!(GetContinuation parParam .keyValSt .continuationSt)
	!(parParam .locSt -> *(*ReadOnlyTxn -> *(partRes, .locSt, *ReadOnlyTxn)))
	!(partRes .resAcc -> *(*ReadOnlyTxn -> *(Bool, .resAcc, *ReadOnlyTxn))) !.continuationSt !.keyValSt !(() -> .locSt)
	!.resAcc !(?Int) !*ReadOnlyTxn -> (!.resAcc, !*ReadOnlyTxn)

//* Allows to combine two seperate `GetContinuation` chains into a single `GetContinuation` chain.
combineContinuations ::
	!(GetContinuation parParam .keyValSt .continuationSt) !(GetContinuation parParam .keyValSt .continuationSt)
	-> (GetContinuation parParam .keyValSt .continuationSt)

:: GetContinuation parParam keyValSt continuationSt =
	E. dbMap k v:
		GetContinuation
			!(continuationSt -> *(*ReadOnlyTxn ->
				*(GetContinuationResult dbMap k v keyValSt continuationSt parParam, continuationSt, *ReadOnlyTxn)))
		& key k & dbMap dbMap
		//* A continuation chain, which is a function that takes a state local to the continuation chain itself
		//* (`continuationSt`), and returns a `ContinuationResult`.
		//* A `GetContinuationResult` may yield a `GetContinuation` itself, which essentially allows to create a chain
		//* of continuations, which is at some point ended by a `NopContinuation`.
	| NopContinuation
		//* A continuation which does nothing, this is used to mark the end of a continuation chain or
		//* for cases where no operation needs to be performed at all.

:: GetContinuationResult dbMap k v keyValSt continuationSt parParam =
	{ condition :: !Condition k //* A `Condition` for the given `dbMap`.
	, map :: !dbMap k v //* The database map to retrieve the key-value pairs which adhere to the `Condition` for.
	, paramFor :: !k v keyValSt -> *(Bool, ?parParam, keyValSt)
		//* Function that is called for each key-value pair in the map that adheres to the condition.
		//* This function yields a `parParam` which can be used by functions such as
		//* `getAccumNestedParallelWithLocalStateAndContinuationChain.
	, nextContinuation :: !GetContinuation parParam keyValSt continuationSt
		//* The next `GetContinuation` to execute, or a `NopContinuation`.
	}

/**
 * Alters the value of the value associated to a key.
 *
 * @param The key.
 * @param The update function (input of `?None` means that there is no value associated to the key, output of `?None`
 *        results in the key-value pair being deleted).
 * @param The map.
 * @param The transaction.
 * @result The updated transaction.
 */
alter :: !k !((?v) -> ?v) !(DBMap k v) !*Txn -> *Txn | key k

/**
 * Gets the set of values associated to a key.
 *
 * @param the key
 * @param the map
 * @param the transaction
 * @result the values associated to the key
 */
getSingleMulti :: !k !(DBMultiMap k v) !*txn -> (![v], !*txn) | readTxn txn & key k

/**
 * Gets a set of key-value-set pairs from a map.
 *
 * @param the condition defining which key-value-set pairs to retrieve
 * @param the transaction
 * @result all key-value-set pairs matching the condition
 */
getMulti :: !(Condition k) !(DBMultiMap k v) !*txn -> (!l (k, [v]), !*txn) | readTxn txn & key, == k & List l (k, [v])

/**
 * Accumulates a value for a set of key-value pairs from a map.
 * @param The condition defining which key-value pairs to update the state for.
 * @param The state update function used to update the state for each key-value pair found.
 *        This function can additionally indicate to stop the accumulation.
 * @param The initial state.
 * @param The map.
 * @param The transaction.
 * @result The updated state.
 */
getMultiAccum ::
	!(Condition k) !(k v .st -> (Bool, .st)) !.st !(DBMultiMap k v) !*txn -> (!.st, !*txn) | readTxn txn & key k

/**
 * Accumulates a value for a set of key-value pairs from a map.
 * This can be more efficient than first using `getMulti` and then further processing the result.
 * key-value pairs can be deleted from the map while accumulating.
 *
 * @param The condition defining which key-value pairs to update the state for.
 * @param The state update function used to update the accumulator for each key-value pair found.
 *        This function can additionally indicate to stop the accumulation (first tuple result) and to delete the
 *        current key-value pair (second tuple result).
 * @param The initial accumulator.
 * @param The map.
 * @param The transaction.
 * @result The updated accumulator.
 */
getMultiAccumDelete ::
	!(Condition k) !(k v .st -> (Bool, Bool, .st)) !.st !(DBMultiMap k v) !*Txn -> (!.st, !*Txn) | key k

/**
 * Accumulates a value for a set of key-value pairs from a map.
 * Additionally, running nested other transactions within the accumulator function is possible.
 * Inside the nested transaction keys of the map we operate on should not be deleted.
 * @param The condition defining which key-value pairs to update the state for.
 * @param The state update function used to update the state for each key-value pair found.
 *        This function can additionally indicate to stop the accumulation.
 * @param The initial state.
 * @param The map.
 * @param The transaction.
 * @result The updated state.
 */
getMultiAccumNested ::
	!(Condition k) !(k v .st -> *(*txn -> (Bool, .st, *txn))) !.st !(DBMultiMap k v) !*txn -> (!.st, !*txn)
	| readTxn txn & key k

/**
 * Accumulates a value for a set of key-value pairs from a map.
 * This can be more efficient than first using `getMulti` and then further processing the result.
 * Additionally, keys can be deleted from the map while accumulating.
 * Also running nested other transactions within the accumulator function is possible.
 * Inside the nested transaction keys of the map we operate on should not be deleted.
 *
 * @param The condition defining which key-value pairs to update the state for.
 * @param The state update function used to update the accumulator for each key-value pair found.
 *        This function can additionally indicate to stop the accumulation (first tuple result) and to delete the
 *        current key-value pair (second tuple result).
 * @param The initial accumulator.
 * @param The map.
 * @param The transaction.
 * @result The updated accumulator.
 */
getMultiAccumNestedDelete ::
	!(Condition k) !(k v .st -> *(*Txn -> (Bool, Bool, .st, *Txn))) !.st !(DBMultiMap k v) !*Txn -> (!.st, !*Txn)
	| key k

/**
 * Accumulates a value for a set of keys from a map that adhere to the given condition.
 * This is more efficient than using other `getMulti` variants when the values of the map do not have to be processed.
 *
 * @param The condition defining which keys to update the state for.
 * @param The state update function used to update the accumulator for each key found.
 *        This function can additionally indicate to stop the accumulation (first tuple result).
 * @param The initial accumulator.
 * @param The map.
 * @param The transaction.
 * @result The updated accumulator.
 */
getMultiAccumKeysOnly ::
	!(Condition k) !(k .st -> (Bool, .st)) !.st !(DBMultiMap k v) !*txn -> (!.st, !*txn)
	| readTxn txn & key k

/**
 * Accumulates a value for a set of keys from a map that adhere to the given condition.
 * This is more efficient than using other `getMulti` variants when the values of the map do not have to be processed.
 * Also running nested other transactions within the accumulator function is possible.
 * Inside the nested transaction keys of the map we operate on should not be deleted.
 *
 * @param The condition defining which keys to update the state for.
 * @param The state update function used to update the accumulator for each key found.
 *        This function can additionally indicate to stop the accumulation (first tuple result).
 * @param The initial accumulator.
 * @param The map.
 * @param The transaction.
 * @result The updated accumulator.
 */
getMultiAccumKeysOnlyNested ::
	!(Condition k) !(k .st -> *(*txn -> (Bool, .st, *txn))) !.st !(DBMultiMap k v) !*txn -> (!.st, !*txn)
	| readTxn txn & key k

/**
 * This function retrieves keys and their values from the {{`DBMultiMap`}} which adhere to the condition, for each of
 * the key-value pairs a nested transaction may be performed to accumulate a result. The nested transactions are
 * performed in parallel, in contrast to {{`getMultiAccumNested`}}.
 * The function can only be used if the {{`maxNumberOfNestedParallelTransactions`}} database option is set to at
 * least the number of parallel transactions used by the operation.
 * NB: The current implementation cannot guarantee a consistent view on the database for all nested transactions.
 * @param The condition defining which key value-set pairs to retrieve.
 * @param For each key-value pair which adhere to the condition, this may yield a parameter which is passed to a
 *        nested transaction. `?None` means that no nested transaction is performed. A state not shared with the
 *        nested transactions is additionally accumulated. The function can additionally indicate to stop the
 *        operation. This transaction is only run single-threaded and thus not in parallel.
 * @param This defines the nested transaction for each provided parameter. The function yields a partial result.
 *        Multiple of such transactions are performed in parallel.
 * @param This accumulates the final results by combining the partial results provided by the nested transactions. The
 *        order of the partial results match the order of the parameters provided to the nested transactions. The
 *        function can additonally indicate to stop the operation. Accumulating the result is done in parallel with
 *        the nested transactions, as soon as partial results are available.
 * @param The initial state determining the parameters of the nested transactions.
 * @param The initial final result state state.
 * @param The map.
 * @param Determines how many nested transaction are ran in parallel.
 *        If not provided the number is determined based on the number of processors available.
 * @param The transaction.
 * @result The updated parallel state.
 */
getMultiAccumNestedParallel ::
	!(Condition k) !(k v .st -> *(Bool, ?parParam, .st)) !(parParam *ReadOnlyTxn -> *(partRes, *ReadOnlyTxn))
	!(partRes .resAcc -> *(*ReadOnlyTxn -> *(Bool, .resAcc, *ReadOnlyTxn))) !.st !.resAcc !(DBMultiMap k v) !(?Int)
	!*ReadOnlyTxn
	-> *(!.resAcc, !*ReadOnlyTxn) | key k

/**
 * This function retrieves keys and their values from the {{`DBMultiMap`}} which adhere to the condition, for each of
 * the key-value pairs a nested transaction may be performed to accumulate a result. The nested transactions are
 * performed in parallel, in contrast to {{`getMultiAccumNested`}}.
 * The function can only be used if the {{`maxNumberOfNestedParallelTransactions`}} database option is set to at
 * least the number of parallel transactions used by the operation.
 * NB: The current implementation cannot guarantee a consistent view on the database for all nested transactions.
 * In constract to {{`getMultiAccumNestedParallel`}} a local state is used per subprocess. The result may
 * therefore depend on the number of concurrent subprocesses, if not used carefully. The local states are usually used
 * for optimisations, such as keeping track of computations already done or caching.
 * @param The condition defining which key value-set pairs to retrieve.
 * @param For each key-value pair which adhere to the condition, this may yield a parameter which is passed to a
 *        nested transaction. `?None` means that no nested transaction is performed. A state not shared with the
 *        nested transactions is additionally accumulated. The function can additionally indicate to stop the
 *        operation. This transaction is only run single-threaded and thus not in parallel.
 * @param This defines the nested transaction for each provided parameter. The function yields a partial result.
 *        Multiple of such transactions are performed in parallel.
 * @param This accumulates the final results by combining the partial results provided by the nested transactions. The
 *        order of the partial results match the order of the parameters provided to the nested transactions. The
 *        function can additonally indicate to stop the operation. Accumulating the result is done in parallel with
 *        the nested transactions, as soon as partial results are available.
 * @param The initial state determining the parameters of the nested transactions.
 * @param A function yielding a (possibly unique) initial local state. (This is a function to allow unique data
 *        structures to be used as local state. Such state has to be generated for each subprocess.)
 * @param The initial final result state state.
 * @param The map.
 * @param Determines how many nested transaction are ran in parallel.
 *        If not provided the number is determined based on the number of processors available.
 * @param The transaction.
 * @result The updated parallel state.
 */
getMultiAccumNestedParallelWithLocalState ::
	!(Condition k) !(k v .st -> *(Bool, ?parParam, .st))
	!(parParam .locSt -> *(*ReadOnlyTxn -> *(partRes, .locSt, *ReadOnlyTxn)))
	!(partRes .resAcc -> *(*ReadOnlyTxn -> *(Bool, .resAcc, *ReadOnlyTxn))) !.st !(() -> .locSt) !.resAcc
	!(DBMultiMap k v) !(?Int) !*ReadOnlyTxn
	-> *(!.resAcc, !*ReadOnlyTxn) | key k

/**
 * Enables to use an accumulator function which never stops accumulation with `getAccum` and `getAccumMulti`.
 *
 * @type (key val .acc -> .acc) key val .acc -> (Bool, .acc)
 */
withoutStop f k v a :== (False, f k v a)

/**
 * Deletes a key-value-pair from a multi map.
 *
 * @param The key to delete.
 * @param The value to delete.
 * @param The map.
 * @param The transaction.
 * @param The updated transaction.
 */
deleteFromMulti :: !k !v !(DBMultiMap k v) !*Txn -> *Txn | key k

/**
 * Alters the values associated to a key of a `DBMultiMap`.
 *
 * @param The key to alter the values for.
 * @param The update function, the input contains the old values and the function returns the altered values.
 * @param The `DBMultiMap` to alter the values for.
 * @param The transaction.
 * @result The updated transaction.
 */
alterMulti :: !k !([v] -> [v]) !(DBMultiMap k v) !*Txn -> *Txn | key k

/**
 * Drops all keys from a map.
 * This does not free any disk space!
 *
 * @param the map
 * @param the transaction
 * @result the updated transaction
 */
deleteAll :: !(dbMap k v) !*Txn -> *Txn | dbMap dbMap special dbMap=DBMap; dbMap=DBMultiMap

/**
 * Deletes all maps. Use this instead of {{`deleteDataOfAllMaps`}} to save resources, see {{`deleteEmptyMaps`}}, but
 * beware of the caveat below.
 * NB: This operation can be expensive for large databases and does not free space. In case the database is not accessed
 *     concurrently and there is not need to keep the database open, removing the database from disk is more efficient.
 * NB: This should only be used without any other transaction being active!
 */
deleteAllMaps :: !*CDB -> *CDB

/**
 * Deletes all data from all maps. Use this instead of {{`deleteAllMaps`}} if other transactions could be active at the
 * same time. This could however cost some resources, see {{`deleteEmptyMaps`}}.
 * NB: This operation can be expensive for large databases and does not free space. In case the database is not accessed
 *     concurrently and there is not need to keep the database open, removing the database from disk is more efficient.
 */
deleteDataFromAllMaps :: !*Txn -> *Txn

/**
 * Delete all maps without data from the database. Maps are still kept in the database even without data, so this is
 * useful to free resources.
 * NB: This should only be used without any other transaction being active!
 */
deleteEmptyMaps :: !*CDB -> *CDB

/**
 * Starts a trasnaction on a DB.
 * NB: it is important to make the transaction end when it is no longer needed using `endTransaction`.
 *
 * @param The database to start a transaction for.
 * @result The transaction.
 */
startTransaction :: !*CDB -> *Txn

/**
 * Starts a read only transaction on a DB.
 * NB: it is important to make the transaction end when it is no longer needed using `endTransaction`.
 *
 * @param The database to start a transaction for.
 * @result The transaction.
 */
startReadOnlyTransaction :: !*CDB -> *ReadOnlyTxn

/**
 * Ends a transaction on a DB. NB: Should only be used to end a transaction started with `startTransaction`.
 *
 * @param !*Txn -> *CDB
 * @rsult The database on which the transaction was performed.
 */
endTransaction :: !*txn -> *CDB | readTxn txn

/**
 * Enables using UTF8 strings as primitive keys (elements of composed keys).
 *
 * @property key length is correct: A.x :: UTF8 :
 *    keyLengthIsCorrect x
 * @property next larger key is larger: A.k :: UTF8:
 *     nextLargerKeyIsLarger k
 * @property there is no key between this one and the next larger: A.k :: UTF8:
 *     noKeyInBetweenNextLargerKey k
 * @property there is no key smaller than the smallest one: A.type :: Type UTF8:
 *     noKeySmallerThanSmallest type
 */
instance primitiveKey UTF8

/**
 * Enables using UTF8 strings as keys.
 *
 * @property is isomorphism: A.x :: UTF8:
 *     toFromEqualsOriginal x
 * @property preserves order: A.x :: UTF8; y :: UTF8:
 *     keyOrderIsPreservedForPair x y
 * @property hasFixedSize implies fixed size strRepresentation of values: A.v1 :: UTF8; v2 :: UTF8:
 *     keyHasFixedSizeImpliesAllStrRepresentationsFixedLength v1 v2
 */
instance key UTF8

/**
 * Enables using strings as primitive keys (elements of composed keys).
 *
 * @property key length is correct: A.x :: KeyString:
 *    keyLengthIsCorrect x
 * @property next larger key is larger: A.k :: KeyString:
 *     nextLargerKeyIsLarger k
 * @property there is no key between this one and the next larger: A.k :: KeyString:
 *     noKeyInBetweenNextLargerKey k
 * @property there is no key smaller than the smallest one: A.type :: Type KeyString:
 *     noKeySmallerThanSmallest type
 */
instance primitiveKey String

/**
 * Enables using strings as keys.
 *
 * @property is isomorphism: A.x :: String:
 *     toFromEqualsOriginal x
 * @property preserves order: A.x :: String; y :: String:
 *     keyOrderIsPreservedForPair x y
 * @property hasFixedSize implies fixed size strRepresentation of values: A.v1 :: String; v2 :: String:
 *     keyHasFixedSizeImpliesAllStrRepresentationsFixedLength v1 v2
 */
instance key String

/**
 * Enables using integers as primitive keys (elements of composed keys).
 *
 * @property key length is correct: A.x :: Int:
 *    keyLengthIsCorrect x
 * @property next larger key is larger: A.k :: Int:
 *     nextLargerKeyIsLarger k
 * @property there is no key between this one and the next larger: A.k :: Int:
 *     noKeyInBetweenNextLargerKey k
 * @property there is no key smaller than the smallest one: A.type :: Type Int:
 *     noKeySmallerThanSmallest type
 */
instance primitiveKey Int

/**
 * Enables using integers as keys.
 *
 * @property is isomorphism: A.x :: Int:
 *     toFromEqualsOriginal x
 * @property preserves order: A.x :: Int; y :: Int:
 *     keyOrderIsPreservedForPair x y
 * @property hasFixedSize implies fixed size strRepresentation of values: A.v1 :: Int; v2 :: Int:
 *     keyHasFixedSizeImpliesAllStrRepresentationsFixedLength v1 v2
 */
instance key Int

/**
 * Enables using booleans as primitive keys (elements of composed keys).
 *
 * @property key length is correct: A.x :: Bool:
 *    keyLengthIsCorrect x
 * @property next larger key is larger: A.k :: Bool:
 *     nextLargerKeyIsLarger k
 * @property there is no key between this one and the next larger: A.k :: Bool:
 *     noKeyInBetweenNextLargerKey k
 * @property there is no key smaller than the smallest one: A.type :: Type Bool:
 *     noKeySmallerThanSmallest type
 */
instance primitiveKey Bool

/**
 * Enables using booleans as keys.
 *
 * @property is isomorphism: A.x :: Bool:
 *     toFromEqualsOriginal x
 * @property preserves order: A.x :: Bool; y :: Bool:
 *     keyOrderIsPreservedForPair x y
 * @property hasFixedSize implies fixed size strRepresentation of values: A.v1 :: Bool; v2 :: Bool:
 *     keyHasFixedSizeImpliesAllStrRepresentationsFixedLength v1 v2
 */
instance key Bool

/**
 * Enables using characters as primitive keys (elements of composed keys).
 *
 * @property key length is correct: A.x :: Char:
 *    keyLengthIsCorrect x
 * @property next larger key is larger: A.k :: Char:
 *     nextLargerKeyIsLarger k
 * @property there is no key between this one and the next larger: A.k :: Char:
 *     noKeyInBetweenNextLargerKey k
 * @property there is no key smaller than the smallest one: A.type :: Type Char:
 *     noKeySmallerThanSmallest type
 */
instance primitiveKey Char

/**
 * Enables using characters as keys.
 *
 * @property is isomorphism: A.x :: Char:
 *     toFromEqualsOriginal x
 * @property preserves order: A.x :: Char; y :: Char:
 *     keyOrderIsPreservedForPair x y
 * @property hasFixedSize implies fixed size strRepresentation of values: A.v1 :: Char; v2 :: Char:
 *     keyHasFixedSizeImpliesAllStrRepresentationsFixedLength v1 v2
 */
instance key Char

/**
 * Enables using maybe values as primitive keys (elements of composed keys).
 *
 * @property key length is correct: A.x :: ?k1:
 *    keyLengthIsCorrect x
 * @property next larger key is larger: A.k :: ?k1:
 *     nextLargerKeyIsLarger k
 * @property there is no key between this one and the next larger: A.k :: ?k1:
 *     noKeyInBetweenNextLargerKey k
 * @property there is no key smaller than the smallest one: A.type :: Type (?k1):
 *     noKeySmallerThanSmallest type
 */
instance primitiveKey (?a) | primitiveKey a

/**
 * Enables using maybe values as keys.
 *
 * @property is isomorphism: A.x :: ?k1:
 *     toFromEqualsOriginal x
 * @property preserves order: A.x :: ?k1; y :: ?k1:
 *     keyOrderIsPreservedForPair x y
 * @property hasFixedSize implies fixed size strRepresentation of values: A.v1 :: ?k1; v2 :: ?k1:
 *     keyHasFixedSizeImpliesAllStrRepresentationsFixedLength v1 v2
 */
instance key (?a) | key a

/**
 * Enables using unit values as primitive keys (elements of composed keys).
 *
 * @property key length is correct: A.x :: ():
 *    keyLengthIsCorrect x
 * @property next larger key is larger: A.k :: ():
 *     nextLargerKeyIsLarger k
 * @property there is no key between this one and the next larger: A.k :: ():
 *     noKeyInBetweenNextLargerKey k
 * @property there is no key smaller than the smallest one: A.type :: Type ():
 *     noKeySmallerThanSmallest type
 */
instance primitiveKey ()

/**
 * Enables using unit values as keys.
 * This is useful for storing single values.
 *
 * @property is isomorphism: A.x :: ():
 *     toFromEqualsOriginal x
 * @property preserves order: A.x :: (); y :: ():
 *     keyOrderIsPreservedForPair x y
 * @property hasFixedSize implies fixed size strRepresentation of values: A.v1 :: (); v2 :: ():
 *     keyHasFixedSizeImpliesAllStrRepresentationsFixedLength v1 v2
 */
instance key ()

/**
 * A composed key of two elements.
 *
 * @property is isomorphism: A.x :: k1; y :: k2:
 *     toFromEqualsOriginal (x, y)
 * @property preserves order: A.x1 :: k1; y1 :: k2; x2 :: k1; y2 :: k2:
 *     keyOrderIsPreservedForPair (x1, y1) (x2, y2)
 * @property hasFixedSize implies fixed size strRepresentation of values: A.x1 :: k1; y1 :: k2; x2 :: k1; y2 :: k2:
 *     keyHasFixedSizeImpliesAllStrRepresentationsFixedLength (x1,y1) (x2,y2)
 */
instance key (!k1, !k2) | primitiveKey k1 & primitiveKey k2 where
	strRepresentation :: !(!k1, !k2) -> KeyStr (!k1, !k2) | primitiveKey k1 & primitiveKey k2
	fromStrRepresentation :: !(KeyStr (!k1, !k2)) -> (!k1, !k2) | primitiveKey k1 & primitiveKey k2

/**
 * A composed key of three elements.
 *
 * @property is isomorphism: A.x :: k1; y :: k2; z :: k3:
 *     toFromEqualsOriginal (x, y, z)
 * @property preserves order: A.x1 :: k1; y1 :: k2; z1 :: k3; x2 :: k1; y2 :: k2; z2 :: k3:
 *     keyOrderIsPreservedForPair (x1, y1, z1) (x2, y2, z2)
 * @property hasFixedSize implies fixed size strRepresentation of values: A.x1 :: k1; y1 :: k2; z1 :: k3; x2 :: k1; y2 :: k2; z2 :: k3:
 *     keyHasFixedSizeImpliesAllStrRepresentationsFixedLength (x1,y1,z1) (x2,y2,z2)
 */
instance key (!k1, !k2, !k3) | primitiveKey k1 & primitiveKey k2 & primitiveKey k3 where
	strRepresentation :: !(!k1, !k2, !k3) -> KeyStr (!k1, !k2, !k3)
		| primitiveKey k1 & primitiveKey k2 & primitiveKey k3
	fromStrRepresentation :: !(KeyStr (!k1, !k2, !k3)) -> (!k1, !k2, !k3)
		| primitiveKey k1 & primitiveKey k2 & primitiveKey k3

/**
 * A composed key of four elements.
 */
instance key (!k1, !k2, !k3, !k4) | primitiveKey k1 & primitiveKey k2 & primitiveKey k3 & primitiveKey k4 where
	strRepresentation :: !(!k1, !k2, !k3, !k4) -> KeyStr (!k1, !k2, !k3, !k4)
		| primitiveKey k1 & primitiveKey k2 & primitiveKey k3 & primitiveKey k4
	fromStrRepresentation :: !(KeyStr (!k1, !k2, !k3, !k4)) -> (!k1, !k2, !k3, !k4)
		| primitiveKey k1 & primitiveKey k2 & primitiveKey k3 & primitiveKey k4


/**
 * A composed key of five elements.
 */
instance key (!k1, !k2, !k3, !k4, !k5)
    | primitiveKey k1 & primitiveKey k2 & primitiveKey k3 & primitiveKey k4 & primitiveKey k5 where
	strRepresentation :: !(!k1, !k2, !k3, !k4, !k5) -> KeyStr (!k1, !k2, !k3, !k4, !k5)
		| primitiveKey k1 & primitiveKey k2 & primitiveKey k3 & primitiveKey k4 & primitiveKey k5
	fromStrRepresentation :: !(KeyStr (!k1, !k2, !k3, !k4, !k5)) -> (!k1, !k2, !k3, !k4, !k5)
		| primitiveKey k1 & primitiveKey k2 & primitiveKey k3 & primitiveKey k4 & primitiveKey k5

/**
 * A composed key of six elements.
 */
instance key (!k1, !k2, !k3, !k4, !k5, !k6)
    | primitiveKey k1 & primitiveKey k2 & primitiveKey k3 & primitiveKey k4 & primitiveKey k5 & primitiveKey k6 where
	strRepresentation :: !(!k1, !k2, !k3, !k4, !k5, !k6) -> KeyStr (!k1, !k2, !k3, !k4, !k5, !k6)
		| primitiveKey k1 & primitiveKey k2 & primitiveKey k3 & primitiveKey k4 & primitiveKey k5 & primitiveKey k6
	fromStrRepresentation :: !(KeyStr (!k1, !k2, !k3, !k4, !k5, !k6)) -> (!k1, !k2, !k3, !k4, !k5, !k6)
		| primitiveKey k1 & primitiveKey k2 & primitiveKey k3 & primitiveKey k4 & primitiveKey k5 & primitiveKey k6

/**
 * Condition that holds for all keys.
 */
all :: Condition k

/**
 * Condition that holds for no keys.
 */
none :: Condition k

/**
 * Condition that holds for the smallest key.
 */
smallest :: Condition k

//* Condition that holds for the given key.
equals :: !k -> Condition k | key k

//* `largestLessThanOrEq k`: Condition that holds for the largest key less than or equal to `k`.
largestLessThanOrEq :: !k -> Condition k | key k

//* `smallestGreaterThanOrEq k`: Condition that holds for the smallest key greater than or equal to `k`.
smallestGreaterThanOrEq :: !k -> Condition k | key k

//* `lessThan k`: Condition that holds for all keys less than `k`.
lessThan :: !k -> Condition k | key k

//* `lessThanOrEqual k`: Condition that holds for all keys less than or equal `k`.
lessThanOrEqual :: !k -> Condition k | key k

//* `greaterThan k`: Condition that holds for all keys greater than `k`.
greaterThan :: !k -> Condition k | primitiveKey k

//* `greaterThanOrEqual k`: Condition that holds for all keys greater than or equal `k`.
greaterThanOrEqual :: !k -> Condition k | key k

/**
 * Condition that holds for all keys in the given range (including bounds).
 */
isBetween :: !k !k -> Condition k | key k

/**
 * Condition that holds for all keys in the given range (including bounds).
 * No bounds are interpreted as infinity.
 */
isBetweenOptional :: !(?k) !(?k) -> Condition k | key k

/**
 * Condition that holds for all keys in the set provided.
 */
isIn :: !(Set k) -> Condition k | key k

//* Holds if holds for one of the argument condition.
(OR) infixr 2 :: !(Condition k) !(Condition k) -> Condition k

//* Holds if holds for one of the conditions.
disjunctionOf :: ![!Condition k!] -> Condition k

//* Holds if holds for one of the conditions.
disjunctionOfWithCustomOrder :: !(k k -> Bool) ![!Condition k!] -> Condition k

>& :: !(Condition k1) !(Condition k2) -> Condition (k1, k2) | primitiveKey k1 & primitiveKey k2

>>& ::
	!(Condition k1) !(Condition k2) !(Condition k3) -> Condition (k1, k2, k3)
	| primitiveKey k1 & primitiveKey k2 & primitiveKey k3

>>>& ::
	!(Condition k1) !(Condition k2) !(Condition k3) !(Condition k4) -> Condition (k1, k2, k3, k4)
	| primitiveKey k1 & primitiveKey k2 & primitiveKey k3 & primitiveKey k4

>>>>& ::
	!(Condition k1) !(Condition k2) !(Condition k3) !(Condition k4) !(Condition k5) -> Condition (k1, k2, k3, k4, k5)
	| primitiveKey k1 & primitiveKey k2 & primitiveKey k3 & primitiveKey k4 & primitiveKey k5

>>>>>& ::
	!(Condition k1) !(Condition k2) !(Condition k3) !(Condition k4) !(Condition k5) !(Condition k6)
	-> Condition (k1, k2, k3, k4, k5, k6)
	| primitiveKey k1 & primitiveKey k2 & primitiveKey k3 & primitiveKey k4 & primitiveKey k5 & primitiveKey k6

/**
 * Folded partitions of the database. This does not obey ACID properties. If another transaction adds a partition,
 * the partion becomes visible directly within the transaction performing this operation.
 * @param The accumulator function used on each partition name.
 * @param The initial accumulator.
 * @param The transaction.
 * @result The final accumulator value.
 */
foldedPartitions :: !(String .acc -> .(*txn -> (.acc, *txn))) !.acc !*txn -> (!.acc, !*txn) | readTxn txn

/**
 * Delete an partition. All parts of partitioned maps matching the partition name are deleted! This does not obey ACID
 * properties. Partitions remain deleted also if the transaction never finishes.
 */
deletePartition :: !String !*Txn -> *Txn

//// Everything below is only relevant for extending CleanDB with new key types and conditions.

:: KeyLength k
	= FixedLength   !Int                      //* the string representation always has the given fixed length
	| DynamicLength !(?({#Char} -> Int))
	//* The string representation has a dynamic length. If `?Just` is provided, composed keys can be stored more
	//* space-efficiently. This comes with the condition that all keys with same length share the same, fix-length
	//* prefix (which has to preserve order). A function has to be provided to get the key-length from a string
	//* with such prefix. The data in the string after the prefix should be ignored.

/**
 * Key length with coerced type. Useful for defining key instances in term of other key instances.
 *
 * @type (KeyLength a) -> KeyLength b
 */
coercedKeyLength length :== 'System._Unsafe'.unsafeCoerce length

class primitiveKey k | key k where
	/**
	 * keyLength: the length of the key's string representation
	 */
	keyLength :: KeyLength k

	/**
	 * nextLargetKey key = key2:
	 *     `key2` is larger than `key` such that there is no key which is larger than `key`, but smaller then `key2`.
	 *     If `key2` is none, there is no such key.
	 */
	nextLargerKey :: !k -> ?k

	/**
	 * smallestKey: the smallest key
	 */
	smallestKey :: k

incrementedStrWithFixedLength :: !String -> ?String

:: DBEnvironmentInfo =
	{ mapSize :: !Int // Size of the underlying memory map.
	, pageSize :: !Int // Size of a database page (does not have to be equal to the size of an OS-page).
	, maxPages :: !Int // The number of available pages within the memory map (mapsize / pagesize).
	, nrPagesUsed :: !Int // The number of pages that are currently in use.
	, lastTxnId :: !Int // The id of the last transaction that was performed (0 if none were performed yet).
	, maxReaders :: !Int // The maximum number of allowed readers on the memory map.
	, nrReadersUsed :: !Int // The number of readers that are currently active.
	}

derive gEq DBEnvironmentInfo

/**
 * Returns the environment information for the given CDB.
 * This avoids the overhead of creating/opening an environment compared to using `dbEnvironmentInfoForPath`.
 *
 * @param CDB to retrieve environment information for.
 * @result The environment information.
 * @result CDB.
 */
dbEnvironmentInfoFor :: !*CDB -> (!DBEnvironmentInfo, !*CDB)

/**
 * Returns the environment information for the database path (directory).
 * This allows to retrieve the environment info of partitioned maps as well compared to `dbEnvironmentInfoFor`.
 *
 * @param CDB to retrieve environment information for.
 * @result The environment information.
 * @result CDB.
 */
dbEnvironmentInfoForPath :: !String !*env -> (!DBEnvironmentInfo, !*env) | SysCallEnv env

/**
 * A string representing a key.
 */
:: KeyStr k =: KeyStr String

instance ==       (KeyStr k) :: !(KeyStr k) !(KeyStr k) -> Bool :== code {.d 2 0 ; jsr eqAC ; .o 0 1 b}
instance <        (KeyStr k) :: !(KeyStr k) !(KeyStr k) -> Bool :== code {.d 2 0 ; jsr cmpAC ; .o 0 1 i ; pushI 0 ; gtI}
instance toString (KeyStr k) :: !(KeyStr k)             -> {#Char} :== code {no_op}

/**
 * Key string with coerced type. Useful for defining key instances in term of other key instances.
 *
 * @type (KeyStr a) -> KeyStr b
 */
coercedKeyStr (KeyStr str) :== KeyStr str

class key k where
	/**
	 * strRepresentation key = keyStr:
	 *     `keyStr` is the string representation of `key`.
	 */
	strRepresentation :: !k -> KeyStr k

	/**
	 * fromStrRepresentation keyStr = key:
	 *     `keyStr` is the string representation of `key`.
	 */
	fromStrRepresentation :: !(KeyStr k) -> k

	/**
	 * hasFixedSize key = fixedSize:
	 * `key` is unused but provided due to having to use the type variable in every function within a typeclass.
	 * `fixedSize` indicates whether values of type k always have a fixed size string representation.
	 */
	hasFixedSize :: k -> Bool

/**
 * This represents a database condition which can be used for retrieving values of specified keys from a map,
 * e.g. using {{`get`}}. Several pre-defined conditions are provided in this module.
 * The internal representation is only relevant for implementing additional conditions not covered by
 * these pre-defined conditions.
 *
 * @var The type of the keys the condition works on
 */
:: Condition k =
	{ continuations :: ![!PredContinuation k!] // ...
	// * This provides the initial continuations for getting the keys-value-pairs obeying the condition.
	// * The condition is the disjunction corresponding to the continuations given.
	// * The relative target `NextKey` points to the first key stored here.
	// * All predicates provided must provide results in ascending order.
	, isLessThan :: !?(k k -> Bool)
	// * An optional key order used to sort results when multiple initial continuations are provided.
	}

conditionWithDefaultOrder conts :== {continuations = conts, isLessThan = ?None}

/**
 * Provides a result and continuation for a key found,
 * The predicate rewrites itself using an continuation, such that an internal state can be used.
 */
:: Pred k =: Pred ((KeyStr k) Bool -> PredResult k)

/**
 * Represents the result of a predicate.
 */
:: PredResult k = {holds :: !Bool, continuation :: !PredContinuation k}

/**
 * Specifies a how the search continues.
 */
:: PredContinuation k
	= NextKey  !(Pred k)                 //* The next key is the next key stored.
	| GivenKey !(GivenKeyContinuation k) //* The next key is the key specified.
	| Stop                               //* There is no next key.

/**
 * Specifies a concrete key as next key, including the predicate and continuation.
 */
:: GivenKeyContinuation k =
	{ targetKey :: !KeyStr k //* The next key is the given one, if it doesn't exist the next larger one.
	, keyMayBeSkipped :: !Bool
	  //* May the first key greater or equal to `targetKey` be skipped? This is used for optimising composed conditions.
	  //* A value of `False` is a safe choice, but may prevent the optimisation.
	, usePreviousKey :: !?((?(KeyStr k)) -> ?(PredContinuation k))
	  //* This indicates whether instead of the found key the previous one is used,
	  //* which is decided based on the next key found.
	  //* A `?Just` result indicates to go to the previous key.
	  //* In this case additionally the continuation in case there is no previous key is provided.
	  //* `?None` is equivalent to `?Just $ const ?None`, but can be more efficient and should be preferred.
	, targetKeyPred :: !Pred k //* Predicate on the key found.
	}

/**
 * statelessPred predFunc  = pred:
 *    `pred` is a predicate for conditions, which remains the constant predicate defined by `predFunc`
 *    and therefore uses no internal state.
 *
 * This is a utility macro to define instances of `cond`.
 *
 * @type ((Pred k) -> PredContinuation k) ((KeyStr k) -> (Bool, (Pred k) -> PredContinuation k))-> PredContinuation k
 */
statelessPred init pred :== init $ Pred (pred` pred)
where
	pred` :: !((KeyStr k) -> (Bool, (Pred k) -> PredContinuation k)) !(KeyStr k) !Bool -> PredResult k
	pred` pred keyStr _ = {holds = includeRes, continuation = cont $ Pred (pred` pred)}
	where
		(includeRes, cont) = pred keyStr

/**
 * statefulPred predFunc st = pred:
 *    `pred` is a predicate for conditions, which uses an internal state with initial value `st`.
 *    The predicate is defined by `predFunc`, which yields an updated internal state next to the predicate result.
 *
 * This is a utility macro to define instances of `cond`.
 *
 * @type
 *     ((Pred k) -> PredContinuation k)
 *     ((KeyStr k) st -> (Bool, (Pred k) -> PredContinuation k, st))
 *     st
 *     -> PredContinuation k
 */
statefulPred init pred st :== init $ Pred (pred` pred st)
where
	pred` ::
		!((KeyStr k) st -> (Bool, (Pred k) -> PredContinuation k, st)) !st !(KeyStr k) !Bool -> PredResult k
	pred` pred st keyStr _ = {holds = includeRes, continuation = cont $ Pred (pred` pred st`)}
	where
		(includeRes, cont, st`) = pred keyStr st

//// Everything below this is not relevant for a user of the module.

/**
 * This represents a transaction in which read operations are allowed.
 * There should only be instances for `*Txn` and `*ReadOnlyTxn`.
 * No new instances should be defined by the user outside of this module.
 */
class readTxn txn
where
	infoOf :: !*txn -> *TxnInfo
	txnWith :: !*TxnInfo -> *txn

/**
 * This contains information common for maps and multi maps.
 */
:: DBMapCommon k v

/**
 * This type is used to determine whether values have a fixed or dynamic or unsigned integer size, used for optimization.
 * (See MDB_DUPFIXED/MDB_INTEGERDUP LMDB flags)
 */
:: MultiMapValueSize

/**
 * This represents a common map, no matter whether it maps to single values of sets of values.
 * There should only be instances for `*DBMap and `*DBMultiMap.
 * No new instances should be defined by the user outside of this module.
 */
class dbMap dbMap where
	/**
	 * commonOf map = common:
	 *     `common` is the common info of `map`.
	 */
	commonOf :: !(dbMap k v) -> DBMapCommon k v
	/**
	 * getMultiMapValueSize map = multiMapValueSize:
	 * `multiMapValueSize` is ?None if `map` is not a multi map.
	 * `multiMapValueSize` is ?Just (valueSize) if `map` is a multi map.
	 * (see `:: MultiMapValueSize`).
	 */
	valueSizeOfMultiMap :: !(dbMap k v) -> ?MultiMapValueSize

//* Exported only for testing purposes.
mapInfoOf :: !(dbMap k v) !*TxnInfo -> (!?MapInfo, !*TxnInfo) | dbMap dbMap

/**
 * NB: Internal type, exported for test purposes only.
 *
 * This contains all information about a map required during a transaction.
 *
 * Maps are stored in two separate parts, we call the low and high part.
 * All keys in the low part are smaller than all keys in the high part.
 * For each write operation a number of key-value pairs are copied from the high to the low map.
 * This compacts the data, as randomly inserting data in a B+ tree causes non-efficient space behaviour.
 * If the high part has becomes empty, the roles of the maps are swapped.
 */
:: MapInfo =
	{ highMap :: !Int //* Handle of the high map
	, highCursor :: !Pointer //* Cursor on the high map, re-used for puts.
	, lowMap :: !Int //* Handle of the low map
	, lowCursor :: !Pointer //* Cursor on the low map, re-used for puts.
	, largeValueMap :: !?Int
		//* Handle of the map used for storing MAX_CHUNK_SIZE sized chunks of data
		//* for large values which exceed MAX_CHUNK_SIZE.
		//* ?None in case of the map being a multi map.
	, lowestKeyInHighMap :: !?String //* The lowest key stored in the high map if present.
	, nrWrites :: !Int //* Number of writes in current transaction
	, isMultiMap :: !Bool //* `True` if the map is a multi map, `False` otherwise.
	, txnPtr :: !Int //* The transaction associated to the map (the current main transaction for non-partitioned maps).
	}

//* Exported only for testing purposes.
readValueFromLargeValueMap :: !Int !String !Pointer !*TxnInfo -> (![String], !*TxnInfo)

/*
 * Abstract type used to monitor duration of transactions for a given database.
 * The key of map is the pid of the transaction process,
 * the value is the number of seconds the transaction has been taking.
 */
:: TxnDurationTable (=: TxnDurationTable (Map Int Int))

derive gEq TxnDurationTable

/**
 * Terminates transactions for which a given predicate on the transaction duration holds for the given database path.
 * This is based on a transaction duration table, which is updated by the function.
 * `initialTxnDurationTable` provides an initial table.
 * This function is meant to be used in a loop, providing an `initialTxnDurationTable` and then using the
 * updated transaction table for further calls.
 *
 * @param The path to mdb_stat.
 * @param A predicate determining whether a process for which the maximum transaction time is exceeded, is killed. The
 *        first argument is the PID of the process and the second the transaction time in seconds.
 * @param A transaction duration table.
 * @param Increase the duration a transaction within a `TxnDurationTable` has taken
 *        if it still exists by this number of seconds (e.g: when calling this function every 5 seconds, use 5).
 * @param The path to the CDB for which transactions should be terminated if they exceed the maximum time.
 * @param World.
 * @result The updated transaction duration table.
 * @result World.
 */
abortTxnsForWhich ::
	!FilePath !(Int Int *World -> (Bool, *World)) !TxnDurationTable !Int !FilePath !*World
	-> (!TxnDurationTable, *World)

//* Returns an empty transaction duration table.
initialTxnDurationTable :: TxnDurationTable

maximumKeySize :: !*txn -> (!Int ,!*txn) | readTxn txn

/**
 * Exported only for testing purposes.
 * When the byte size of a value exceeds this size, it is split in chunks with a maximum size of this size.
 * E.g: when storing a value of 16000 bytes, the value gets split up into chunks of 4080, 4080, 4080 and 3760 bytes,
 * respectively. The chunks are then stored in a seperate location within the database.
 * The first 4080 bytes are stored in the high/low map. The rest is stored within a seperate internal database.
 * This is done to avoid having to find a sequence of multiple free memory pages to store the value in.
 * Which is avoided because finding a sequence of multiple free memory pages in the LMDB free memory pages list
 * is inefficient.
 * Looking for a single free memory page in the aforementioned list is relatively efficient.
 * The value is 4080 because this was determined to the size in bytes of a memory page without the header,
 * so we never have to find a sequence of multiple free memory pages to store a value.
 */
MAX_CHUNK_SIZE :== 4080

instance SysCallEnv ReadOnlyTxn

instance SysCallEnv Txn
