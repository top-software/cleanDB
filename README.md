# CleanDB

_CleanDB_ is an embedded database for the fuctional programming language [Clean](https://clean.cs.ru.nl/Clean). Concurrent accesses are realised by multiple processes accessing the same database file on the same machine. There is no support for distributing databases across multiple machines yet.

The main design principle is that data is organised in maps moddeld by datatypes of the host language _Clean_. The design makes it possible to specify the structure of data and also access the data using the host language. No different language (e.g. _SQL_) is required; data accesses are specified by _Clean_ functions, which enables maximum code reusability. A drawback is that the level of abstraction is lower than the one provided by relational databases. Indexes on data for instance have to be explicitely specified and accessed.

This design also makes _CleanDB_ very efficient. Data does not have to be transferred between the database and application programs. Data access is specified by accumulator functions which access the data with very low overhead. This allows to retrieved accumulated values (e.g. sums) on a dataset which is larger than the memory available.

The lower level of abstraction also allows programmer to implement very efficient data accesses. The burden of having to handle low level details can become an advantage here, as relational databases provide less control on how data is retrived. Implementing highly efficient solution with relational databases furthermore require to understand the internal workings anyhow.

_CleanDB_ is built on top of [LMDB](https://symas.com/lmdb/), so it inherits some of it's properties:

 * Transactions have a full _ACID_ semantics.
 * Concurrent read-only transtions scale almost linearly with the number of cores, in case data fits in memory (http://www.lmdb.tech/bench/inmem/scaling.html).
 * Write transactions are efficient, but not not scale. Only a single write transaction can be active at the same time.

 These properties make _CleanDB_ mostly suited for read intensive applications. An example is [VIIA](https://gitlab.com/top-software/viia), where data (mostly positions of vessels) is added at a fixed rate to the database. Analysing the data in contrast requires numerous reads of the same data and has to scale with the number of users and alerts.

 Still write operations should be suffienctly fast for writing user content of tens of thousands of users. [VIIA](https://gitlab.com/top-software/viia) can for instance handle several thousand vessel position updates per second.

## Licence

The package is licensed under the BSD-2-Clause license; for details, see the [LICENSE](/LICENSE) file.

### LMDB

Copyright The OpenLDAP Foundation; for the license see [LICENSE](/LICENSE_LMDB).